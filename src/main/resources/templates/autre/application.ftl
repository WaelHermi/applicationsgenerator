<?xml version="1.0" encoding="UTF-8" ?>
<applications>

    <#if application??>
    <application applicationType="<#if application.applicationType??>${application.applicationType}</#if>" group="<#if application.groupApplication??>${application.groupApplication}</#if>" artifact="<#if application.artifactApplication??>${application.artifactApplication}</#if>" version="<#if application.versionApplication??>${application.versionApplication}</#if>" port="<#if application.portApplication??>${application.portApplication}</#if>" authenticationType="<#if application.authentificationType??>${application.authentificationType}<#else >NON</#if>" packaging="<#if application.packagingApplication??>${application.packagingApplication}</#if>" description="<#if application.descriptionApplication??>${application.descriptionApplication}</#if>" lombok="<#if application.lombok??>${application.lombok?c}</#if>">
    <#if application.base??><database type="<#if application.base.baseType??>${application.base.baseType}</#if>" name="<#if application.base.nomBase??>${application.base.nomBase}</#if>" ip="<#if application.base.ipBase??>${application.base.ipBase}</#if>" port="<#if application.base.portBase??>${application.base.portBase}</#if>" username="<#if application.base.usernameBase??>${application.base.usernameBase}</#if>" password="<#if application.base.passwordBase??>${application.base.passwordBase}</#if>"/></#if>

        <#if application.entities??>
        <entities>
            <#list application.entities as entity>
            <entity name="<#if entity.nomEntity??>${entity.nomEntity}</#if>" inheritance="<#if entity.inheritanceType??>${entity.inheritanceType}<#else>NON</#if>" discriminatorColumn="<#if entity.discriminatorColumn??>${entity.discriminatorColumn}</#if>" discriminatorValue="<#if entity.discriminatorValue??>${entity.discriminatorValue}</#if>" primaryKeyJoinColumn="<#if entity.primaryKeyJoinColumn??>${entity.primaryKeyJoinColumn}</#if>" mother="<#if entity.nomMere??>${entity.nomMere}</#if>" controleur="<#if entity.generateControleur??>${entity.generateControleur?c}</#if>">

                <#if entity.clePrimaire??>
                <primarykey name="<#if entity.clePrimaire.nomClePrimaire??>${entity.clePrimaire.nomClePrimaire}</#if>">
                    <#if entity.clePrimaire.attributs??>
                    <#list entity.clePrimaire.attributs as attribut>
                    <attribute name="<#if attribut.nomAttribut??>${attribut.nomAttribut}</#if>" visibility="<#if attribut.visibiliteAttribut??>${attribut.visibiliteAttribut}</#if>" type="<#if attribut.attributType??>${attribut.attributType}</#if>" generationType="<#if attribut.generationType??>${attribut.generationType}<#else>NON</#if>" findBy="true"/>
                    </#list>
                    </#if>
                </primarykey>
                </#if>

                <#if entity.attributs??>
                <attributes>
                    <#list entity.attributs as attribut>
                    <attribute name="<#if attribut.nomAttribut??>${attribut.nomAttribut}</#if>" visibility="<#if attribut.visibiliteAttribut??>${attribut.visibiliteAttribut}</#if>" type="<#if attribut.attributType??>${attribut.attributType}</#if>" unique="<#if attribut.unique??>${attribut.unique?c}</#if>" nullable="<#if attribut.nullable??>${attribut.nullable?c}</#if>" generationType="<#if attribut.generationType??>${attribut.generationType}<#else >NON</#if>" findBy="<#if attribut.findBy??>${attribut.findBy?c}</#if>" dto="<#if attribut.dtoAttribut??>${attribut.dtoAttribut?c}</#if>"/>
                    </#list>
                </attributes>
                </#if>

                <#if entity.relations??>
                <relations>
                    <#list entity.relations as relation>
                    <relation type="<#if relation.relationJPAType??>${relation.relationJPAType}</#if>"  entity="<#if relation.nomEntity??>${relation.nomEntity}</#if>" name="<#if relation.nomRelation??>${relation.nomRelation}</#if>" biName="<#if relation.biNomRelation??>${relation.biNomRelation}</#if>" fetch="<#if relation.fetchType??>${relation.fetchType}</#if>" findBy="<#if relation.findBy??>${relation.findBy?c}</#if>" jsonIgnore="<#if relation.jsonIgnore??>${relation.jsonIgnore?c}</#if>" bi="<#if relation.bidirectionnelle??>${relation.bidirectionnelle?c}</#if>" dto="<#if relation.dtoRelation??>${relation.dtoRelation?c}</#if>"></relation>
                    </#list>
                </relations>
                </#if>

            </entity>
            </#list>
        </entities>
        </#if>

    </application>
    </#if>

</applications>
