package ${package};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ${application.artifactApplication?cap_first}Application {

	public static void main(String[] args) {
		SpringApplication.run(${application.artifactApplication?cap_first}Application.class, args);
	}
}