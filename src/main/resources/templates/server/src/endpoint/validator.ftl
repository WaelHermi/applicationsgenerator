package ${package}.endpoint;

import ${package}.domain.${entity.nomEntity};
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ${entity.nomEntity}Validator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return ${entity.nomEntity}.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

    }
}