<#if entity??>
package ${package}.endpoint;

import ${package}.service.I${entity.nomEntity}Service;
import ${package}.domain.${entity.nomEntity};
<#if entity.clePrimaire?? && entity.clePrimaire.attributs?? && entity.clePrimaire.attributs?size gt 1>
import ${package}.domain.${entity.clePrimaire.nomClePrimaire};
</#if>
import ${package}.dto.${entity.nomEntity}Dto;
import ${package}.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class ${entity.nomEntity}Endpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private I${entity.nomEntity}Service ${entity.nomEntity?uncap_first}Service;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/${entity.nomEntity?uncap_first}s")
    @ApiOperation(
        value = "Get all ${entity.nomEntity?uncap_first}s",
        notes = "Returns first N ${entity.nomEntity?uncap_first}s specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<${entity.nomEntity}Dto> ${entity.nomEntity?uncap_first}sDto =
                ${entity.nomEntity?uncap_first}Service.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(${entity.nomEntity?uncap_first}sDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/${entity.nomEntity?uncap_first}/{id}")
    @ApiOperation(
        value = "Get ${entity.nomEntity?uncap_first} by id",
        notes = "Returns ${entity.nomEntity?uncap_first} for id specified.",
        response = ${entity.nomEntity}Dto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "${entity.nomEntity} not found")})
    public ResponseEntity get(@ApiParam("${entity.nomEntity} id") @PathVariable("id") ${primarykey} id) {

        ${entity.nomEntity}Dto ${entity.nomEntity?uncap_first}Dto = null;
        try {
            ${entity.nomEntity?uncap_first}Dto = convertToDto(${entity.nomEntity?uncap_first}Service.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(${entity.nomEntity?uncap_first}Dto);
    }

    @PostMapping(path = "/v1/${entity.nomEntity?uncap_first}",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new ${entity.nomEntity?uncap_first}",
        notes = "Creates new ${entity.nomEntity?uncap_first}. Returns created ${entity.nomEntity?uncap_first} with id.",
        response = ${entity.nomEntity}Dto.class)
    public ResponseEntity add(
        @ApiParam("${entity.nomEntity}") @Valid @RequestBody ${entity.nomEntity}Dto ${entity.nomEntity?uncap_first}Dto){

        try {
            ${entity.nomEntity?uncap_first}Dto = convertToDto(${entity.nomEntity?uncap_first}Service.add(convertToEntity(${entity.nomEntity?uncap_first}Dto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(${entity.nomEntity?uncap_first}Dto);
    }


    @PutMapping(path = "/v1/${entity.nomEntity?uncap_first}",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing ${entity.nomEntity?uncap_first}",
        notes = "Updates exisitng ${entity.nomEntity?uncap_first}. Returns updated ${entity.nomEntity?uncap_first}.",
        response = ${entity.nomEntity}Dto.class)
    public ResponseEntity update(
        @ApiParam("${entity.nomEntity}") @Valid @RequestBody ${entity.nomEntity}Dto ${entity.nomEntity?uncap_first}Dto){

        try {
            ${entity.nomEntity?uncap_first}Dto = convertToDto(${entity.nomEntity?uncap_first}Service.update(convertToEntity(${entity.nomEntity?uncap_first}Dto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(${entity.nomEntity?uncap_first}Dto);
    }

    @DeleteMapping(path = "/v1/${entity.nomEntity?uncap_first}/{id}")
    @ApiOperation(
        value = "Delete ${entity.nomEntity?uncap_first}",
        notes = "Delete ${entity.nomEntity?uncap_first}. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("${entity.nomEntity?uncap_first} id") @PathVariable("id") ${primarykey} id){

        boolean result;

        try {
            result = ${entity.nomEntity?uncap_first}Service.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    <#if entity.attributs??>
        <#list entity.attributs as attribut>
            <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
                <#if attribut.findBy >
                </#if>
            </#if>
        </#list>
    </#if>

    private ${entity.nomEntity}Dto convertToDto(${entity.nomEntity} ${entity.nomEntity?uncap_first}) {
        return modelMapper.map(${entity.nomEntity?uncap_first}, ${entity.nomEntity}Dto.class);
    }

    private ${entity.nomEntity} convertToEntity(${entity.nomEntity}Dto ${entity.nomEntity?uncap_first}Dto) {
        return modelMapper.map(${entity.nomEntity?uncap_first}Dto, ${entity.nomEntity}.class);
    }
}
</#if>