package ${package}.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ${package}.domain.Role;
import ${package}.domain.Utilisateur;
import ${package}.repository.IRoleRepository;
import ${package}.repository.IUtilisateurRepository;

import java.util.Arrays;
import java.util.Optional;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent>  {

    boolean alreadySetup = false;

    @Autowired
    private IUtilisateurRepository utilisateurRepository;

    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup)
        return;

        createRoleIfNotFound("ROLE_ADMINISTRATEUR");
        Optional<Role> adminRole = roleRepository.findByNomRole("ROLE_ADMINISTRATEUR");
        Utilisateur adminUtilisateur = new Utilisateur();
        adminUtilisateur.setPasswordUtilisateur(passwordEncoder.encode("admin"));
        adminUtilisateur.setUsernameUtilisateur("admin");
        adminUtilisateur.setRoles(adminRole.isPresent() ? Arrays.asList(adminRole.get()) : null);
        adminUtilisateur.setActive(true);
        createUtilisateurIfNotFount(adminUtilisateur);

    }

    public void createUtilisateurIfNotFount(Utilisateur utilisateur){
        if(!utilisateurRepository.existsByUsernameUtilisateur(utilisateur.getUsernameUtilisateur())){
            utilisateurRepository.save(utilisateur);
        }
    }

    @Transactional
    Role createRoleIfNotFound(String name) {

        Optional<Role> roleOptional = roleRepository.findByNomRole(name);
            if (!roleOptional.isPresent()) {
                Role role = new Role();
                role.setNomRole(name);
                roleRepository.save(role);
                roleOptional = Optional.of(role);
            }
                return roleOptional.get();
    }
}