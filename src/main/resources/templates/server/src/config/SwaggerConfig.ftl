package ${package}.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

<#if application.authentificationType?? && application.authentificationType.name() == "JWT" >
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import com.google.common.collect.Lists;
import java.util.Arrays;
import java.util.List;
</#if>

@Configuration
@EnableSwagger2
public class SwaggerConfig {

<#if application.authentificationType?? && application.authentificationType.name() == "JWT" >
    public static final String AUTHORIZATION_HEADER = "Authorization";

</#if>
    /**
     * Every Docket bean is picked up by the swagger-mvc framework - allowing for multiple
     * swagger groups i.e. same code base multiple swagger resource listings.
     */
    @Bean
    public Docket customDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("${package}"))
                .paths(PathSelectors.regex("/.*"))
                .build()
<#if application.authentificationType?? && application.authentificationType.name() == "JWT" >
                .securitySchemes(Lists.newArrayList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
</#if>
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "${application.artifactApplication}",
                "${application.descriptionApplication}",
                "${application.artifactApplication} v1",
                "Terms of service",
                "e-mail",
                "License of API",
                "Url");
        return apiInfo;
    }
<#if application.authentificationType?? && application.authentificationType.name() == "JWT" >

    private ApiKey apiKey() {
        return new ApiKey("Bearer", AUTHORIZATION_HEADER, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth())
        .forPaths(PathSelectors.any()).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("Bearer", authorizationScopes));
    }

</#if>
}
