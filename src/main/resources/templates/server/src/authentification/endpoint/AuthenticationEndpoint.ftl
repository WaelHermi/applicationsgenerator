package ${package}.endpoint;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ${package}.domain.Utilisateur;
import ${package}.dto.UtilisateurDto;
import ${package}.model.AuthenticationRequest;
import ${package}.model.AuthenticationResponse;
import ${package}.service.IUtilisateurService;
import ${package}.service.JwtUtil;

@RestController
public class AuthenticationEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private IUtilisateurService utilisateurService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsernameUtilisateur(), authenticationRequest.getPasswordUtilisateur()));
        } catch (BadCredentialsException e){
            throw new Exception("Nom d'utilisateur ou mot de passe incorrecte", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsernameUtilisateur());
        final String jwt = jwtUtil.generateToken(userDetails);
        final Utilisateur utilisateur = utilisateurService.findByUsernameUtilisateur(authenticationRequest.getUsernameUtilisateur());

        authenticationRequest.setStatus(true);
        return ResponseEntity.ok(new AuthenticationResponse(convertToDto(utilisateur), jwt));
    }

    private UtilisateurDto convertToDto(Utilisateur utilisateur) {
        return modelMapper.map(utilisateur, UtilisateurDto.class);
    }

}
