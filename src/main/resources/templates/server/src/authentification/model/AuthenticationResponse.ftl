package ${package}.model;

<#if application.lombok>
import lombok.AllArgsConstructor;
import lombok.Getter;
import ${package}.dto.UtilisateurDto;

@AllArgsConstructor
@Getter
</#if>
public class AuthenticationResponse {

    private UtilisateurDto utilisateur;
    private final String jwt;

<#if !application.lombok>
    public AuthenticationResponse(UtilisateurDto utilisateur, String jwt) {
        this.utilisateur = utilisateur;
        this.jwt = jwt;
    }

    public UtilisateurDto getUtilisateur() {
        return utilisateur;
    }

    public String getJwt() {
        return jwt;
    }
</#if>
}