package ${package}.model;


<#if application.lombok>
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
</#if>
public class AuthenticationRequest {

    private String usernameUtilisateur;
    private String passwordUtilisateur;
    private Boolean status = false;

<#if !application.lombok>

    public AuthenticationRequest() { }

    public AuthenticationRequest(String usernameUtilisateur, String passwordUtilisateur, Boolean status) {
        this.usernameUtilisateur = usernameUtilisateur;
        this.passwordUtilisateur = passwordUtilisateur;
        this.status = status;
    }

    public String getUsernameUtilisateur() {
        return usernameUtilisateur;
    }

    public void setUsernameUtilisateur(String usernameUtilisateur) {
        this.usernameUtilisateur = usernameUtilisateur;
    }

    public String getPasswordUtilisateur() {
        return passwordUtilisateur;
    }

    public void setPasswordUtilisateur(String passwordUtilisateur) {
        this.passwordUtilisateur = passwordUtilisateur;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
</#if>
}