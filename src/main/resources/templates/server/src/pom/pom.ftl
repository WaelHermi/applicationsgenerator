<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.2.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<groupId>${application.groupApplication}</groupId>
	<artifactId>${application.artifactApplication}</artifactId>
	<version>${application.versionApplication}</version>
	<name>${application.artifactApplication}</name>
	<#if application.descriptionApplication??>
	<description>${application.descriptionApplication}</description>
	</#if>
	<packaging>${application.packagingApplication.valeurPackaging}</packaging>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
	</properties>




	<dependencies>

		<#if application?? && application.applicationType??>
			<#if application.applicationType.dependances??>
				<#list application.applicationType.dependances as dependance>
		<dependency>
			<groupId>${dependance.groupDependance}</groupId>
			<artifactId>${dependance.artifactDependance}</artifactId><#if dependance.versionDependance?? && dependance.versionDependance != "">${'\n'}${'\t'}${'\t'}${'\t'}<version>${dependance.versionDependance}</version></#if>
		</dependency>

				</#list></#if></#if>

		<#if application?? && application.authentificationType??>
			<#if application.authentificationType.dependances??>
				<#list application.authentificationType.dependances as dependance>
		<dependency>
			<groupId>${dependance.groupDependance}</groupId>
			<artifactId>${dependance.artifactDependance}</artifactId><#if dependance.versionDependance?? && dependance.versionDependance != "">${'\n'}${'\t'}${'\t'}${'\t'}<version>${dependance.versionDependance}</version></#if>
			<#if dependance.dependanceScope?? && dependance.dependanceScope != '' >
			<scope>${ dependance.dependanceScope.valeurType }</scope>
			</#if>
		</dependency>

				</#list></#if></#if>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.6</version>
		</dependency>


		<#if application.authentificationType?? && application.authentificationType.name() != "NON" >
		<!-- Spring Security -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.9.1</version>
		</dependency>

		</#if>
		<!-- swagger -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.9.2</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.9.2</version>
		</dependency>

        <#if application.base?? && application.base.baseType??>
        <#if application.base.baseType.dependances??>
        <#list application.base.baseType.dependances as dependance>
        <!-- ${application.base.baseType.nomType} -->
		<dependency>
			<groupId>${dependance.groupDependance}</groupId>
			<artifactId>${dependance.artifactDependance}</artifactId><#if dependance.versionDependance?? && dependance.versionDependance != "">${'\n'}${'\t'}${'\t'}${'\t'}<version>${dependance.versionDependance}</version></#if>
			<scope>runtime</scope>
		</dependency></#list></#if></#if>

		<dependency>
			<groupId>org.modelmapper</groupId>
			<artifactId>modelmapper</artifactId>
			<version>2.3.5</version>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>1.18.10</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
