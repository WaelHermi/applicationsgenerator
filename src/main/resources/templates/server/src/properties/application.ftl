server.port = ${application.portApplication}
spring.application.name = ${application.artifactApplication}

<#if application?? && application.applicationType?? && application.applicationType.proprietes?? && application.applicationType.proprietes?size gt 0>
## Générale
    <#list application.applicationType.proprietes as propriete>
        ${propriete.nomPropriete}=${propriete.valeurPropriete}
    </#list>
</#if>

<#if application.base?? && application.base.usernameBase?? && application.base.baseType?? && application.base.baseType.proprietes?? && application.base.baseType.proprietes?size gt 0>
## Database
    <#list application.base.baseType.proprietes as propriete>
${propriete.nomPropriete}=${propriete.valeurPropriete}
    </#list>
</#if>


<#if application?? && application.authentificationType?? && application.authentificationType.proprietes?? && application.authentificationType.proprietes?size gt 0>
## Security
    <#list application.authentificationType.proprietes as propriete>
${propriete.nomPropriete}=${propriete.valeurPropriete}
    </#list>
</#if>

## Spring Boot Actuator
management.endpoints.web.exposure.include=info, heatlh, metrics