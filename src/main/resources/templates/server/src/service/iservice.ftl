package ${package}.service;

import ${package}.domain.${entity.nomEntity};
<#if entity.clePrimaire?? && entity.clePrimaire.attributs?? && entity.clePrimaire.attributs?size gt 1>
import ${package}.domain.${entity.clePrimaire.nomClePrimaire};
</#if>
import ${package}.repository.I${entity.nomEntity}Repository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ${package}.exception.CreateException;
import ${package}.exception.DeleteException;
import ${package}.exception.GetException;
import ${package}.exception.UpdateException;

public interface I${entity.nomEntity}Service {

    Page<${entity.nomEntity}> findAll(Pageable pageable) throws GetException;

    ${entity.nomEntity} findOne(${primarykey} id) throws GetException;

    ${entity.nomEntity} add(${entity.nomEntity} ${entity.nomEntity?uncap_first}) throws CreateException;

    ${entity.nomEntity} update(${entity.nomEntity} ${entity.nomEntity?uncap_first}) throws UpdateException;

    boolean delete(${primarykey} id) throws DeleteException;

<#if entity.attributs??>
    <#list entity.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
            <#if attribut.findBy && attribut.unique >

    ${entity.nomEntity} findBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut}) throws GetException;
            <#elseif attribut.findBy>

    Collection<${entity.nomEntity}> findBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut}) throws GetException;
            </#if>
        </#if>
    </#list>
</#if>

}