package ${package}.service;

import ${package}.domain.${entity.nomEntity};
<#if entity.clePrimaire?? && entity.clePrimaire.attributs?? && entity.clePrimaire.attributs?size gt 1>
import ${package}.domain.${entity.clePrimaire.nomClePrimaire};
</#if>
import ${package}.repository.I${entity.nomEntity}Repository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ${package}.exception.CreateException;
import ${package}.exception.DeleteException;
import ${package}.exception.GetException;
import ${package}.exception.UpdateException;

<#if application.authentificationType?? && application.authentificationType.name() == "JWT" && entity.nomEntity == "Utilisateur">
import ${package}.domain.Role;
import ${package}.repository.IRoleRepository;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
</#if>
@Service
@Transactional
@Primary
public class ${entity.nomEntity}ServiceImpl implements I${entity.nomEntity}Service<#if application.authentificationType?? && application.authentificationType.name() == "JWT" && entity.nomEntity == "Utilisateur">, UserDetailsService</#if> {

    @Autowired
    private I${entity.nomEntity}Repository ${entity.nomEntity?uncap_first}Repository;
    private static final String ENTITY_NAME = ${'\"'+entity.nomEntity+'\"'};

    @Transactional(readOnly = true)
    public Page<${entity.nomEntity}> findAll(Pageable pageable) {

        return ${entity.nomEntity?uncap_first}Repository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public ${entity.nomEntity} findOne(${primarykey} id) throws GetException {

        Optional<${entity.nomEntity}> ${entity.nomEntity?uncap_first} = ${entity.nomEntity?uncap_first}Repository.findById(id);

        if(!${entity.nomEntity?uncap_first}.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return ${entity.nomEntity?uncap_first}.get();
    }

    @Override
    public ${entity.nomEntity} add(${entity.nomEntity} ${entity.nomEntity?uncap_first}) throws CreateException {

        <#if entity.attributs??>
            <#list entity.attributs as attribut>
                <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
                    <#if attribut.unique>
        if(${entity.nomEntity?uncap_first}Repository.existsBy${attribut.nomAttribut?cap_first}(${entity.nomEntity?uncap_first}.get${attribut.nomAttribut?cap_first}())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique(${'\"'+attribut.nomAttribut+'\"'});
            throw createException;
        }
                    </#if>
                </#if>
            </#list>
        </#if>

        return ${entity.nomEntity?uncap_first}Repository.saveAndFlush(${entity.nomEntity?uncap_first});
    }

    @Override
    public ${entity.nomEntity} update(${entity.nomEntity} ${entity.nomEntity?uncap_first}) throws UpdateException {

        <#if entity.clePrimaire?? && entity.clePrimaire.attributs??>
            <#list entity.clePrimaire.attributs as attribut>
                <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
        if( ${entity.nomEntity?uncap_first}.get${attribut.nomAttribut?cap_first}() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

                </#if>
            </#list>
        </#if>
        <#if entity.attributs??>
            <#list entity.attributs as attribut>
                <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
                    <#if attribut.unique>
        if(${entity.nomEntity?uncap_first}Repository.existsBy${attribut.nomAttribut?cap_first}And${primaryKeyNom?cap_first}Not(${entity.nomEntity?uncap_first}.get${attribut.nomAttribut?cap_first}(), ${entity.nomEntity?uncap_first}.get${primaryKeyNom?cap_first}())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique(${'\"'+attribut.nomAttribut+'\"'});

            throw updateException;

        }

                    </#if>
                </#if>
            </#list>
        </#if>
        return ${entity.nomEntity?uncap_first}Repository.saveAndFlush(${entity.nomEntity?uncap_first});
    }

    @Override
    public boolean delete(${primarykey} id) throws DeleteException {

        if(!${entity.nomEntity?uncap_first}Repository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        ${entity.nomEntity?uncap_first}Repository.deleteById(id);
        return !${entity.nomEntity?uncap_first}Repository.existsById(id);
    }
<#if entity.attributs??>
    <#list entity.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
            <#if attribut.findBy && attribut.unique>

    @Override
    public ${entity.nomEntity} findBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut?uncap_first}) throws GetException {

    Optional<${entity.nomEntity}> ${entity.nomEntity?uncap_first} = ${entity.nomEntity?uncap_first}Repository.findBy${attribut.nomAttribut?cap_first}(${attribut.nomAttribut?uncap_first});

        if(!${entity.nomEntity?uncap_first}.isPresent()){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
        return ${entity.nomEntity?uncap_first}.get();
    }
            <#elseif attribut.findBy>

    @Override
    public Collection<${entity.nomEntity}> findBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut?uncap_first}) throws GetException {

        Collection<${entity.nomEntity}> ${entity.nomEntity?uncap_first} = ${entity.nomEntity?uncap_first}Repository.findBy${attribut.nomAttribut?cap_first}(${attribut.nomAttribut?uncap_first});

        if(${entity.nomEntity?uncap_first} == null){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
            return ${entity.nomEntity?uncap_first};
    }
            </#if>
        </#if>
    </#list>
</#if>

<#if application.authentificationType?? && application.authentificationType.name() == "JWT" && entity.nomEntity == "Utilisateur">

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Utilisateur> utilisateur = utilisateurRepository.findByUsernameUtilisateur(username);
        if(utilisateur.isPresent()){
            return new User(utilisateur.get().getUsernameUtilisateur(), utilisateur.get().getPasswordUtilisateur(), true, true ,true, utilisateur.get().getActive(), getAuthorities(new ArrayList<>(utilisateur.get().getRoles()))) ;
        }else{
            throw new UsernameNotFoundException("Nom d'utilisateur non trouvé");
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles) {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        for (Role role: roles) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getNomRole());
            grantedAuthorities.add(grantedAuthority);
        }
            return grantedAuthorities;
    }
</#if>
}