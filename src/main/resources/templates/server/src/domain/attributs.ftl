<#if entity.clePrimaire?? && entity.clePrimaire.attributs??>
    <#list entity.clePrimaire.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
    ${application.base.baseType.idAnnotation}
            <#if attribut.generationType?? && attribut.generationType.valeur??>
    @GeneratedValue(<#if attribut.generationType?? && attribut.generationType.valeur?? && attribut.generationType.valeur != "" >strategy= ${attribut.generationType.valeur}</#if>)
            </#if>
    ${attribut.visibiliteAttribut.valeur} ${attribut.attributType.nomType} ${attribut.nomAttribut};

        </#if>
    </#list>
</#if>
<#if entity.attributs??>
    <#list entity.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
            <#if attribut.unique || !attribut.nullable>
    @Column(<#if attribut.unique && !attribut.nullable>unique = ${attribut.unique?c}, nullable = ${attribut.nullable?c}<#elseif !attribut.nullable>nullable = ${attribut.nullable?c}<#elseif attribut.unique>unique = ${attribut.unique?c}</#if>)
            </#if>
    ${attribut.visibiliteAttribut.valeur} ${attribut.attributType.nomType} ${attribut.nomAttribut};

        </#if>
    </#list>
</#if>