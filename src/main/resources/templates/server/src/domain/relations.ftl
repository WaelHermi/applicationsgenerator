<#if entity.relations??>
    <#list entity.relations as relation>
        <#if relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" && relation.destination.nomEntity != entity.nomEntity >
            <#if relation.relationJPAType.annotation == "@OneToOne">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
    <#if relation.destination.clePrimaire?? && relation.destination.clePrimaire.attributs?? && relation.destination.clePrimaire.attributs?size gt 1>
    @JoinColumns(
        <#list relation.destination.clePrimaire.attributs as attribut>
        @JoinColumn( name = ${'\"'+attribut.nomAttribut+'\"'} )
        </#list>
    )
    <#else>
        <#list relation.destination.clePrimaire.attributs as attribut>
    @JoinColumn( name = ${'\"'+attribut.nomAttribut+'\"'} )
        </#list>
    </#if>
    private ${relation.nomEntity} ${relation.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.annotation == "@ManyToOne">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
                <#if relation.destination.clePrimaire?? && relation.destination.clePrimaire.attributs?? && relation.destination.clePrimaire.attributs?size gt 1>
    @JoinColumns(
                    <#list relation.destination.clePrimaire.attributs as attribut>
        @JoinColumn( name = ${'\"'+attribut.nomAttribut+'\"'} )
                    </#list>
    )
                <#else>
                    <#list relation.destination.clePrimaire.attributs as attribut>
    @JoinColumn(name = ${'\"'+attribut.nomAttribut+'\"'})
                    </#list>
                </#if>
    private ${relation.nomEntity} ${relation.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.annotation == "@OneToMany">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if (relation.cascadeTypes?? || relation.fetchType.valeur??) && relation.bidirectionnelle>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+entity.nomEntity?uncap_first+'\"'}</#if>)
    private Collection<${relation.nomEntity}> ${relation.nomEntity?uncap_first}s;

            </#if>
            <#if relation.relationJPAType.annotation == "@ManyToMany">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
    @JoinTable(joinColumns = { <#list entity.clePrimaire.attributs as attribut>@JoinColumn(name = ${'\"'+attribut.nomAttribut+'\"'})<#sep>, </#sep></#list> }, inverseJoinColumns = { <#list relation.destination.clePrimaire.attributs as attribut>@JoinColumn(name = ${'\"'+attribut.nomAttribut+'\"'})<#sep>, </#sep></#list> })
    private Collection<${relation.nomEntity}> ${relation.nomEntity?uncap_first}s;

            </#if>
        <#elseif relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" && relation.source.nomEntity != entity.nomEntity && relation.bidirectionnelle >
            <#if relation.relationJPAType.inverseAnnotation == "@OneToOne">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if ( relation.bidirectionnelle  && relation.destination.nomEntity == entity.nomEntity ) && ( relation.cascadeTypes?? || relation.fetchType.valeur?? )>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+entity.nomEntity?uncap_first+'\"'}</#if>)
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private ${relation.source.nomEntity} ${relation.source.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@ManyToOne">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
                <#if relation.source.clePrimaire?? && relation.source.clePrimaire.attributs?? && relation.source.clePrimaire.attributs?size gt 1>
    @JoinColumns(
                    <#list relation.source.clePrimaire.attributs as attribut>
        @JoinColumn( name = ${'\"'+attribut.nomAttribut+'\"'} )
                    </#list>
    )
                <#else>
                    <#list relation.source.clePrimaire.attributs as attribut>
    @JoinColumn(name = ${'\"'+attribut.nomAttribut+'\"'})
                    </#list>
                </#if>
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private ${relation.source.nomEntity} ${relation.source.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@OneToMany">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeType??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if ( relation.bidirectionnelle  && relation.destination.nomEntity == entity.nomEntity ) && ( relation.cascadeTypes?? || relation.fetchType.valeur?? )>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+entity.nomEntity?uncap_first+'\"'}</#if>)
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private Collection<${relation.source.nomEntity}> ${relation.source.nomEntity?uncap_first}s;

            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@ManyToMany">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if ( relation.bidirectionnelle  && relation.destination.nomEntity == entity.nomEntity ) && ( relation.cascadeTypes?? || relation.fetchType.valeur?? )>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+entity.nomEntity?uncap_first+'s\"'}</#if>)
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private Collection<${relation.source.nomEntity}> ${relation.source.nomEntity?uncap_first}s;

            </#if>
        <#elseif relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" &&  relation.source.nomEntity == relation.destination.nomEntity>
            <#if relation.relationJPAType.annotation == "@OneToOne">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
    @JoinColumn
    private ${relation.nomEntity} ${relation.nomRelation?uncap_first};

            <#elseif relation.relationJPAType.annotation == "@ManyToOne">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
    @JoinColumn
    private ${relation.nomEntity} ${relation.nomRelation?uncap_first};

            <#elseif relation.relationJPAType.annotation == "@OneToMany">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>, mappedBy=${'\"'+entity.BiNomRelation?uncap_first+'\"'})
    private Collection<${relation.nomEntity}> ${relation.nomRelation?uncap_first}s;

            <#elseif relation.relationJPAType.annotation == "@ManyToMany">
    ${relation.relationJPAType.annotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
    @JoinTable(joinColumns = { <#list entity.clePrimaire.attributs as attribut>@JoinColumn(name = ${'\"'+attribut.nomAttribut+'\"'})<#sep>, </#sep></#list> }, inverseJoinColumns = { <#list entity.clePrimaire.attributs as attribut>@JoinColumn(name = ${'\"'+attribut.nomAttribut+'\"'})<#sep>, </#sep></#list> })
    private Collection<${relation.nomEntity}> ${relation.nomRelation?uncap_first}s;

            </#if>
            <#if relation.bidirectionnelle>
                <#if relation.relationJPAType.inverseAnnotation == "@OneToOne">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if ( relation.bidirectionnelle  && relation.destination.nomEntity == entity.nomEntity ) && ( relation.cascadeTypes?? || relation.fetchType.valeur?? )>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+relation.nomRelation?uncap_first+'\"'}</#if>)
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private ${relation.source.nomEntity} ${relation.biNomRelation?uncap_first};

                </#if>
                <#if relation.relationJPAType.inverseAnnotation == "@ManyToOne">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if>)
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    @JoinColumn
    private ${relation.source.nomEntity} ${relation.biNomRelation?uncap_first};

                </#if>
                <#if relation.relationJPAType.inverseAnnotation == "@OneToMany">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if ( relation.bidirectionnelle  && relation.destination.nomEntity == entity.nomEntity ) && ( relation.cascadeTypes?? || relation.fetchType.valeur?? )>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+relation.nomRelation?uncap_first+'\"'}</#if>)
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private Collection<${relation.source.nomEntity}> ${relation.biNomRelation?uncap_first};

                </#if>
                <#if relation.relationJPAType.inverseAnnotation == "@ManyToMany">
    ${relation.relationJPAType.inverseAnnotation}(<#if relation.cascadeTypes??>cascade = { <#list relation.cascadeTypes as cascadeType>${cascadeType.valeur}<#sep>, </#sep></#list> }</#if><#if relation.cascadeTypes?? && relation.fetchType.valeur??>, </#if><#if relation.fetchType.valeur??>fetch = ${relation.fetchType.valeur}</#if><#if ( relation.bidirectionnelle  && relation.destination.nomEntity == entity.nomEntity ) && ( relation.cascadeTypes?? || relation.fetchType.valeur?? )>, </#if><#if relation.bidirectionnelle>mappedBy=${'\"'+relation.nomRelation?uncap_first+'s\"'}</#if>)
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private Collection<${relation.source.nomEntity}> ${relation.biNomRelation?uncap_first};

                </#if>
            </#if>
        </#if>
    </#list>
</#if>