<#if application?? && application.base?? && entity??>
package ${package}.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
<#if application.lombok>
import lombok.*;
</#if>

${application.base.baseType.entityAnnotation}
<#if entity.clePrimaire?? && entity.clePrimaire.attributs?? && entity.clePrimaire.attributs?size gt 1>
@IdClass(${entity.clePrimaire.nomClePrimaire}.class)
</#if>
<#if entity.inheritanceType?? && entity.inheritanceType.valeur??>
@Inheritance(strategy = ${entity.inheritanceType.valeur})
</#if>
<#if entity.discriminatorColumn?? && entity.discriminatorColumn != '' && entity.inheritanceType.valeur == "InheritanceType.SINGLE_TABLE">
@DiscriminatorColumn(name = ${'\"'+entity.discriminatorColumn+'\"'})
</#if>
<#if entity.discriminatorValue?? && entity.discriminatorValue != ''>
@DiscriminatorValue(${'\"'+entity.discriminatorValue+'\"'})
</#if>
<#if application.lombok>
@NoArgsConstructor
@Getter @Setter
</#if>
public class ${entity.nomEntity}<#if entity.nomMere?? && entity.nomMere != '' && entity.nomMere != 'Object'> extends ${entity.nomMere}</#if> implements Serializable {

<#include "attributs.ftl">
<#include "relations.ftl">
<#include "gettersAndSetters.ftl">

}
</#if>

