<#if entity.clePrimaire?? && entity.clePrimaire.attributs??>
    <#list entity.clePrimaire.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
    ${attribut.visibiliteAttribut.valeur} ${attribut.attributType.nomType} ${attribut.nomAttribut};

        </#if>
    </#list>
</#if>
<#if entity.attributs??>
    <#list entity.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "" && attribut.dtoAttribut>
            <#if attribut.nullable?? && !attribut.nullable>
    @NotNull(message = ${'\"'+attribut.nomAttribut+' ne peut pas être null\"'})
            </#if>
    ${attribut.visibiliteAttribut.valeur} ${attribut.attributType.nomType} ${attribut.nomAttribut};

        </#if>
    </#list>
</#if>