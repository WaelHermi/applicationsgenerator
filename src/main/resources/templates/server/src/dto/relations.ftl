<#if entity.relations??>
    <#list entity.relations as relation>
        <#if relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" && relation.destination.nomEntity != entity.nomEntity && relation.dtoRelation >
            <#if relation.relationJPAType.annotation == "@OneToOne">
    private ${relation.nomEntity}Dto ${relation.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.annotation == "@ManyToOne">
    private ${relation.nomEntity}Dto ${relation.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.annotation == "@OneToMany">
    private Collection<${relation.nomEntity}Dto> ${relation.nomEntity?uncap_first}s;

            </#if>
            <#if relation.relationJPAType.annotation == "@ManyToMany">
    private Collection<${relation.nomEntity}Dto> ${relation.nomEntity?uncap_first}s;

            </#if>
        <#elseif relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" && relation.source.nomEntity != entity.nomEntity && relation.bidirectionnelle >
            <#if relation.relationJPAType.inverseAnnotation == "@OneToOne">
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private ${relation.source.nomEntity} ${relation.source.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@ManyToOne">
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private ${relation.source.nomEntity}Dto ${relation.source.nomEntity?uncap_first};

            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@OneToMany">
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private Collection<${relation.source.nomEntity}Dto> ${relation.source.nomEntity?uncap_first}s;

            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@ManyToMany">
                <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                </#if>
    private Collection<${relation.source.nomEntity}Dto> ${relation.source.nomEntity?uncap_first}s;

            </#if>
        <#elseif relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" &&  relation.source.nomEntity == relation.destination.nomEntity>
            <#if relation.relationJPAType.annotation == "@OneToOne">
    private ${relation.nomEntity}Dto ${relation.nomRelation?uncap_first};

            <#elseif relation.relationJPAType.annotation == "@ManyToOne">
    private ${relation.nomEntity}Dto ${relation.nomRelation?uncap_first};

            <#elseif relation.relationJPAType.annotation == "@OneToMany">
    private Collection<${relation.nomEntity}Dto> ${relation.nomRelation?uncap_first}s;

            <#elseif relation.relationJPAType.annotation == "@ManyToMany">
    private Collection<${relation.nomEntity}Dto> ${relation.nomRelation?uncap_first}s;

            </#if>
            <#if relation.bidirectionnelle>
                <#if relation.relationJPAType.inverseAnnotation == "@OneToOne">
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private ${relation.source.nomEntity}Dto ${relation.biNomRelation?uncap_first};

                </#if>
                <#if relation.relationJPAType.inverseAnnotation == "@ManyToOne">
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private ${relation.source.nomEntity}Dto ${relation.biNomRelation?uncap_first};

                </#if>
                <#if relation.relationJPAType.inverseAnnotation == "@OneToMany">
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private Collection<${relation.source.nomEntity}Dto> ${relation.biNomRelation?uncap_first};

                </#if>
                <#if relation.relationJPAType.inverseAnnotation == "@ManyToMany">
                    <#if application.lombok>
    @Getter(onMethod = @__( @JsonIgnore))
                    </#if>
    private Collection<${relation.source.nomEntity}Dto> ${relation.biNomRelation?uncap_first};

                </#if>
            </#if>
        </#if>
    </#list>
</#if>