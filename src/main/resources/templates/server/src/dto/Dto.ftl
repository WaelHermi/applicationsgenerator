<#if application?? && application.base?? && entity??>
package ${package}.dto;

import java.io.Serializable;
import java.util.*;
import java.sql.Timestamp;
<#if application.lombok>
import lombok.*;
</#if>
import javax.validation.constraints.*;


<#if application.lombok>
@NoArgsConstructor
@Getter @Setter
</#if>
public class ${entity.nomEntity}Dto<#if entity.nomMere?? && entity.nomMere != '' && entity.nomMere != 'Object'> extends ${entity.nomMere}Dto</#if> implements Serializable {

<#include "attributs.ftl">
<#include "relations.ftl">
<#include "gettersAndSetters.ftl">
}
</#if>

