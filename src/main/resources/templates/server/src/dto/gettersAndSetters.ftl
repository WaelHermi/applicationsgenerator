<#if entity.clePrimaire.attributs?? && !application.lombok>
    <#list entity.clePrimaire.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
    public ${attribut.attributType.nomType} get${attribut.nomAttribut?cap_first}() {
        return this.${attribut.nomAttribut};
    }
    public void set${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut}) {
        this.${attribut.nomAttribut} = ${attribut.nomAttribut};
    }
        </#if>
    </#list>
</#if>
<#if entity.attributs?? && !application.lombok>
    <#list entity.attributs as attribut>
        <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "" && attribut.dtoAttribut>
    public ${attribut.attributType.nomType} get${attribut.nomAttribut?cap_first}() {
        return this.${attribut.nomAttribut};
    }
    public void set${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut}) {
        this.${attribut.nomAttribut} = ${attribut.nomAttribut};
    }
        </#if>
    </#list>
</#if>
<#if entity.relations?? &&  !application.lombok>
    <#list entity.relations as relation>
        <#if relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.nomEntity?? && relation.nomEntity != "" &&  ( relation.destination.nomEntity != entity.nomEntity || relation.source.nomEntity == relation.destination.nomEntity )>
            <#if relation.relationJPAType.annotation == "@ManyToOne" || relation.relationJPAType.annotation == "@OneToOne">
    public ${relation.nomEntity}Dto get${relation.nomEntity}() {
        return this.${relation.nomEntity?uncap_first};
    }
    public void set${relation.nomEntity}(${relation.nomEntity}Dto ${relation.nomEntity?uncap_first}) {
        this.${relation.nomEntity?uncap_first} = ${relation.nomEntity?uncap_first};
    }
            </#if>
            <#if relation.relationJPAType.annotation == "@OneToMany" ||  relation.relationJPAType.annotation == "@ManyToMany">
    public Collection<${relation.nomEntity}Dto> get${relation.nomEntity}s(){
        return this.${relation.nomEntity?uncap_first}s;
    }
    public void set${relation.nomEntity}s(Collection<${relation.nomEntity}Dto> ${relation.nomEntity?uncap_first}s) {
        this.${relation.nomEntity?uncap_first}s = ${relation.nomEntity?uncap_first}s;
    }
            </#if>
        <#elseif relation.bidirectionnelle && relation.relationJPAType.annotation?? && relation.relationJPAType.annotation != "" && relation.source.nomEntity?? && relation.source.nomEntity != "" &&  ( relation.destination.nomEntity == entity.nomEntity || relation.source.nomEntity != relation.destination.nomEntity )>
            <#if relation.relationJPAType.inverseAnnotation == "@ManyToOne" || relation.relationJPAType.inverseAnnotation == "@OneToOne">
    @JsonIgnore
    public ${relation.source.nomEntity}Dto get${relation.source.nomEntity}() {
        return this.${relation.source.nomEntity?uncap_first};
    }
    public void set${relation.source.nomEntity}(${relation.source.nomEntity}Dto ${relation.source.nomEntity?uncap_first}) {
        this.${relation.source.nomEntity?uncap_first} = ${relation.source.nomEntity?uncap_first};
    }
            </#if>
            <#if relation.relationJPAType.inverseAnnotation == "@OneToMany" ||  relation.relationJPAType.inverseAnnotation == "@ManyToMany">
    @JsonIgnore
    public Collection<${relation.source.nomEntity}Dto> get${relation.source.nomEntity}s(){
        return this.${relation.source.nomEntity?uncap_first}s;
    }
    public void set${relation.source.nomEntity}s(Collection<${relation.source.nomEntity}Dto> ${relation.source.nomEntity?uncap_first}s) {
        this.${relation.source.nomEntity?uncap_first}s = ${relation.source.nomEntity?uncap_first}s;
    }
            </#if>
        </#if>
    </#list>
</#if>