<#if application?? && application.base?? && entity?? && entity.clePrimaire??>
package ${package}.domain;

import java.io.Serializable;
import java.util.*;
<#if application.lombok>
import lombok.*;
</#if>

<#if application.lombok>
@NoArgsConstructor
@Getter @Setter
</#if>
public class ${entity.clePrimaire.nomClePrimaire} implements Serializable {

<#if entity.clePrimaire.attributs??>
<#list entity.clePrimaire.attributs as attribut>
<#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
    ${attribut.visibiliteAttribut.valeur} ${attribut.attributType.nomType} ${attribut.nomAttribut};

</#if>
</#list>
</#if>
<#if entity.clePrimaire.attributs?? && !application.lombok>
<#list entity.clePrimaire.attributs as attribut>
<#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
    public ${attribut.attributType.nomType} get${attribut.nomAttribut?cap_first}() {
        return this.${attribut.nomAttribut};
    }

    public void set${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut}) {
        this.${attribut.nomAttribut} = ${attribut.nomAttribut};
    }

</#if>
</#list>
</#if>
}
</#if>

