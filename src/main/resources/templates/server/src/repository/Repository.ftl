<#if entity??>
package ${package}.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import ${package}.domain.${entity.nomEntity};
<#if entity.clePrimaire?? && entity.clePrimaire.attributs?? && entity.clePrimaire.attributs?size gt 1>
import ${package}.domain.${entity.clePrimaire.nomClePrimaire};
</#if>

public interface I${entity.nomEntity}Repository extends JpaRepository<${entity.nomEntity}, ${primarykey}>{
    <#if entity.attributs??>
        <#list entity.attributs as attribut>
            <#if attribut.nomAttribut?? && attribut.nomAttribut != "" && attribut.attributType.nomType?? && attribut.attributType.nomType != "">
                <#if attribut.findBy && attribut.unique >

    Optional<${entity.nomEntity}> findBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut});
                <#elseif attribut.findBy>

    Collection<${entity.nomEntity}> findBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut});
                </#if>
                <#if attribut.unique >

    boolean existsBy${attribut.nomAttribut?cap_first}(${attribut.attributType.nomType} ${attribut.nomAttribut});

    boolean existsBy${attribut.nomAttribut?cap_first}And${primaryKeyNom?cap_first}Not(${attribut.attributType.nomType} ${attribut.nomAttribut?uncap_first}, ${primarykey} ${primaryKeyNom?uncap_first});
                </#if>
            </#if>
        </#list>
    </#if>

}
</#if>

