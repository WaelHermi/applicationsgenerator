package tn.com.veganet.generateur.util;

public class BaseConstantes {

    public static String ID_ANNOTATION() {
        return "@Id";
    }

    public static String ENTITY_ANNOTATION() {
        return "@Entity";
    }

}
