package tn.com.veganet.generateur.util;

public class EntityNamingFunctions {

    public static String FromTableToEntityName(String name){

        if(name != null && name.length() > 1) {
            name = name.substring(0, 1).toUpperCase() + name.substring(1);

            while (name.indexOf("_") != -1) {
                int index = name.indexOf("_");
                name = name.replaceFirst("_", "");
                Character character = name.charAt(index);
                if (character != null) {
                    character = Character.toUpperCase(character);
                    char[] nameArray = name.toCharArray();
                    nameArray[index] = character;
                    name = new String(nameArray);
                }
            }
        }
        return name;
    }

    public static String FromTableColonneToEntityAttributName(String name){

        if(name != null && name.length() > 1) {

            name = name.substring(0, 1).toLowerCase() + name.substring(1);

            while (name.indexOf("_") != -1) {
                int index = name.indexOf("_");
                name = name.replaceFirst("_", "");
                Character character = name.charAt(index);
                if (character != null) {
                    character = Character.toUpperCase(character);
                    char[] nameArray = name.toCharArray();
                    nameArray[index] = character;
                    name = new String(nameArray);
                }
            }
        }
        return name;
    }

}
