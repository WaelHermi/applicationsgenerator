package tn.com.veganet.generateur.util;

public class GlobaleConstantes {

    private GlobaleConstantes() { }

    //--------------------------------------------------------------------------------------------
    // SHEMIN
    //--------------------------------------------------------------------------------------------
    public static final String JAVA_EMPLACEMENT = "/src/main/java/";

    public static final String DESTIONATION_APPLICATION_CHEMIN = "/home/hermi/Dev/generationtest/temp";
    public static final String SOURCE_APPLICATION_MONOLITHIQUE_CHEMIN = "/home/hermi/Dev/generationtest/server";

    //--------------------------------------------------------------------------------------------
    // APPLICATION
    //--------------------------------------------------------------------------------------------
    public static final String APPLICATION_XML = "application";
    public static final String GROUP_APPLICATION_XML = "group";
    public static final String ARTIFACT_APPLICATION_XML = "artifact";
    public static final String PORT_APPLICATION_XML = "port";
    public static final String DESCRIPTION_APPLICATION_XML = "description";
    public static final String TYPE_APPLICATION_XML = "applicationType";
    public static final String TYPE_AUTHENTIFICATION_XML = "authenticationType";
    public static final String PACKAGING_APPLICATION_XML = "packaging";
    public static final String VERSION_APPLICATION_XML = "version";
    public static final String LOMBOK_APPLICATION_XML = "lombok";

    //--------------------------------------------------------------------------------------------
    // ENTITY
    //--------------------------------------------------------------------------------------------
    public static final String ENTITIES_XML = "entities";
    public static final String ENTITY_XML = "entity";
    public static final String NOM_ENTITY_XML = "name";
    public static final String NOM_MERE_ENTITY_XML = "mother";
    public static final String INHERITANCE_ENTITY_XML = "inheritance";
    public static final String DISCRIMINATOR_COLUMN_ENTITY_XML = "discriminatorColumn";
    public static final String DISCRIMINATOR_VALUE_ENTITY_XML = "discriminatorValue";
    public static final String PRIMARY_KEY_JOIN_COLUMN_ENTITY_XML = "primaryKeyJoinColumn";
    public static final String CONTROLEUR_ENTITY_XML = "controleur";

    //--------------------------------------------------------------------------------------------
    // PRIMARY KEY
    //--------------------------------------------------------------------------------------------
    public static final String PRIMARY_KEY_XML = "primarykey";

    //--------------------------------------------------------------------------------------------
    // ATTRIBUT
    //--------------------------------------------------------------------------------------------
    public static final String ATTRIBUTS_XML = "attributes";
    public static final String ATTRIBUT_XML = "attribute";
    public static final String NOM_ATTRIBUT_XML = "name";
    public static final String VISIBILITE_ATTRIBUT_XML = "visibility";
    public static final String TYPE_ATTRIBUT_XML = "type";
    public static final String UNIQUE_ATTRIBUT_XML = "unique";
    public static final String NULLABLE_ATTRBUT_XML = "nullable";
    public static final String GENERATION_TYPE_ATTRIBUT_XML = "generationType";
    public static final String FIND_BY_ATTRIBUT_XML = "findBy";
    public static final String DTO_ATTRIBUT_XML = "dto";

    //--------------------------------------------------------------------------------------------
    // RELATION
    //--------------------------------------------------------------------------------------------
    public static final String RELATIONS_XML = "relations";
    public static final String RELATION_XML = "relation";
    public static final String NOM_RELATION_XML = "name";
    public static final String BI_NOM_RELATION_XML = "biName";
    public static final String TYPE_RELATION_XML = "type";
    public static final String ENTITY_RELATION_XML = "entity";
    public static final String CASCADES_XML = "cascades";
    public static final String CASCADE_TYPE_XML = "cascade";
    public static final String CASCADE_TYPE_NAME = "name";
    public static final String FIND_BY_RELATION_XML = "findBy";
    public static final String JSON_IGONRE_RELATION_XML = "jsonIgnore";
    public static final String FETCH_TYPE_XML = "fetch";
    public static final String BI_RELATION_XML = "bi";
    public static final String DTO_RELATION_XML = "dto";

    //--------------------------------------------------------------------------------------------
    // BASE
    //--------------------------------------------------------------------------------------------
    public static final String BASE_XML = "database";
    public static final String TYPE_BASE_XML = "type";
    public static final String NOM_BASE_XML = "name";
    public static final String USERNAME_BASE_XML = "username";
    public static final String PASSWORD_BASE_XML = "password";
    public static final String IP_BASE_XML = "ip";
    public static final  String PORT_BASE_XML = "port";

    //--------------------------------------------------------------------------------------------
    // TABLE
    //--------------------------------------------------------------------------------------------
    public final static String TABLE_CAT = "TABLE_CAT"; // table catalog (may be null)
    public final static String TABLE_NAME = "TABLE_NAME"; // table name
    public final static String TABLE_TYPE = "TABLE_TYPE"; // table type. Typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM".
    public final static String REMARKS = "REMARKS"; // explanatory comment on the table
    public final static String TYPE_NAME = "TYPE_NAME"; // type name (may be null)

    //--------------------------------------------------------------------------------------------
    // SCHEMA 
    //--------------------------------------------------------------------------------------------
    public final static String TABLE_CATALOG = "TABLE_CATALOG"; // table catalog (may be null)
    public final static String TABLE_SCHEM = "TABLE_SCHEM";   // table schema

    //--------------------------------------------------------------------------------------------
    // COLONNES TABLE
    //--------------------------------------------------------------------------------------------
    public final static String COLUMN_NAME = "COLUMN_NAME";
    public final static String DATA_TYPE = "DATA_TYPE"; // int : SQL type from java.sql.Types
    public final static String COLUMN_SIZE = "COLUMN_SIZE"; // int : column size. For char or date types this is the maximum number of characters, for numeric or decimal types this is precision.
    public final static String DECIMAL_DIGITS = "DECIMAL_DIGITS"; // int : the number of fractional digits
    public final static String NUM_PREC_RADIX = "NUM_PREC_RADIX"; // int : Radix (typically either 10 or 2)
    public final static String NULLABLE = "NULLABLE"; // int : is NULL allowed
    public final static String COLUMN_DEF = "COLUMN_DEF"; // String : default value (may be null)
    public final static String CHAR_OCTET_LENGTH = "CHAR_OCTET_LENGTH"; // int : for char types the maximum number of bytes in the column
    public final static String ORDINAL_POSITION = "ORDINAL_POSITION"; // int : index of column in table (starting at 1)

    //--------------------------------------------------------------------------------------------
    // COLONNE CLE PRIMAIRE
    //--------------------------------------------------------------------------------------------
    public final static String KEY_SEQ = "KEY_SEQ"; // short => sequence number within primary key
    public final static String PK_NAME = "PK_NAME"; // String => primary key name (may be null)

    //--------------------------------------------------------------------------------------------
    // COLONNE CLE ETRANGERE
    //--------------------------------------------------------------------------------------------
    public final static String PKTABLE_CAT = "PKTABLE_CAT"; // primary key table catalog being imported (may be null)
    public final static String PKTABLE_SCHEM = "PKTABLE_SCHEM"; // primary key table schema being imported (may be null)
    public final static String PKTABLE_NAME = "PKTABLE_NAME"; // primary key table name being imported
    public final static String PKCOLUMN_NAME = "PKCOLUMN_NAME"; // primary key column name being imported
    public final static String FKTABLE_CAT = "FKTABLE_CAT"; // foreign key table catalog (may be null)
    public final static String FKTABLE_SCHEM = "FKTABLE_SCHEM"; // foreign key table schema (may be null)
    public final static String FKTABLE_NAME = "FKTABLE_NAME"; // foreign key table name
    public final static String FKCOLUMN_NAME = "FKCOLUMN_NAME"; // foreign key column name
    public final static String UPDATE_RULE = "UPDATE_RULE"; // short => What happens to a foreign key when the primary key is updated:
    public final static String DELETE_RULE = "DELETE_RULE"; // short => What happens to the foreign key when primary is deleted.
    public final static String FK_NAME = "FK_NAME"; // String => foreign key name (may be null)
    public final static String DEFERRABILITY = "DEFERRABILITY"; // short => can the evaluation of foreign key constraints be deferred until commit

}