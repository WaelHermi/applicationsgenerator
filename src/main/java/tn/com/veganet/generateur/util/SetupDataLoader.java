package tn.com.veganet.generateur.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.*;
import tn.com.veganet.generateur.model.type.ApplicationType;
import tn.com.veganet.generateur.model.type.AuthentificationType;
import tn.com.veganet.generateur.model.type.BaseType;
import tn.com.veganet.generateur.model.type.DependanceScope;
import tn.com.veganet.generateur.repository.*;

import java.util.*;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent>  {

    boolean alreadySetup = false;

    @Autowired
    private IUtilisateurRepository utilisateurRepository;

    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IDependanceBaseRepository dependanceBaseRepository;

    @Autowired
    private IDependanceApplicationRepository dependanceApplicationRepository;

    @Autowired
    private IDependanceAuthentificationRepository dependanceAuthentificationRepository;

    @Autowired
    private IProprieteBaseRepository proprieteBaseRepository;

    @Autowired
    private IProprieteApplicationRepository proprieteApplicationRepository;

    @Autowired
    private IProprieteAuthentificationRepository proprieteAuthentificationRepository;

    @Autowired
    private IFonctionnaliteRepository fonctionnaliteRepository;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        Fonctionnalite fonctionnaliteGenerationApplication = createFonctionnaliteIfNotFound("100", "Générateur des API", "/generation/wizard", "browser-outline");
        Fonctionnalite fonctionnaliteGestionDependances = createFonctionnaliteIfNotFound("200", "Gestion des dépendances", "/gestion/dependance", "external-link-outline");
        Fonctionnalite fonctionnaliteGestionProrietes = createFonctionnaliteIfNotFound("300", "Gestion des propriétés", "/gestion/propriete", "file-text-outline");
        Fonctionnalite fonctionnaliteCheckTraceAcces = createFonctionnaliteIfNotFound("400", "Historique d'accès", "/administration/trace_acces", "arrow-circle-right-outline");
        Fonctionnalite fonctionnaliteCheckTraceDonnes = createFonctionnaliteIfNotFound("500", "Historique des opérations", "/administration/trace_donnees", "checkmark-circle-outline");
        Fonctionnalite fonctionnaliteGestionUtilisateurs = createFonctionnaliteIfNotFound("600", "Gestion des utilisateurs", "/administration/utilisateurs", "people-outline");


        Collection<Fonctionnalite> adminFonctionnalites = Arrays.asList(
                fonctionnaliteGestionUtilisateurs, fonctionnaliteCheckTraceAcces, fonctionnaliteCheckTraceDonnes);

        Collection<Fonctionnalite> clientFonctionnalites = Arrays.asList(
                fonctionnaliteGenerationApplication);

        Collection<Fonctionnalite> gestionnaireFonctionnalites = Arrays.asList(fonctionnaliteGestionDependances, fonctionnaliteGestionProrietes);

        createRoleIfNotFound("ROLE_ADMINISTRATEUR",adminFonctionnalites);
        createRoleIfNotFound("ROLE_CLIENT", clientFonctionnalites);
        createRoleIfNotFound("ROLE_GESTIONNAIRE", gestionnaireFonctionnalites);


        Optional<Role> adminRole = roleRepository.findByNomRole("ROLE_ADMINISTRATEUR");
        Utilisateur adminUtilisateur = new Utilisateur();
        adminUtilisateur.setPrenomUtilisateur("Wael");
        adminUtilisateur.setNomUtilisateur("HERMI");
        adminUtilisateur.setPasswordUtilisateur(passwordEncoder.encode("12345"));
        adminUtilisateur.setAdresseMailUtilisateur("HermiWael25@gmail.com");
        adminUtilisateur.setTelephoneUtilisateur("93558585");
        adminUtilisateur.setUsernameUtilisateur("WaelHermi");
        adminUtilisateur.setRoles(adminRole.isPresent() ? Arrays.asList(adminRole.get()) : null);
        adminUtilisateur.setActive(true);
        createUtilisateurIfNotFount(adminUtilisateur);

        Optional<Role> gestionnaireRole = roleRepository.findByNomRole("ROLE_GESTIONNAIRE");
        Utilisateur gestionnaireUtilisateur = new Utilisateur();
        gestionnaireUtilisateur.setPrenomUtilisateur("Khalil");
        gestionnaireUtilisateur.setNomUtilisateur("BRAMLI");
        gestionnaireUtilisateur.setPasswordUtilisateur(passwordEncoder.encode("12345"));
        gestionnaireUtilisateur.setAdresseMailUtilisateur("KhalilBramli@gmail.com");
        gestionnaireUtilisateur.setTelephoneUtilisateur("90123123");
        gestionnaireUtilisateur.setUsernameUtilisateur("KhalilBramli");
        gestionnaireUtilisateur.setRoles(gestionnaireRole.isPresent() ? Arrays.asList(gestionnaireRole.get()) : null);
        gestionnaireUtilisateur.setActive(true);
        createUtilisateurIfNotFount(gestionnaireUtilisateur);

        Optional<Role> clientRole = roleRepository.findByNomRole("ROLE_CLIENT");
        Utilisateur clientUtilisateur = new Utilisateur();
        clientUtilisateur.setPrenomUtilisateur("Oussama");
        clientUtilisateur.setNomUtilisateur("DRIDI");
        clientUtilisateur.setPasswordUtilisateur(passwordEncoder.encode("12345"));
        clientUtilisateur.setAdresseMailUtilisateur("Oussama.Dridi@veganet.tn.com");
        clientUtilisateur.setTelephoneUtilisateur("93558585");
        clientUtilisateur.setUsernameUtilisateur("OussamaDridi");
        clientUtilisateur.setRoles(clientRole.isPresent() ? Arrays.asList(clientRole.get()) : null);
        clientUtilisateur.setActive(true);
        createUtilisateurIfNotFount(clientUtilisateur);

        // PostgreSQL
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.datasource.username", "");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.datasource.password", "");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.jpa.database", "POSTGRESQL");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.datasource.platform", "postgres");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.datasource.url", "jdbc:postgresql://");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.jpa.show-sql", "true");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.jpa.generate-ddl", "true");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.jpa.hibernate.ddl-auto", "create");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation", "true");
        createProprietesDatabaseIfNotFound(BaseType.POSTGRESQL, "spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        createDependancesDatabaseIfNotFound(BaseType.POSTGRESQL, "org.postgresql", "postgresql", "", DependanceScope.COMPILE);

        // MySQL
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.datasource.username", "");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.datasource.password", "");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.jpa.database", "MYSQL");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.datasource.platform", "mysql");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.datasource.url", "jdbc:mysql:///MaTable?createDatabaseIfNotExist=true");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.jpa.show-sql", "true");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.jpa.generate-ddl", "true");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.jpa.hibernate.ddl-auto", "update");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation", "true");
        createProprietesDatabaseIfNotFound(BaseType.MYSQL, "spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        createDependancesDatabaseIfNotFound(BaseType.MYSQL,  "mysql", "mysql-connector-java", "", DependanceScope.COMPILE);

        // General dependances
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE, "org.springframework.boot", "spring-boot-starter-web", "", DependanceScope.COMPILE);
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE, "org.springframework.boot", "spring-boot-starter-data-jpa", "", DependanceScope.COMPILE);
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE, "org.springframework.boot", "spring-boot-starter-actuator", "", DependanceScope.COMPILE);
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE, "io.springfox", "springfox-swagger2", "2.9.2", DependanceScope.COMPILE);
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE, "io.springfox", "springfox-swagger-ui", "2.9.2", DependanceScope.COMPILE);
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE, "org.projectlombok", "lombok", "1.18.10", DependanceScope.PROVIDED);
        createDependancesApplicationIfNotFound(ApplicationType.MONOLITHIQUE,"org.modelmapper", "modelmapper", "2.3.5", DependanceScope.COMPILE);

        // Authentication dependances
        createDependancesAuthentificationIfNotFound(AuthentificationType.JWT, "org.springframework.boot", "spring-boot-starter-security", "", DependanceScope.COMPILE);
        createDependancesAuthentificationIfNotFound(AuthentificationType.JWT, "io.jsonwebtoken", "jjwt", "0.9.1", DependanceScope.COMPILE);

        alreadySetup = true;
    }

    public void createUtilisateurIfNotFount(Utilisateur utilisateur){
        if(!utilisateurRepository.existsByUsernameUtilisateur(utilisateur.getUsernameUtilisateur())){
            utilisateurRepository.save(utilisateur);
        }
    }

    @Transactional
    public Fonctionnalite createFonctionnaliteIfNotFound(String codeFonctionnalite, String designationFonctionnalite, String urlFonctionnalite, String iconFonctionnalite) {

        Fonctionnalite fonctionnalite = new Fonctionnalite();
        fonctionnalite.setCodeFonctionnalite(codeFonctionnalite);
        fonctionnalite.setDesignationFonctionnalite(designationFonctionnalite);
        fonctionnalite.setUrlFonctionnalite(urlFonctionnalite);
        fonctionnalite.setIconFonctionnalite(iconFonctionnalite);

        Optional<Fonctionnalite> fonctionnaliteOptional = fonctionnaliteRepository.findByDesignationFonctionnalite(designationFonctionnalite);

        if (!fonctionnaliteOptional.isPresent()) {
            fonctionnalite = fonctionnaliteRepository.save(fonctionnalite);
        }else{
            fonctionnalite = fonctionnaliteOptional.get();
        }
        return fonctionnalite;
    }

    @Transactional
    Role createRoleIfNotFound(
            String name, Collection<Fonctionnalite> fonctionnalites) {

        Optional<Role> roleOptional = roleRepository.findByNomRole(name);
        if (!roleOptional.isPresent()) {
            Role role = new Role();
            role.setNomRole(name);
            role.setFonctionnalites(fonctionnalites);
            roleRepository.save(role);
            roleOptional = Optional.of(role);
        }
        return roleOptional.get();
    }

    @Transactional
    DependanceApplication createDependancesApplicationIfNotFound(ApplicationType applicationType, String groupDependance, String artifactDependance, String versionDependance, DependanceScope dependanceScope) {

        DependanceApplication dependanceApplication = new DependanceApplication();
        dependanceApplication.setApplicationType(applicationType);
        dependanceApplication.setGroupDependance(groupDependance);
        dependanceApplication.setArtifactDependance(artifactDependance);
        dependanceApplication.setVersionDependance(versionDependance);
        dependanceApplication.setDependanceScope(dependanceScope);

        Optional<DependanceApplication> dependanceApplicationOptional = dependanceApplicationRepository.findByApplicationTypeAndGroupDependanceAndArtifactDependance(dependanceApplication.getApplicationType(), dependanceApplication.getGroupDependance(), dependanceApplication.getArtifactDependance());
        if (!dependanceApplicationOptional.isPresent()) {
            dependanceApplicationRepository.save(dependanceApplication);
        }
        return dependanceApplication;
    }

    @Transactional
    DependanceBase createDependancesDatabaseIfNotFound(BaseType baseType, String groupDependance, String artifactDependance, String versionDependance, DependanceScope dependanceScope) {

        DependanceBase dependanceBase = new DependanceBase();
        dependanceBase.setBaseType(baseType);
        dependanceBase.setGroupDependance(groupDependance);
        dependanceBase.setArtifactDependance(artifactDependance);
        dependanceBase.setVersionDependance(versionDependance);
        dependanceBase.setDependanceScope(dependanceScope);

        Optional<DependanceBase> dependanceBaseOptional = dependanceBaseRepository.findByBaseTypeAndGroupDependanceAndArtifactDependance(dependanceBase.getBaseType(), dependanceBase.getGroupDependance(), dependanceBase.getArtifactDependance());
        if (!dependanceBaseOptional.isPresent()) {
            dependanceBaseRepository.save(dependanceBase);
        }
        return dependanceBase;
    }

    @Transactional
    DependanceAuthentification createDependancesAuthentificationIfNotFound(AuthentificationType authentificationType, String groupDependance, String artifactDependance, String versionDependance, DependanceScope dependanceScope) {

        DependanceAuthentification dependanceAuthentification = new DependanceAuthentification();
        dependanceAuthentification.setAuthentificationType(authentificationType);
        dependanceAuthentification.setGroupDependance(groupDependance);
        dependanceAuthentification.setArtifactDependance(artifactDependance);
        dependanceAuthentification.setVersionDependance(versionDependance);
        dependanceAuthentification.setDependanceScope(dependanceScope);

        Optional<DependanceAuthentification> dependanceAuthentificationOptional = dependanceAuthentificationRepository.findByAuthentificationTypeAndGroupDependanceAndArtifactDependance(dependanceAuthentification.getAuthentificationType(), dependanceAuthentification.getGroupDependance(), dependanceAuthentification.getArtifactDependance());
        if (!dependanceAuthentificationOptional.isPresent()) {
            dependanceAuthentificationRepository.save(dependanceAuthentification);
        }
        return dependanceAuthentification;
    }

    @Transactional
    ProprieteApplication createProprietesApplicationIfNotFound(ApplicationType applicationType, String nomPropriete, String valeurPropriete) {

        ProprieteApplication proprieteApplication = new ProprieteApplication();
        proprieteApplication.setApplicationType(applicationType);
        proprieteApplication.setNomPropriete(nomPropriete);
        proprieteApplication.setValeurPropriete(valeurPropriete);

        Optional<ProprieteApplication> proprieteApplicationOptional = proprieteApplicationRepository.findByApplicationTypeAndNomPropriete(proprieteApplication.getApplicationType(), proprieteApplication.getNomPropriete());
        if (!proprieteApplicationOptional.isPresent()) {
            proprieteApplicationRepository.save(proprieteApplication);
        }
        return proprieteApplication;
    }

    @Transactional
    ProprieteBase createProprietesDatabaseIfNotFound(BaseType baseType, String nomPropriete, String valeurPropriete) {

        ProprieteBase proprieteBase = new ProprieteBase();
        proprieteBase.setBaseType(baseType);
        proprieteBase.setNomPropriete(nomPropriete);
        proprieteBase.setValeurPropriete(valeurPropriete);

        Optional<ProprieteBase> proprieteBaseOptional = proprieteBaseRepository.findByBaseTypeAndNomPropriete(proprieteBase.getBaseType(), proprieteBase.getNomPropriete());
        if (!proprieteBaseOptional.isPresent()) {
            proprieteBaseRepository.save(proprieteBase);
        }
        return proprieteBase;
    }

    @Transactional
    ProprieteAuthentification createProprietesAuthentificationIfNotFound(AuthentificationType authentificationType, String nomPropriete, String valeurPropriete) {

        ProprieteAuthentification proprieteAuthentification = new ProprieteAuthentification();
        proprieteAuthentification.setAuthentificationType(authentificationType);
        proprieteAuthentification.setNomPropriete(nomPropriete);
        proprieteAuthentification.setValeurPropriete(valeurPropriete);

        Optional<ProprieteAuthentification> proprieteAuthentificationOptional = proprieteAuthentificationRepository.findByAuthentificationTypeAndNomPropriete(proprieteAuthentification.getAuthentificationType(), proprieteAuthentification.getNomPropriete());
        if (!proprieteAuthentificationOptional.isPresent()) {
            proprieteAuthentificationRepository.save(proprieteAuthentification);
        }
        return proprieteAuthentification;
    }


}