package tn.com.veganet.generateur.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;
import java.sql.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tn.com.veganet.generateur.model.type.ApplicationType;

@Entity
@DiscriminatorValue("TYPE_APPLICATION")
@NoArgsConstructor
@Getter @Setter
public class ProprieteApplication extends Propriete implements Serializable {

    @Enumerated(EnumType.STRING)
    private ApplicationType applicationType;

}

