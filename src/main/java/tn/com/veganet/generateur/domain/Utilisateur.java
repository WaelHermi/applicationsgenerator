package tn.com.veganet.generateur.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter @Setter
public class Utilisateur implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idUtilisateur;

    @Column(nullable = false)
    private String adresseMailUtilisateur;

    private String adresseUtilisateur;

    private String telephoneUtilisateur;

    @Column(nullable = false)
    private String passwordUtilisateur;

    @Column(unique = true, nullable = false)
    private String usernameUtilisateur;

    @Column(nullable = false)
    private String prenomUtilisateur;

    @Column(nullable = false)
    private String nomUtilisateur;

    private Date dateDeNaissanceUtilisateur;

    private Boolean active;

    @ManyToMany()
    @JoinTable(joinColumns = { @JoinColumn(name = "idUtilisateur") }, inverseJoinColumns = { @JoinColumn(name = "idRole") })
    private Collection<Role> roles;

    @OneToMany(mappedBy="utilisateur")
    @Getter(onMethod = @__( @JsonIgnore))
    private Collection<TraceDonnees> traceDonneess;

    @OneToMany(mappedBy="utilisateur")
    @Getter(onMethod = @__( @JsonIgnore))
    private Collection<GeneratedProject> generatedProjects;

}

