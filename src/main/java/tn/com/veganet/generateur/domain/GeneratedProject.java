package tn.com.veganet.generateur.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter @Setter
public class GeneratedProject implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idGeneratedProject;

    private String typeGeneratedProject;

    @Column(unique = true, nullable = false)
    private String urlGeneratedProject;

    @Column(nullable = false)
    private String nomGeneratedProject;

    private Timestamp date;

    @ManyToOne()
    @JoinColumn(name = "idUtilisateur")
    private Utilisateur utilisateur;


}

