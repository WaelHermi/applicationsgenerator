package tn.com.veganet.generateur.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter @Setter
public class Fonctionnalite implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idFonctionnalite;

    private String iconFonctionnalite;

    @Column(unique = true)
    private String urlFonctionnalite;

    @Column(unique = true, nullable = false)
    private String designationFonctionnalite;

    @Column(unique = true, nullable = false)
    private String codeFonctionnalite;


}

