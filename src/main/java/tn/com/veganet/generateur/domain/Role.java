package tn.com.veganet.generateur.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter @Setter
public class Role implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idRole;

    @Column(unique = true, nullable = false)
    private String nomRole;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(joinColumns = { @JoinColumn(name = "idRole") }, inverseJoinColumns = { @JoinColumn(name = "idFonctionnalite") })
    private Collection<Fonctionnalite> fonctionnalites;

}

