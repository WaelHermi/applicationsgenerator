package tn.com.veganet.generateur.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;
import java.sql.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tn.com.veganet.generateur.model.type.DependanceScope;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Type")
@NoArgsConstructor
@Getter @Setter
public class Dependance implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idDependance;

    private String versionDependance;

    @Column(nullable = false)
    private String artifactDependance;

    @Column(nullable = false)
    private String groupDependance;

    @Enumerated(EnumType.STRING)
    DependanceScope dependanceScope;


}

