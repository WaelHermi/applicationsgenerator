package tn.com.veganet.generateur.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;
import java.sql.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tn.com.veganet.generateur.model.type.AuthentificationType;

@Entity
@DiscriminatorValue("TYPE_AUTHENTIFICATION")
@NoArgsConstructor
@Getter @Setter
public class ProprieteAuthentification extends Propriete implements Serializable {

    @Enumerated(EnumType.STRING)
    private AuthentificationType authentificationType;

}

