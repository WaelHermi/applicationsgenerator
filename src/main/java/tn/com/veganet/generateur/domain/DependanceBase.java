package tn.com.veganet.generateur.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;
import java.sql.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tn.com.veganet.generateur.model.type.BaseType;

@Entity
@DiscriminatorValue("TYPE_BASE")
@NoArgsConstructor
@Getter @Setter
public class DependanceBase extends Dependance implements Serializable {

    @Enumerated(EnumType.STRING)
    private BaseType baseType;
}

