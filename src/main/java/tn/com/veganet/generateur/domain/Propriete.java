package tn.com.veganet.generateur.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;
import java.sql.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Type")
@NoArgsConstructor
@Getter @Setter
public class Propriete implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idPropriete;

    @Column(nullable = false)
    private String valeurPropriete;

    @Column(nullable = false)
    private String nomPropriete;

}

