package tn.com.veganet.generateur.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter @Setter
public class TraceDonnees implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idTraceDonnees;

    private Timestamp date;

    private String operation;

    private String versionNavigateur;

    private String navigateur;

    private String ip;

    @ManyToOne()
    @JoinColumn(name = "idUtilisateur")
    private Utilisateur utilisateur;

}

