package tn.com.veganet.generateur.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter @Setter
public class TraceAcces implements Serializable {

    @Id
    @GeneratedValue()
    private UUID idTraceAcces;

    private Timestamp date;

    private String versionNavigateur;

    private String navigateur;

    private String ip;

    private Boolean status;

    private String identifiant;


}

