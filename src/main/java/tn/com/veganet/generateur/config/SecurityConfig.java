package tn.com.veganet.generateur.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import tn.com.veganet.generateur.filter.JwtRequestFilter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/authenticate").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/client").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/SendEmailToResetPassword/**").permitAll()
                .antMatchers("/v1/generateur/**").hasRole("CLIENT")
                .antMatchers("/v1/type/**").hasAnyRole("CLIENT", "GESTIONNAIRE")
                .antMatchers("/v1/generatedProject**").hasRole("CLIENT")
                .antMatchers("/v1/generatedProjects/getByUtilisateur").hasRole("CLIENT")
                .antMatchers("/v1/dependance**").hasRole("GESTIONNAIRE")
                .antMatchers("/v1/propriete**").hasRole("GESTIONNAIRE")
                .antMatchers("/v1/fonctionnalite**").hasRole("ADMINISTRATEUR")
                .antMatchers("/v1/traceAccess**").hasRole("ADMINISTRATEUR")
                .antMatchers("/v1/traceDonnees**").hasRole("ADMINISTRATEUR")
                .antMatchers("/v1/utilisateur**").hasRole("ADMINISTRATEUR")
                .antMatchers("/v1/role**").hasRole("ADMINISTRATEUR")
                .antMatchers(HttpMethod.PUT, "/v1/utilisateur/resetPassword").permitAll()
                .anyRequest().authenticated().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable();
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/**", "/swagger-ui.html", "/webjars/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
