package tn.com.veganet.generateur.compresseur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tn.com.veganet.generateur.util.GlobaleConstantes;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
public class Compresseur {

    private Compresseur() {
    }

    public static String zipDirectory(String parentDirectory) throws IOException {

        String sourceFile = GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory;
        FileOutputStream fos = new FileOutputStream(GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory +".zip");
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(sourceFile);
        zipFile(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fos.close();

        return GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory + ".zip";
    }

    protected static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {


        if (fileToZip.isHidden()) {
            return;
        }
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }
}
