package tn.com.veganet.generateur.extracteur.xml;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import tn.com.veganet.generateur.model.application.Entity;
import tn.com.veganet.generateur.model.application.Relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Primary
public class ExtracteurUtil {

    public List<Entity> affecterEntitiesToRelations(List<Entity> entities){

        for (Entity entity: entities
        ) {

            if(entity.getRelations() != null) {
                for (Relation relation : entity.getRelations()
                ) {
                    entities.stream().filter(value -> value.getNomEntity().equals(relation.getNomEntity())).findFirst().ifPresent(value -> {
                        if (relation.getDestination() == null) {
                            relation.setDestination(value);
                            if (!relation.getDestination().getNomEntity().equals(entity.getNomEntity())) {
                                relation.setSource(entity);
                                if(relation.getDestination().getRelations() == null){
                                    relation.getDestination().setRelations(new ArrayList<>());
                                }
                                relation.getDestination().getRelations().removeIf( relationElement -> relationElement.getNomEntity().equals(relation.getNomEntity()));
                                if(!(relation.getDestination().getNomEntity().equals(relation.getNomEntity()))){
                                    relation.getDestination().getRelations().add(relation);
                                }
                            } else if (relation.getDestination().getNomEntity().equals(entity.getNomEntity()) && relation.getSource() == null) {
                                relation.setSource(entity);
                            }
                        }
                    });
                }
            }

        }

        return entities;
    }

    public List<Entity> affecterExtendedEntities(List<Entity> entities) {

        if(entities != null) {
            for (Entity entity : entities) {
                entities.stream().filter(value -> value.getNomEntity().equals(entity.getNomMere())).findFirst().ifPresent(value -> entity.setMere(value));
            }
        }

        return entities;
    }

}
