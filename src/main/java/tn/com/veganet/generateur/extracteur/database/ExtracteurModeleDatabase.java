package tn.com.veganet.generateur.extracteur.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tn.com.veganet.generateur.model.application.Base;
import tn.com.veganet.generateur.model.application.Entity;
import tn.com.veganet.generateur.model.application.Relation;
import tn.com.veganet.generateur.model.database.*;
import tn.com.veganet.generateur.model.database.metadata.ColonneCleEtrangereMetaData;
import tn.com.veganet.generateur.model.database.metadata.ColonneClePrimaireMetaData;
import tn.com.veganet.generateur.model.database.metadata.ColonneMetaData;
import tn.com.veganet.generateur.model.database.metadata.TableMetaData;
import tn.com.veganet.generateur.model.type.BaseColonneType;
import tn.com.veganet.generateur.model.type.InheritanceType;
import tn.com.veganet.generateur.model.type.RelationJPAType;
import tn.com.veganet.generateur.repository.IProprieteBaseRepository;
import tn.com.veganet.generateur.util.EntityNamingFunctions;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class ExtracteurModeleDatabase implements IExtracteurModeleDatabase {

    @Autowired
    private IProprieteBaseRepository proprieteBaseRepository;

    @Override
    public List<Entity> getEntitiesFromDatabaseTables(Base base) throws SQLException {

        List<Entity> entities = new ArrayList<>();

        List<Table> tables = getDatabaseTables(base);

        for (Table table: tables
        ) {
            Entity entity = table.getEntity();
            getClePrimaireInformations(entity, table);
            entities.add(entity);
        }
        getRelationForEntity(entities, tables);
        entities.removeIf( element -> element.getClePrimaire() == null || element.getClePrimaire().getAttributs() == null || element.getClePrimaire().getAttributs().size() == 0 );
        return entities;
    }

    @Override
    public Boolean getConnection(Base base) {

            String url = "jdbc:" + base.getBaseType().getConnector() + "://" + base.getIpBase() +"/"+ base
                .getNomBase();
            System.out.println(url);

        try(Connection connection = DriverManager.getConnection(url, base.getUsernameBase(), base.getPasswordBase())) {
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void getClePrimaireInformations(Entity entity, Table table) {

        if(entity != null && entity.getClePrimaire() != null && entity.getClePrimaire().getAttributs() != null) {
            entity.getClePrimaire().getAttributs().forEach( attributClePrimaire -> {
                entity.getAttributs().removeIf( attribut -> attribut.getNomAttribut().equals(attributClePrimaire.getNomAttribut()) );
            });
        }
    }


    private void getRelationForEntity(List<Entity> entities, List<Table> tables){

        for (Table table: tables) {

            Integer sizeColonnesCleEtrangere = 0;

            for (CleEtrangere cleEtrangere: table.getCleEtrangeres()) {
                sizeColonnesCleEtrangere = sizeColonnesCleEtrangere + cleEtrangere.getColonneCleEtrangereList().size();
            }

            if(table.getCleEtrangeres() != null && table.getCleEtrangeres().size() > 0) {

                if (sizeColonnesCleEtrangere == table.getColonnes().size() && table.getColonnes().size() == 2) {
                    getManyToManyRelation(entities, table);
                } else {

                    for (CleEtrangere cleEtrangere: table.getCleEtrangeres()) {

                        Optional<ColonneCleEtrangere> colonneCleEtrangereOptional =  cleEtrangere != null && cleEtrangere.getColonneCleEtrangereList() != null ? cleEtrangere.getColonneCleEtrangereList().stream().findFirst() : null;
                        Optional<ColonneClePrimaire> colonneClePrimaireOptional = table.getClePrimaire() != null && table.getClePrimaire().getColonneClePrimaires() != null ? table.getClePrimaire().getColonneClePrimaires().stream().findFirst() : null;

                        if ( colonneClePrimaireOptional.isPresent() && colonneCleEtrangereOptional.isPresent()
                                &&  colonneCleEtrangereOptional.get().getColonneCleEtrangereMetaData().getNomTableCleEtrangere().equals(colonneClePrimaireOptional.get().getColonneClePrimaireMetaData().getNomTable())
                                && !colonneCleEtrangereOptional.get().getColonneCleEtrangereMetaData().getNomTableClePrimaire().equals(table.getTableMetaData().getNomTable())
                                && colonneCleEtrangereOptional.get().getColonneCleEtrangereMetaData().getNomColonneClePrimaire().equals(colonneClePrimaireOptional.get().getColonneClePrimaireMetaData().getNomcolumn()) )
                        {
                                getJoinedHeritage(entities, cleEtrangere);

                        } else {
                            getOneToManyRelation(entities, cleEtrangere);
                        }
                    }

                }
            }
        }
    }

    private void getJoinedHeritage(List<Entity> entities, CleEtrangere cleEtrangere){

        Optional<Entity> filleEntityOptional =  entities.stream().filter( element -> element.getNomEntity().equals(EntityNamingFunctions.FromTableToEntityName(cleEtrangere.getColonneCleEtrangereList().get(0).getColonneCleEtrangereMetaData().getNomTableCleEtrangere()))).findFirst();

        if(filleEntityOptional.isPresent()){
            filleEntityOptional.get().setNomMere(EntityNamingFunctions.FromTableToEntityName(cleEtrangere.getColonneCleEtrangereList().get(0).getColonneCleEtrangereMetaData().getNomTableClePrimaire()));
            filleEntityOptional.get().setInheritanceType(InheritanceType.JOINED);
        }

    }

    private void getOneToManyRelation(List<Entity> entities, CleEtrangere cleEtrangere){

            Relation relation = Relation.getRelationForDatabase(RelationJPAType.MANYTOONE);
            Optional<Entity> sourceEntityOptional =  entities.stream().filter( element -> element.getNomEntity().equals(EntityNamingFunctions.FromTableToEntityName(cleEtrangere.getColonneCleEtrangereList().get(0).getColonneCleEtrangereMetaData().getNomTableCleEtrangere()))).findFirst();
            Optional<Entity> destinationEntityOptional =  entities.stream().filter( element -> element.getNomEntity().equals(EntityNamingFunctions.FromTableToEntityName(cleEtrangere.getColonneCleEtrangereList().get(0).getColonneCleEtrangereMetaData().getNomTableClePrimaire()))).findFirst();

            if(sourceEntityOptional.isPresent() && destinationEntityOptional.isPresent()) {
                relation.setNomEntity(destinationEntityOptional.get().getNomEntity());

                relation.setNomRelation(EntityNamingFunctions.FromTableColonneToEntityAttributName(destinationEntityOptional.get().getNomEntity()));
                relation.setBiNomRelation(EntityNamingFunctions.FromTableColonneToEntityAttributName(sourceEntityOptional.get().getNomEntity()) + 's');

                if(sourceEntityOptional.get().getRelations() != null){
                    sourceEntityOptional.get().getRelations().add(relation);
                }else{
                    sourceEntityOptional.get().setRelations(new ArrayList<Relation>(){{ add(relation); }});
                }
                cleEtrangere.getColonneCleEtrangereList().forEach( colonneCleEtrangereElement -> {
                    sourceEntityOptional.get().getAttributs().removeIf( entityElement ->
                            entityElement.getNomAttribut().equals(EntityNamingFunctions.FromTableColonneToEntityAttributName(colonneCleEtrangereElement.getColonneCleEtrangereMetaData().getNomColonneCleEtrangere())));
                        });
            }
    }

    private void getManyToManyRelation(List<Entity> entities, Table table){

        Relation relation = Relation.getRelationForDatabase(RelationJPAType.MANYTOMANY);

        Optional<Entity> sourceEntityOptional =  entities.stream().filter( element -> element.getNomEntity().equals(EntityNamingFunctions.FromTableToEntityName(table.getCleEtrangeres().get(0).getColonneCleEtrangereList().get(0).getColonneCleEtrangereMetaData().getNomTableClePrimaire()))).findFirst();
        Optional<Entity> destinationEntityOptional =  entities.stream().filter( element -> element.getNomEntity().equals(EntityNamingFunctions.FromTableToEntityName(table.getCleEtrangeres().get(1).getColonneCleEtrangereList().get(0).getColonneCleEtrangereMetaData().getNomTableClePrimaire()))).findFirst();

        if(sourceEntityOptional.isPresent() && destinationEntityOptional.isPresent()) {
            // relation.setDestination(destinationEntityOptional.get());
            relation.setNomEntity(destinationEntityOptional.get().getNomEntity());

            relation.setNomRelation(EntityNamingFunctions.FromTableColonneToEntityAttributName(destinationEntityOptional.get().getNomEntity() + 's'));
            relation.setBiNomRelation(EntityNamingFunctions.FromTableColonneToEntityAttributName(sourceEntityOptional.get().getNomEntity() + 's'));

            if(sourceEntityOptional.get().getRelations() != null){
                sourceEntityOptional.get().getRelations().add(relation);
            }else{
                sourceEntityOptional.get().setRelations(new ArrayList<Relation>(){{ add(relation); }});
            }
            Optional<Entity> relationEntityOptional = entities.stream().filter(element -> element.getNomEntity().equals(EntityNamingFunctions.FromTableToEntityName(table.getTableMetaData().getNomTable()))).findFirst();
            System.out.println(relationEntityOptional.get().getNomEntity());
            if(relationEntityOptional.isPresent()){
                entities.remove(relationEntityOptional.get());
            }
        }

    }

    private List<Table> getDatabaseTables(Base base) throws SQLException {

        List<Table> tables = new ArrayList<>();
        String url = "jdbc:postgresql://"+ base.getIpBase() +"/"+ base
                .getNomBase();

        try(Connection connection = DriverManager.getConnection(url, base.getUsernameBase(), base.getPasswordBase())) {

            DatabaseMetaData databaseMetaData = connection.getMetaData();
            tables = getTables(databaseMetaData, connection);

        } catch (SQLException e) {
            throw e;
        }

        return tables;
    }

    private List<Table> getTables(DatabaseMetaData databaseMetaData, Connection connection) throws SQLException {


        List<Table> tables = new ArrayList<>();
        ResultSet rsTables = databaseMetaData.getTables(null, null, "%", new String[]{"TABLE","VIEW"});

        while (rsTables.next()){

            TableMetaData tableMetaData = new TableMetaData(rsTables);
            Table table = new Table(tableMetaData);

            table.setClePrimaire(new ClePrimaire(getColonnesClePrimaire(databaseMetaData, tableMetaData.getNomTable())));
            table.setColonnes(getColonnes(databaseMetaData, tableMetaData.getNomTable()));
            table.setCleEtrangeres(getCleEtrangeres(databaseMetaData, tableMetaData.getNomTable()));
            List<String> autoIncrementedColumns = getAutoIncrementedColumns(connection,  null, table.getTableMetaData().getNomTable());
            // System.out.println(autoIncrementedColumns);
            getColonnesUnique(connection, table)
                    .forEach( colonneName ->  table.getColonnes().stream().
                            filter( colonneTable -> colonneTable.getColonneMetaData().getNomColonne().equals(colonneName)).findFirst().ifPresent( colonneTable -> colonneTable.setUnique(true) ));
            tables.add(table);
        }

        return tables;
    }

    private List<String> getColonnesUnique(Connection connection,  Table table) throws SQLException {

        Statement statement = connection.createStatement();

        List<String> nameColonnes = new ArrayList<>();

        ResultSet resultSet = statement.executeQuery(
                "SELECT cu.COLUMN_NAME " +
                        "FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc " +
                        "    inner join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE cu " +
                        "        on cu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME " +
                        "where " +
                        "    tc.CONSTRAINT_TYPE = 'UNIQUE'" +
                        "    and tc.TABLE_NAME = '" + table.getTableMetaData().getNomTable() + "'");
        while (resultSet.next()){
                nameColonnes.add(resultSet.getString("column_name"));
        }

        return nameColonnes;
    }

    private List<ColonneClePrimaire> getColonnesClePrimaire(DatabaseMetaData databaseMetaData, String nomTable) throws SQLException {

        ResultSet rsColonnesClePrimaireMetaData = databaseMetaData.getPrimaryKeys(null, null, nomTable);
        List<ColonneClePrimaire> colonnesClePrimaire = new ArrayList<>();

        while(rsColonnesClePrimaireMetaData.next()){

            ColonneClePrimaireMetaData colonneClePrimaireMetaData = new ColonneClePrimaireMetaData(rsColonnesClePrimaireMetaData);
            ColonneClePrimaire colonneClePrimaire = new ColonneClePrimaire();
            colonneClePrimaire. setColonneClePrimaireMetaData(colonneClePrimaireMetaData);
            colonnesClePrimaire.add(colonneClePrimaire);
        }

        rsColonnesClePrimaireMetaData.close();

        return colonnesClePrimaire;
    }

    private List<Colonne> getColonnes(DatabaseMetaData databaseMetaData, String nomTable) {

        List<Colonne> colonnes = new ArrayList<>();

        try(ResultSet rsColonnesMetaData = databaseMetaData.getColumns(null, null, nomTable, "%")) {
            while(rsColonnesMetaData.next()){

                ColonneMetaData colonneMetaData = new ColonneMetaData(rsColonnesMetaData);
                Colonne colonne = new Colonne();
                colonne.setColonneMetaData(colonneMetaData);
                //colonne.setIncrementeAutomatiquement(autoIncrementeColonnes.get());
                colonnes.add(colonne);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return colonnes;
    }

    private List<CleEtrangere> getCleEtrangeres(DatabaseMetaData databaseMetaData, String nomTable) throws SQLException {

        ResultSet rsColonnesCleEtrangereMetaData = databaseMetaData.getImportedKeys(null, null, nomTable);
        List<CleEtrangere> cleEtrangeres = new ArrayList<>();

        while(rsColonnesCleEtrangereMetaData.next()){

            ColonneCleEtrangereMetaData colonneCleEtrangereMetaData = new ColonneCleEtrangereMetaData(rsColonnesCleEtrangereMetaData);

            ColonneCleEtrangere colonneCleEtrangere = new ColonneCleEtrangere();
            colonneCleEtrangere.setColonneCleEtrangereMetaData(colonneCleEtrangereMetaData);

            Optional<CleEtrangere> cleEtrangereOptional = cleEtrangeres.stream().filter( element -> element.getNomCleEtrangere().equals(colonneCleEtrangere.getColonneCleEtrangereMetaData().getNomCleEtrangere())).findFirst();

            if(cleEtrangereOptional.isPresent()){

                if(cleEtrangereOptional.get().getColonneCleEtrangereList() != null){
                    cleEtrangereOptional.get().getColonneCleEtrangereList().add(colonneCleEtrangere);
                }else {
                    cleEtrangereOptional.get().setColonneCleEtrangereList(new ArrayList<>());
                    cleEtrangereOptional.get().getColonneCleEtrangereList().add(colonneCleEtrangere);
                }
            }else {
                List<ColonneCleEtrangere> colonneCleEtrangeres = new ArrayList<>();
                colonneCleEtrangeres.add(colonneCleEtrangere);
                cleEtrangeres.add(new CleEtrangere(colonneCleEtrangere.getColonneCleEtrangereMetaData().getNomCleEtrangere(), colonneCleEtrangeres));
            }
        }

        rsColonnesCleEtrangereMetaData.close();

        return cleEtrangeres;
    }

    private List<String> getAutoIncrementedColumns(Connection conn, String schemaName, String tableName) throws SQLException
    {
        LinkedList<String> result = new LinkedList<>();

        Statement stmt = conn.createStatement();

        ResultSet rs = executeSqlSelect(stmt, schemaName, tableName);
        if ( rs != null ) {
            ResultSetMetaData rsmd = rs.getMetaData();
            int n = rsmd.getColumnCount();
            // for each column ( from 1 to N )
            for ( int i = 1 ; i <= n ; i++) {
                if ( rsmd.isAutoIncrement(i) ) { // if auto-incremented column
                    String colName = rsmd.getColumnName(i);
                    result.addLast(colName);
                }
            }

            rs.close();
        }

        stmt.close();

        return result ;
    }

    private ResultSet executeSqlSelect(Statement stmt, String schemaName, String tableName) {
        for ( int step = 1 ; step <=3 ; step++ ) {
            String sqlRequest = buildSqlRequest( schemaName, tableName, step);
            try {
                ResultSet rs = stmt.executeQuery(sqlRequest);
                return rs ;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return null ; // Not supposed to happen
    }

    private String buildSqlRequest(String schemaName, String tableName, int step) {
        String fullTableName = null ;
        if ( step == 1 ) {
            fullTableName = buildFullTableName(schemaName, tableName, null);
        }
        else if ( step == 2 ) {
            fullTableName = buildFullTableName(schemaName, tableName, "\"" );
        }
        else if ( step == 3 ) {
            fullTableName = buildFullTableName(schemaName, tableName, "'" );
        }
        else {
            throw new RuntimeException("buildSqlRequest() : invalid step value");
        }
        String sqlRequest = "SELECT * FROM " + fullTableName + " WHERE  1=0" ;
        //log("SQL request (step="+step+") : " + sqlRequest );
        return sqlRequest;
    }

    private String buildFullTableName(String schemaName, String tableName, String quote) {
        if ( quote != null ) {
            // With quote, e.g. "myschema"."mytable"
            if ( schemaName != null && !schemaName.isEmpty() ) {
                return quote + schemaName.trim() + quote + "." + quote + tableName.trim() + quote;
            }
            else {
                return quote + tableName.trim() + quote ;
            }
        }
        else {
            // Without quote, e.g. myschema.mytable
            if ( schemaName != null ) {
                return schemaName.trim() + "." + tableName.trim() ;
            }
            else {
                return tableName.trim()  ;
            }
        }
    }

}
