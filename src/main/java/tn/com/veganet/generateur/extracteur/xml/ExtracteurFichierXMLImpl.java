package tn.com.veganet.generateur.extracteur.xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tn.com.veganet.generateur.model.application.*;
import tn.com.veganet.generateur.model.type.*;
import tn.com.veganet.generateur.util.GlobaleConstantes;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Extracteur qui permet d'extraire un Objet de type Application à partir d'un fichier XML
 */
@Component
@Primary
public class ExtracteurFichierXMLImpl implements IExtracteurFichier {

    @Autowired
    private ExtracteurUtil extracteurUtil;

    private static ExtracteurFichierXMLImpl extracteurFichierXMLImpl = null;

    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    @Override
    public Application extraireApplicationsFromFile(File file) throws ParserConfigurationException {

        final DocumentBuilder builder = factory.newDocumentBuilder();
        Application application = new Application();


        try {

            final Document document= builder.parse(file);
            document.getDocumentElement().normalize();
            application = extraireApplicationsFromDocument(document);

        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        return application;
    }

    @Override
    public Application extraireApplicationFromFile(File file) {
        return null;
    }

    private Application extraireApplicationsFromDocument(Document document){


        Element element =  (Element) document.getElementsByTagName(GlobaleConstantes.APPLICATION_XML).item(0);

                Application application = extraireApplicationFromElement(element);

                System.out.println(application.getArtifactApplication());

                Base base = extraireBaseDeDonnesFromElement((Element) element.getElementsByTagName(GlobaleConstantes.BASE_XML).item(0));

                application.setBase(base);

                if(element.getElementsByTagName(GlobaleConstantes.ENTITIES_XML).getLength() > 0){

                    Element entityElement = (Element) element.getElementsByTagName(GlobaleConstantes.ENTITIES_XML).item(0);

                    NodeList entitiesNodeList = entityElement.getElementsByTagName(GlobaleConstantes.ENTITY_XML);

                    List<Entity> entities = extraireEntities(entitiesNodeList);

                    for (Entity entity: entities
                         ) {
                        System.out.println(entity.getNomEntity());
                    }

                    if(entities != null){
                        entities = extracteurUtil.affecterEntitiesToRelations(entities);
                        entities = extracteurUtil.affecterExtendedEntities(entities);
                        application.setEntities(entities);
                    }
                }

        return application;
    }


    /**
     * Extraire le modèle de type Application à partir d'un élément XML
     * @param element L'élément XML
     * @return Objet de type Application
     */
    private Application extraireApplicationFromElement(Element element){

        Application application = new Application();
        application.setGroupApplication(element.getAttribute(GlobaleConstantes.GROUP_APPLICATION_XML));
        application.setArtifactApplication(element.getAttribute(GlobaleConstantes.ARTIFACT_APPLICATION_XML));
        application.setVersionApplication(element.getAttribute(GlobaleConstantes.VERSION_APPLICATION_XML));
        application.setDescriptionApplication(element.getAttribute(GlobaleConstantes.DESCRIPTION_APPLICATION_XML));
        application.setPortApplication(element.getAttribute(GlobaleConstantes.PORT_APPLICATION_XML));
        application.setPackagingApplication(Packaging.valueOf(element.getAttribute(GlobaleConstantes.PACKAGING_APPLICATION_XML).toUpperCase()));
        application.setApplicationType(ApplicationType.valueOf(element.getAttribute(GlobaleConstantes.TYPE_APPLICATION_XML).toUpperCase()));
        application.setAuthentificationType(AuthentificationType.valueOf(element.getAttribute(GlobaleConstantes.TYPE_AUTHENTIFICATION_XML).toUpperCase()));
        application.setLombok(Boolean.valueOf(element.getAttribute(GlobaleConstantes.LOMBOK_APPLICATION_XML)));

        return application;
    }

    /**
     * Extraire le modèle de type Base à partir d'un élément XML
     * @param element L'élément XML
     * @return Objet de type Base
     */
    private Base extraireBaseDeDonnesFromElement(Element element){

        Base base = null;

        if(element != null){
            base = new Base(BaseType.valueOf(element.getAttribute(GlobaleConstantes.TYPE_BASE_XML).toUpperCase()),
                    element.getAttribute(GlobaleConstantes.NOM_BASE_XML),
                    element.getAttribute(GlobaleConstantes.IP_BASE_XML),
                    element.getAttribute(GlobaleConstantes.PORT_BASE_XML),
                    element.getAttribute(GlobaleConstantes.USERNAME_BASE_XML),
                    element.getAttribute(GlobaleConstantes.PASSWORD_BASE_XML));
        }

        return base;
    }


    /**
     * Extraire les modèles de type Entity à partir d'un élément XML
     * @param entitiesNodeList Liste des nodes XML
     * @return Liste des d'objets de type Entity
     */
    private List<Entity> extraireEntities(NodeList entitiesNodeList) {

        List<Entity> entities = new ArrayList<>();

        for (int i = 0; i < entitiesNodeList.getLength(); i++) {

            Node node = entitiesNodeList.item(i);

            if(node.getNodeType() == Node.ELEMENT_NODE) {

                Element element = (Element) node;

                Entity entity = new Entity();

                entity.setNomEntity(element.getAttribute(GlobaleConstantes.NOM_ENTITY_XML));
                entity.setNomMere(element.getAttribute(GlobaleConstantes.NOM_MERE_ENTITY_XML));
                entity.setInheritanceType(InheritanceType.valueOf(element.getAttribute(GlobaleConstantes.INHERITANCE_ENTITY_XML).toUpperCase()));
                entity.setDiscriminatorColumn(element.getAttribute(GlobaleConstantes.DISCRIMINATOR_COLUMN_ENTITY_XML));
                entity.setDiscriminatorValue(element.getAttribute(GlobaleConstantes.DISCRIMINATOR_VALUE_ENTITY_XML));
                entity.setPrimaryKeyJoinColumn(element.getAttribute(GlobaleConstantes.PRIMARY_KEY_JOIN_COLUMN_ENTITY_XML));
                entity.setGenerateControleur(Boolean.valueOf(element.getAttribute(GlobaleConstantes.CONTROLEUR_ENTITY_XML)));

                if(element.getElementsByTagName(GlobaleConstantes.PRIMARY_KEY_XML).getLength() > 0)
                {
                    Element primaryKeyElement = (Element) element.getElementsByTagName(GlobaleConstantes.PRIMARY_KEY_XML).item(0);
                    NodeList attributsNodeList = primaryKeyElement.getElementsByTagName(GlobaleConstantes.ATTRIBUT_XML);

                    ClePrimaire clePrimaire = new ClePrimaire(primaryKeyElement.getAttribute("name"), extraireAttributs(attributsNodeList));
                    entity.setClePrimaire(clePrimaire);
                }

                if(element.getElementsByTagName(GlobaleConstantes.ATTRIBUTS_XML).getLength() > 0)
                {
                    Element attributsElement = (Element) element.getElementsByTagName(GlobaleConstantes.ATTRIBUTS_XML).item(0);
                    NodeList attributsNodeList = attributsElement.getElementsByTagName(GlobaleConstantes.ATTRIBUT_XML);

                    entity.setAttributs(extraireAttributs(attributsNodeList));
                }

                if(element.getElementsByTagName(GlobaleConstantes.RELATIONS_XML).getLength() > 0)
                {
                    Element relationElement =  (Element) element.getElementsByTagName(GlobaleConstantes.RELATIONS_XML).item(0);

                    NodeList relationsNodeList = relationElement.getElementsByTagName(GlobaleConstantes.RELATION_XML);

                    List<Relation> relations = extraireRelations(relationsNodeList);

                    entity.setRelations(relations);
                }

                entities.add(entity);
            }
        }

        return entities;
    }


    /**
     * Extraire les modèles de type Attribut à partir d'une NodeList
     * @param attributsNodeList Liste des nodes XML
     * @return Liste d'objets de type Attribut
     */
    private List<Attribut> extraireAttributs(NodeList attributsNodeList) {

        List<Attribut> attributs = new ArrayList<>();


        for (int i = 0; i < attributsNodeList.getLength(); i++) {

            Node node = attributsNodeList.item(i);

            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;


                Attribut attribut = new Attribut();


                attribut.setNomAttribut(element.getAttribute(GlobaleConstantes.NOM_ATTRIBUT_XML));
                attribut.setVisibiliteAttribut(Visibilite.valueOf(element.getAttribute(GlobaleConstantes.VISIBILITE_ATTRIBUT_XML).toUpperCase()));
                attribut.setAttributType(AttributType.valueOf(element.getAttribute(GlobaleConstantes.TYPE_ATTRIBUT_XML).toUpperCase()));
                attribut.setUnique(Boolean.valueOf(element.getAttribute(GlobaleConstantes.UNIQUE_ATTRIBUT_XML)));
                attribut.setNullable(Boolean.valueOf(element.getAttribute(GlobaleConstantes.NULLABLE_ATTRBUT_XML)));
                attribut.setGenerationType(GenerationType.valueOf(element.getAttribute(GlobaleConstantes.GENERATION_TYPE_ATTRIBUT_XML).toUpperCase()));
                attribut.setFindBy(Boolean.valueOf(element.getAttribute(GlobaleConstantes.FIND_BY_ATTRIBUT_XML)));
                attribut.setDtoAttribut(Boolean.valueOf(element.getAttribute(GlobaleConstantes.DTO_ATTRIBUT_XML)));




                attributs.add(attribut);
            }
        }

        return attributs;
    }

    /**
     * Extraire les modèles Relation à partir d'un élément XML
     * @param relationsNodeList Liste des nodes XML
     * @return Liste des d'objets de type Relation
     */
    private List<Relation> extraireRelations(NodeList relationsNodeList) {

        List<Relation> relations = new ArrayList<>();


        for (int i = 0; i < relationsNodeList.getLength(); i++) {

            Node node = relationsNodeList.item(i);

            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                Relation relation = new Relation();

                relation.setNomRelation(element.getAttribute(GlobaleConstantes.NOM_RELATION_XML));
                relation.setBiNomRelation(element.getAttribute(GlobaleConstantes.BI_NOM_RELATION_XML));
                relation.setRelationJPAType(RelationJPAType.valueOf(element.getAttribute(GlobaleConstantes.TYPE_RELATION_XML).toUpperCase()));
                relation.setNomEntity(element.getAttribute(GlobaleConstantes.ENTITY_RELATION_XML));
                relation.setBidirectionnelle(Boolean.valueOf(element.getAttribute(GlobaleConstantes.BI_RELATION_XML)));
                relation.setFetchType(FetchType.valueOf(element.getAttribute(GlobaleConstantes.FETCH_TYPE_XML).toUpperCase()));
                relation.setFindBy(Boolean.valueOf(element.getAttribute(GlobaleConstantes.FIND_BY_RELATION_XML)));
                relation.setJsonIgnore(Boolean.valueOf(element.getAttribute(GlobaleConstantes.JSON_IGONRE_RELATION_XML)));
                relation.setDtoRelation(Boolean.valueOf(element.getAttribute(GlobaleConstantes.DTO_RELATION_XML)));

                if(element.getElementsByTagName(GlobaleConstantes.CASCADE_TYPE_XML).getLength() > 0)
                {
                    Element cascadesElement = (Element) element.getElementsByTagName(GlobaleConstantes.CASCADES_XML).item(0);
                    NodeList cascadesNodeList = cascadesElement.getElementsByTagName(GlobaleConstantes.CASCADE_TYPE_XML);

                    relation.setCascadeTypes(getRelationCascadeTypes(cascadesNodeList));
                }

                relations.add(relation);
            }
        }

        return relations;
    }

    private List<CascadeType> getRelationCascadeTypes(NodeList cascadeNodeList) {

        List<CascadeType> cascadeTypes = new ArrayList<>();

        for (int i = 0; i < cascadeNodeList.getLength(); i++) {

            Node node = cascadeNodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                cascadeTypes.add(CascadeType.valueOf(element.getAttribute(GlobaleConstantes.CASCADE_TYPE_NAME)));

            }
        }

        return cascadeTypes;
    }



}
