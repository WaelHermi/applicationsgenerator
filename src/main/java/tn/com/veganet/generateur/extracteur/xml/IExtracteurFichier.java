package tn.com.veganet.generateur.extracteur.xml;

import tn.com.veganet.generateur.model.application.Application;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.util.List;

/**
 * Extracteur qui permet d'extraire un Objet de type Application à partir d'un fichier
 */
public interface IExtracteurFichier {


    /**
     * Permet d'extraire les informations de plusieurs applications à partir d'un fichier XML ou d'un fichier JSON.
     * @param file Fichier XML ou FICHIER JSON.
     * @return Une liste du modèle Application.
     */
    public abstract Application extraireApplicationsFromFile(File file) throws ParserConfigurationException;

    /**
     * Méthode qui permet d'extraire les informations d'une seule applications à partir d'un fichier XML ou d'un fichier JSON.
     * @param file Fichier XML ou FICHIER JSON.
     * @return Une instance du modèle Application.
     */
    public abstract Application extraireApplicationFromFile(File file);
}
