package tn.com.veganet.generateur.extracteur.xml;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

@Component
@Primary
public class ValidateurFichierXMLImpl implements IValidateurFichierXML{

    
    private static ValidateurFichierXMLImpl validateurFichierXMLImpl;


    public static ValidateurFichierXMLImpl getInstance() {

        if(validateurFichierXMLImpl == null){
            validateurFichierXMLImpl =  new ValidateurFichierXMLImpl();
        }
        return validateurFichierXMLImpl;
    }

    public String validerFichier(File file) {

        try {
            Source xmlFile = new StreamSource(file);
            SchemaFactory schemaFactory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            Schema schema = schemaFactory.newSchema(getClass().getClassLoader().getResource("static/description.xsd"));

            Validator validator = schema.newValidator();
            //validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            //validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            validator.validate(xmlFile);
            System.out.println("Le fichier est valide");
            return "true";
        } catch (SAXException e) {
            System.out.println("Le fichier n'est pas valide :" + e.getMessage());
            return "Le fichier n'est pas valide :" + e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "false";

    }
}
