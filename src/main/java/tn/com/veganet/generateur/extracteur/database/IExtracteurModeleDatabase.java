package tn.com.veganet.generateur.extracteur.database;

import tn.com.veganet.generateur.model.application.Base;
import tn.com.veganet.generateur.model.application.Entity;
import tn.com.veganet.generateur.model.database.Table;

import java.sql.SQLException;
import java.util.List;

public interface IExtracteurModeleDatabase {

    List<Entity> getEntitiesFromDatabaseTables(Base base) throws SQLException;

    Boolean getConnection(Base base);

}
