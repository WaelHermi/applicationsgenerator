package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.com.veganet.generateur.domain.TraceAcces;
import tn.com.veganet.generateur.dto.TraceAccesDto;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.service.ITraceAccesService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class TraceAccesEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private ITraceAccesService traceAccesService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/traceAccess")
    @ApiOperation(
        value = "Get all traceAccess",
        notes = "Returns first N traceAccess specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<TraceAccesDto> traceAccessDto =
                traceAccesService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(traceAccessDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/traceAcces/{id}")
    @ApiOperation(
        value = "Get traceAcces by id",
        notes = "Returns traceAcces for id specified.",
        response = TraceAccesDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "TraceAcces not found")})
    public ResponseEntity get(@ApiParam("TraceAcces id") @PathVariable("id") UUID id) {

        TraceAccesDto traceAccesDto = null;
        try {
            traceAccesDto = convertToDto(traceAccesService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(traceAccesDto);
    }

    @GetMapping(path = "/v1/traceAccesCount")
    @ApiOperation(
            value = "Count the number of TraceAcces",
            notes = "Count the number of TraceAcces. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = traceAccesService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }


    private TraceAccesDto convertToDto(TraceAcces traceAcces) {
        return modelMapper.map(traceAcces, TraceAccesDto.class);
    }

    private TraceAcces convertToEntity(TraceAccesDto traceAccesDto) {
        return modelMapper.map(traceAccesDto, TraceAcces.class);
    }
}
