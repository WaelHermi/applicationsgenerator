package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IDependanceBaseService;
import tn.com.veganet.generateur.domain.DependanceBase;
import tn.com.veganet.generateur.dto.DependanceBaseDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class DependanceBaseEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IDependanceBaseService dependanceBaseService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/dependanceBases")
    @ApiOperation(
        value = "Get all dependanceBases",
        notes = "Returns first N dependanceBases specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<DependanceBaseDto> dependanceBasesDto =
                dependanceBaseService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(dependanceBasesDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/dependanceBase/{id}")
    @ApiOperation(
        value = "Get dependanceBase by id",
        notes = "Returns dependanceBase for id specified.",
        response = DependanceBaseDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "DependanceBase not found")})
    public ResponseEntity get(@ApiParam("DependanceBase id") @PathVariable("id") UUID id) {

        DependanceBaseDto dependanceBaseDto = null;
        try {
            dependanceBaseDto = convertToDto(dependanceBaseService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceBaseDto);
    }

    @PostMapping(path = "/v1/dependanceBase",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new dependanceBase",
        notes = "Creates new dependanceBase. Returns created dependanceBase with id.",
        response = DependanceBaseDto.class)
    public ResponseEntity add(
        @ApiParam("DependanceBase") @Valid @RequestBody DependanceBaseDto dependanceBaseDto){

        try {
            dependanceBaseDto = convertToDto(dependanceBaseService.add(convertToEntity(dependanceBaseDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceBaseDto);
    }


    @PutMapping(path = "/v1/dependanceBase",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing dependanceBase",
        notes = "Updates exisitng dependanceBase. Returns updated dependanceBase.",
        response = DependanceBaseDto.class)
    public ResponseEntity update(
        @ApiParam("DependanceBase") @Valid @RequestBody DependanceBaseDto dependanceBaseDto){

        try {
            dependanceBaseDto = convertToDto(dependanceBaseService.update(convertToEntity(dependanceBaseDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceBaseDto);
    }

    @DeleteMapping(path = "/v1/dependanceBase/{id}")
    @ApiOperation(
        value = "Delete dependanceBase",
        notes = "Delete dependanceBase. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("dependanceBase id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = dependanceBaseService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/dependanceBase/count")
    @ApiOperation(
            value = "Count the number of dependanceBase",
            notes = "Count the number of dependanceBase. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = dependanceBaseService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private DependanceBaseDto convertToDto(DependanceBase dependanceBase) {
        return modelMapper.map(dependanceBase, DependanceBaseDto.class);
    }

    private DependanceBase convertToEntity(DependanceBaseDto dependanceBaseDto) {
        return modelMapper.map(dependanceBaseDto, DependanceBase.class);
    }
}
