package tn.com.veganet.generateur.endpoint;

import org.springframework.security.access.prepost.PreAuthorize;
import tn.com.veganet.generateur.service.IDependanceService;
import tn.com.veganet.generateur.domain.Dependance;
import tn.com.veganet.generateur.dto.DependanceDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class DependanceEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IDependanceService dependanceService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/dependances")
    @ApiOperation(
        value = "Get all dependances",
        notes = "Returns first N dependances specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<DependanceDto> dependancesDto =
                dependanceService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(dependancesDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/dependance/{id}")
    @ApiOperation(
        value = "Get dependance by id",
        notes = "Returns dependance for id specified.",
        response = DependanceDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Dependance not found")})
    public ResponseEntity get(@ApiParam("Dependance id") @PathVariable("id") UUID id) {

        DependanceDto dependanceDto = null;
        try {
            dependanceDto = convertToDto(dependanceService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceDto);
    }

    @PostMapping(path = "/v1/dependance",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new dependance",
        notes = "Creates new dependance. Returns created dependance with id.",
        response = DependanceDto.class)
    public ResponseEntity add(
        @ApiParam("Dependance") @Valid @RequestBody DependanceDto dependanceDto){

        try {
            dependanceDto = convertToDto(dependanceService.add(convertToEntity(dependanceDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceDto);
    }


    @PutMapping(path = "/v1/dependance",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing dependance",
        notes = "Updates exisitng dependance. Returns updated dependance.",
        response = DependanceDto.class)
    public ResponseEntity update(
        @ApiParam("Dependance") @Valid @RequestBody DependanceDto dependanceDto){

        try {
            dependanceDto = convertToDto(dependanceService.update(convertToEntity(dependanceDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceDto);
    }

    @DeleteMapping(path = "/v1/dependance/{id}")
    @ApiOperation(
        value = "Delete dependance",
        notes = "Delete dependance. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("dependance id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = dependanceService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private DependanceDto convertToDto(Dependance dependance) {
        return modelMapper.map(dependance, DependanceDto.class);
    }

    private Dependance convertToEntity(DependanceDto dependanceDto) {
        return modelMapper.map(dependanceDto, Dependance.class);
    }
}
