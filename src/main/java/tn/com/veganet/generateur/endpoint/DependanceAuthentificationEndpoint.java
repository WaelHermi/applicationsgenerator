package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IDependanceAuthentificationService;
import tn.com.veganet.generateur.domain.DependanceAuthentification;
import tn.com.veganet.generateur.dto.DependanceAuthentificationDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class DependanceAuthentificationEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IDependanceAuthentificationService dependanceAuthentificationService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/dependanceAuthentifications")
    @ApiOperation(
        value = "Get all dependanceAuthentifications",
        notes = "Returns first N dependanceAuthentifications specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<DependanceAuthentificationDto> dependanceAuthentificationsDto =
                dependanceAuthentificationService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(dependanceAuthentificationsDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/dependanceAuthentification/{id}")
    @ApiOperation(
        value = "Get dependanceAuthentification by id",
        notes = "Returns dependanceAuthentification for id specified.",
        response = DependanceAuthentificationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "DependanceAuthentification not found")})
    public ResponseEntity get(@ApiParam("DependanceAuthentification id") @PathVariable("id") UUID id) {

        DependanceAuthentificationDto dependanceAuthentificationDto = null;
        try {
            dependanceAuthentificationDto = convertToDto(dependanceAuthentificationService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceAuthentificationDto);
    }

    @PostMapping(path = "/v1/dependanceAuthentification",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new dependanceAuthentification",
        notes = "Creates new dependanceAuthentification. Returns created dependanceAuthentification with id.",
        response = DependanceAuthentificationDto.class)
    public ResponseEntity add(
        @ApiParam("DependanceAuthentification") @Valid @RequestBody DependanceAuthentificationDto dependanceAuthentificationDto){

        try {
            dependanceAuthentificationDto = convertToDto(dependanceAuthentificationService.add(convertToEntity(dependanceAuthentificationDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceAuthentificationDto);
    }


    @PutMapping(path = "/v1/dependanceAuthentification",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing dependanceAuthentification",
        notes = "Updates exisitng dependanceAuthentification. Returns updated dependanceAuthentification.",
        response = DependanceAuthentificationDto.class)
    public ResponseEntity update(
        @ApiParam("DependanceAuthentification") @Valid @RequestBody DependanceAuthentificationDto dependanceAuthentificationDto){

        try {
            dependanceAuthentificationDto = convertToDto(dependanceAuthentificationService.update(convertToEntity(dependanceAuthentificationDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceAuthentificationDto);
    }

    @DeleteMapping(path = "/v1/dependanceAuthentification/{id}")
    @ApiOperation(
        value = "Delete dependanceAuthentification",
        notes = "Delete dependanceAuthentification. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("dependanceAuthentification id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = dependanceAuthentificationService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/dependanceAuthentification/count")
    @ApiOperation(
            value = "Count the number of dependanceAuthentification",
            notes = "Count the number of dependanceAuthentification. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = dependanceAuthentificationService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private DependanceAuthentificationDto convertToDto(DependanceAuthentification dependanceAuthentification) {
        return modelMapper.map(dependanceAuthentification, DependanceAuthentificationDto.class);
    }

    private DependanceAuthentification convertToEntity(DependanceAuthentificationDto dependanceAuthentificationDto) {
        return modelMapper.map(dependanceAuthentificationDto, DependanceAuthentification.class);
    }
}
