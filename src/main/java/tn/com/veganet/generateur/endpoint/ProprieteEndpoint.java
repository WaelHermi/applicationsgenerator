package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IProprieteService;
import tn.com.veganet.generateur.domain.Propriete;
import tn.com.veganet.generateur.dto.ProprieteDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class ProprieteEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IProprieteService proprieteService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/proprietes")
    @ApiOperation(
        value = "Get all proprietes",
        notes = "Returns first N proprietes specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<ProprieteDto> proprietesDto =
                proprieteService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(proprietesDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/propriete/{id}")
    @ApiOperation(
        value = "Get propriete by id",
        notes = "Returns propriete for id specified.",
        response = ProprieteDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Propriete not found")})
    public ResponseEntity get(@ApiParam("Propriete id") @PathVariable("id") UUID id) {

        ProprieteDto proprieteDto = null;
        try {
            proprieteDto = convertToDto(proprieteService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteDto);
    }

    @PostMapping(path = "/v1/propriete",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new propriete",
        notes = "Creates new propriete. Returns created propriete with id.",
        response = ProprieteDto.class)
    public ResponseEntity add(
        @ApiParam("Propriete") @Valid @RequestBody ProprieteDto proprieteDto){

        try {
            proprieteDto = convertToDto(proprieteService.add(convertToEntity(proprieteDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteDto);
    }


    @PutMapping(path = "/v1/propriete",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing propriete",
        notes = "Updates exisitng propriete. Returns updated propriete.",
        response = ProprieteDto.class)
    public ResponseEntity update(
        @ApiParam("Propriete") @Valid @RequestBody ProprieteDto proprieteDto){

        try {
            proprieteDto = convertToDto(proprieteService.update(convertToEntity(proprieteDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteDto);
    }

    @DeleteMapping(path = "/v1/propriete/{id}")
    @ApiOperation(
        value = "Delete propriete",
        notes = "Delete propriete. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("propriete id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = proprieteService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private ProprieteDto convertToDto(Propriete propriete) {
        return modelMapper.map(propriete, ProprieteDto.class);
    }

    private Propriete convertToEntity(ProprieteDto proprieteDto) {
        return modelMapper.map(proprieteDto, Propriete.class);
    }
}
