package tn.com.veganet.generateur.endpoint;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.dto.UtilisateurDto;
import tn.com.veganet.generateur.model.AuthenticationRequest;
import tn.com.veganet.generateur.model.AuthenticationResponse;
import tn.com.veganet.generateur.service.ITraceAccesService;
import tn.com.veganet.generateur.service.IUtilisateurService;
import tn.com.veganet.generateur.service.JwtUtil;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthenticationEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ITraceAccesService traceAccesService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private IUtilisateurService utilisateurService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest, HttpServletRequest httpServletRequest) throws Exception{

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsernameUtilisateur(), authenticationRequest.getPasswordUtilisateur()));
        } catch (BadCredentialsException e){
            traceAccesService.add(authenticationRequest, httpServletRequest);
            throw new Exception("Nom d'utilisateur ou mot de passe incorrecte", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsernameUtilisateur());
        final String jwt = jwtUtil.generateToken(userDetails);
        final Utilisateur utilisateur = utilisateurService.findByUsernameUtilisateur(authenticationRequest.getUsernameUtilisateur());

        authenticationRequest.setStatus(true);
        traceAccesService.add(authenticationRequest, httpServletRequest);

        return ResponseEntity.ok(new AuthenticationResponse(convertToDto(utilisateur), jwt));
    }

    private UtilisateurDto convertToDto(Utilisateur utilisateur) {
        return modelMapper.map(utilisateur, UtilisateurDto.class);
    }

}
