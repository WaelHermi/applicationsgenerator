package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IProprieteApplicationService;
import tn.com.veganet.generateur.domain.ProprieteApplication;
import tn.com.veganet.generateur.dto.ProprieteApplicationDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class ProprieteApplicationEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IProprieteApplicationService proprieteApplicationService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/proprieteApplications")
    @ApiOperation(
        value = "Get all proprieteApplications",
        notes = "Returns first N proprieteApplications specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<ProprieteApplicationDto> proprieteApplicationsDto =
                proprieteApplicationService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(proprieteApplicationsDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/proprieteApplication/{id}")
    @ApiOperation(
        value = "Get proprieteApplication by id",
        notes = "Returns proprieteApplication for id specified.",
        response = ProprieteApplicationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "ProprieteApplication not found")})
    public ResponseEntity get(@ApiParam("ProprieteApplication id") @PathVariable("id") UUID id) {

        ProprieteApplicationDto proprieteApplicationDto = null;
        try {
            proprieteApplicationDto = convertToDto(proprieteApplicationService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteApplicationDto);
    }

    @PostMapping(path = "/v1/proprieteApplication",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new proprieteApplication",
        notes = "Creates new proprieteApplication. Returns created proprieteApplication with id.",
        response = ProprieteApplicationDto.class)
    public ResponseEntity add(
        @ApiParam("ProprieteApplication") @Valid @RequestBody ProprieteApplicationDto proprieteApplicationDto){

        try {
            proprieteApplicationDto = convertToDto(proprieteApplicationService.add(convertToEntity(proprieteApplicationDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteApplicationDto);
    }


    @PutMapping(path = "/v1/proprieteApplication",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing proprieteApplication",
        notes = "Updates exisitng proprieteApplication. Returns updated proprieteApplication.",
        response = ProprieteApplicationDto.class)
    public ResponseEntity update(
        @ApiParam("ProprieteApplication") @Valid @RequestBody ProprieteApplicationDto proprieteApplicationDto){

        try {
            proprieteApplicationDto = convertToDto(proprieteApplicationService.update(convertToEntity(proprieteApplicationDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteApplicationDto);
    }

    @DeleteMapping(path = "/v1/proprieteApplication/{id}")
    @ApiOperation(
        value = "Delete proprieteApplication",
        notes = "Delete proprieteApplication. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("proprieteApplication id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = proprieteApplicationService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/proprieteApplication/count")
    @ApiOperation(
            value = "Count the number of proprieteApplication",
            notes = "Count the number of proprieteApplication. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = proprieteApplicationService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private ProprieteApplicationDto convertToDto(ProprieteApplication proprieteApplication) {
        return modelMapper.map(proprieteApplication, ProprieteApplicationDto.class);
    }

    private ProprieteApplication convertToEntity(ProprieteApplicationDto proprieteApplicationDto) {
        return modelMapper.map(proprieteApplicationDto, ProprieteApplication.class);
    }
}
