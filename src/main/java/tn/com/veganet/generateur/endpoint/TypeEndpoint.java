package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.com.veganet.generateur.model.EnumValeursWrapper;
import tn.com.veganet.generateur.service.ITypeService;

import java.util.List;

@RestController
@RequestMapping(path = "v1/type")
@CrossOrigin("*")
public class TypeEndpoint {

    @Autowired
    private ITypeService typeService;

    @GetMapping(path = "application")
    @ApiOperation(
            value = "Get all Application types.",
            notes = "Returns all Application types.",
            response = List.class)
    public List<EnumValeursWrapper> getApplicationEnumValues(){ return typeService.getApplicationTypeEnumValues(); }

    @GetMapping(path = "packaging")
    @ApiOperation(
            value = "Get all Packaging types.",
            notes = "Returns all Packaging types.",
            response = List.class)
    public List<EnumValeursWrapper> getPackagingEnumValues(){
        return typeService.getPackagingEnumValues();
    }

    @GetMapping(path = "base")
    @ApiOperation(
            value = "Get all data base types.",
            notes = "Returns all data base types.",
            response = List.class)
    public List<EnumValeursWrapper> getBaseTypesEnumValues(){
        return typeService.getBaseTypeEnumValues();
    }

    @GetMapping(path = "attribut")
    @ApiOperation(
            value = "Get all attribute types.",
            notes = "Returns all attribute types.",
            response = List.class)
    public List<EnumValeursWrapper> getAttributTypesEnumValues(){
        return typeService.getAttributTypeEnumValues();
    }

    @GetMapping(path = "visibilite")
    @ApiOperation(
            value = "Get all visibility types.",
            notes = "Returns all visibility types.",
            response = List.class)
    public List<EnumValeursWrapper> getVisibiliteEnumValues(){
        return typeService.getVisibiliteEnumValues();
    }

    @GetMapping(path = "generation")
    @ApiOperation(
            value = "Get all generation types.",
            notes = "Returns all generation types.",
            response = List.class)
    public List<EnumValeursWrapper> getGenerationTypeEnumValues(){
        return typeService.getGenerationTypeEnumValues();
    }

    @GetMapping(path = "relation")
    @ApiOperation(
            value = "Get all relation types.",
            notes = "Returns relation types.",
            response = List.class)
    public List<EnumValeursWrapper> getRelationJPATypeEnumValues(){
        return typeService.getRelationJPATypeEnumValues();
    }

    @GetMapping(path = "fetch")
    @ApiOperation(
            value = "Get all fetch types.",
            notes = "Returns all fetch types.",
            response = List.class)
    public List<EnumValeursWrapper> getFetchTypeEnumValues(){
        return typeService.getFetchTypeEnumValues();
    }

    @GetMapping(path = "cascade")
    @ApiOperation(
            value = "Get all cascade types.",
            notes = "Returns all cascade types.",
            response = List.class)
    public List<EnumValeursWrapper> getCascadeTypeEnumValues(){
        return typeService.getCascadeTypeEnumValues();
    }

    @GetMapping(path = "authentification")
    @ApiOperation(
            value = "Get all authentification types.",
            notes = "Returns all authentification types.",
            response = List.class)
    public List<EnumValeursWrapper> getAuthentificationTypeEnumValues(){
        return typeService.getAuthentificationTypeEnumValues();
    }

    @GetMapping(path = "inheritance")
    @ApiOperation(
            value = "Get all Inheritance types.",
            notes = "Returns all Inheritance types.",
            response = List.class)
    public List<EnumValeursWrapper> getInheritanceTypeEnumValues(){
        return typeService.getInheritanceTypeEnumValues();
    }

    @GetMapping(path = "scope")
    @ApiOperation(
            value = "Get all Dependance Scope types.",
            notes = "Returns all Dependance Scope types.",
            response = List.class)
    public List<EnumValeursWrapper> getDependanceScopeEnumValues(){
        return typeService.getDependanceScopeEnumValues();
    }
}
