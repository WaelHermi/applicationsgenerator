package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.com.veganet.generateur.domain.Role;
import tn.com.veganet.generateur.dto.RoleDto;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.service.IRoleService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class RoleEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/roles")
    @ApiOperation(
        value = "Get all roles",
        notes = "Returns first N roles specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<RoleDto> rolesDto =
                roleService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(rolesDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/role/{id}")
    @ApiOperation(
        value = "Get role by id",
        notes = "Returns role for id specified.",
        response = RoleDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Role not found")})
    public ResponseEntity get(@ApiParam("Role id") @PathVariable("id") UUID id) {

        RoleDto roleDto = null;
        try {
            roleDto = convertToDto(roleService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(roleDto);
    }

    @PostMapping(path = "/v1/role",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new role",
        notes = "Creates new role. Returns created role with id.",
        response = RoleDto.class)
    public ResponseEntity add(
        @ApiParam("Role") @Valid @RequestBody RoleDto roleDto){

        try {
            roleDto = convertToDto(roleService.add(convertToEntity(roleDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(roleDto);
    }


    @PutMapping(path = "/v1/role",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing role",
        notes = "Updates exisitng role. Returns updated role.",
        response = RoleDto.class)
    public ResponseEntity update(
        @ApiParam("Role") @Valid @RequestBody RoleDto roleDto){

        try {
            roleDto = convertToDto(roleService.update(convertToEntity(roleDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(roleDto);
    }

    @DeleteMapping(path = "/v1/role/{id}")
    @ApiOperation(
        value = "Delete role",
        notes = "Delete role. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("role id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = roleService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private RoleDto convertToDto(Role role) {
        return modelMapper.map(role, RoleDto.class);
    }

    private Role convertToEntity(RoleDto roleDto) {
        return modelMapper.map(roleDto, Role.class);
    }
}
