package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IDependanceApplicationService;
import tn.com.veganet.generateur.domain.DependanceApplication;
import tn.com.veganet.generateur.dto.DependanceApplicationDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class DependanceApplicationEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IDependanceApplicationService dependanceApplicationService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/dependanceApplications")
    @ApiOperation(
        value = "Get all dependanceApplications",
        notes = "Returns first N dependanceApplications specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<DependanceApplicationDto> dependanceApplicationsDto =
                dependanceApplicationService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(dependanceApplicationsDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/dependanceApplication/{id}")
    @ApiOperation(
        value = "Get dependanceApplication by id",
        notes = "Returns dependanceApplication for id specified.",
        response = DependanceApplicationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "DependanceApplication not found")})
    public ResponseEntity get(@ApiParam("DependanceApplication id") @PathVariable("id") UUID id) {

        DependanceApplicationDto dependanceApplicationDto = null;
        try {
            dependanceApplicationDto = convertToDto(dependanceApplicationService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceApplicationDto);
    }

    @PostMapping(path = "/v1/dependanceApplication",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new dependanceApplication",
        notes = "Creates new dependanceApplication. Returns created dependanceApplication with id.",
        response = DependanceApplicationDto.class)
    public ResponseEntity add(
        @ApiParam("DependanceApplication") @Valid @RequestBody DependanceApplicationDto dependanceApplicationDto){

        try {
            dependanceApplicationDto = convertToDto(dependanceApplicationService.add(convertToEntity(dependanceApplicationDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceApplicationDto);
    }


    @PutMapping(path = "/v1/dependanceApplication",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing dependanceApplication",
        notes = "Updates exisitng dependanceApplication. Returns updated dependanceApplication.",
        response = DependanceApplicationDto.class)
    public ResponseEntity update(
        @ApiParam("DependanceApplication") @Valid @RequestBody DependanceApplicationDto dependanceApplicationDto){

        try {
            dependanceApplicationDto = convertToDto(dependanceApplicationService.update(convertToEntity(dependanceApplicationDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(dependanceApplicationDto);
    }

    @DeleteMapping(path = "/v1/dependanceApplication/{id}")
    @ApiOperation(
        value = "Delete dependanceApplication",
        notes = "Delete dependanceApplication. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("dependanceApplication id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = dependanceApplicationService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/dependanceApplication/count")
    @ApiOperation(
            value = "Count the number of dependanceApplication",
            notes = "Count the number of dependanceApplication. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = dependanceApplicationService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private DependanceApplicationDto convertToDto(DependanceApplication dependanceApplication) {
        return modelMapper.map(dependanceApplication, DependanceApplicationDto.class);
    }

    private DependanceApplication convertToEntity(DependanceApplicationDto dependanceApplicationDto) {
        return modelMapper.map(dependanceApplicationDto, DependanceApplication.class);
    }
}
