package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IProprieteAuthentificationService;
import tn.com.veganet.generateur.domain.ProprieteAuthentification;
import tn.com.veganet.generateur.dto.ProprieteAuthentificationDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class ProprieteAuthentificationEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IProprieteAuthentificationService proprieteAuthentificationService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/proprieteAuthentifications")
    @ApiOperation(
        value = "Get all proprieteAuthentifications",
        notes = "Returns first N proprieteAuthentifications specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<ProprieteAuthentificationDto> proprieteAuthentificationsDto =
                proprieteAuthentificationService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(proprieteAuthentificationsDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/proprieteAuthentification/{id}")
    @ApiOperation(
        value = "Get proprieteAuthentification by id",
        notes = "Returns proprieteAuthentification for id specified.",
        response = ProprieteAuthentificationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "ProprieteAuthentification not found")})
    public ResponseEntity get(@ApiParam("ProprieteAuthentification id") @PathVariable("id") UUID id) {

        ProprieteAuthentificationDto proprieteAuthentificationDto = null;
        try {
            proprieteAuthentificationDto = convertToDto(proprieteAuthentificationService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteAuthentificationDto);
    }

    @PostMapping(path = "/v1/proprieteAuthentification",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new proprieteAuthentification",
        notes = "Creates new proprieteAuthentification. Returns created proprieteAuthentification with id.",
        response = ProprieteAuthentificationDto.class)
    public ResponseEntity add(
        @ApiParam("ProprieteAuthentification") @Valid @RequestBody ProprieteAuthentificationDto proprieteAuthentificationDto){

        try {
            proprieteAuthentificationDto = convertToDto(proprieteAuthentificationService.add(convertToEntity(proprieteAuthentificationDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteAuthentificationDto);
    }


    @PutMapping(path = "/v1/proprieteAuthentification",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing proprieteAuthentification",
        notes = "Updates exisitng proprieteAuthentification. Returns updated proprieteAuthentification.",
        response = ProprieteAuthentificationDto.class)
    public ResponseEntity update(
        @ApiParam("ProprieteAuthentification") @Valid @RequestBody ProprieteAuthentificationDto proprieteAuthentificationDto){

        try {
            proprieteAuthentificationDto = convertToDto(proprieteAuthentificationService.update(convertToEntity(proprieteAuthentificationDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteAuthentificationDto);
    }

    @DeleteMapping(path = "/v1/proprieteAuthentification/{id}")
    @ApiOperation(
        value = "Delete proprieteAuthentification",
        notes = "Delete proprieteAuthentification. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("proprieteAuthentification id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = proprieteAuthentificationService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/proprieteAuthentification/count")
    @ApiOperation(
            value = "Count the number of proprieteAuthentification",
            notes = "Count the number of proprieteAuthentification. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = proprieteAuthentificationService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private ProprieteAuthentificationDto convertToDto(ProprieteAuthentification proprieteAuthentification) {
        return modelMapper.map(proprieteAuthentification, ProprieteAuthentificationDto.class);
    }

    private ProprieteAuthentification convertToEntity(ProprieteAuthentificationDto proprieteAuthentificationDto) {
        return modelMapper.map(proprieteAuthentificationDto, ProprieteAuthentification.class);
    }
}
