package tn.com.veganet.generateur.endpoint;

import freemarker.template.TemplateException;
import io.swagger.annotations.*;
import jdk.nashorn.internal.ir.EmptyNode;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.model.UpdateProjectContext;
import tn.com.veganet.generateur.model.application.Application;
import tn.com.veganet.generateur.model.application.Base;
import tn.com.veganet.generateur.model.application.Entity;
import tn.com.veganet.generateur.model.database.Table;
import tn.com.veganet.generateur.model.type.BaseType;
import tn.com.veganet.generateur.model.type.GenerationType;
import tn.com.veganet.generateur.service.IGenerateurService;
import tn.com.veganet.generateur.service.ITraceDonneesService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Api(tags = {"Endpoints des opérations de génération des API"})
@RestController
@RequestMapping("v1/generateur")
@CrossOrigin("*")
public class GenerateurEndpoint {

    @Autowired
    IGenerateurService generateurService;

    @Autowired
    ITraceDonneesService traceDonneesService;

    @PostMapping(value = "application_from_xml")
    @ApiOperation(value = "Permet de générer une API REST Spring Boot à partir d'un fichier XML",
        notes = "Retourne un fichier ZIP de l'API générée"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class)})
    public ResponseEntity generateApplicationFromXml(@ApiParam(name = "fichier", value = "Un fichier XML qui doit contenir la configuration de l'API", required = true, type = "MultipartFile")  @RequestParam MultipartFile fichier, HttpServletRequest httpServletRequest) throws IOException, ParserConfigurationException{

        File tempFile = File.createTempFile("file", "." + FilenameUtils.getExtension(fichier.getOriginalFilename()));
        // ask JVM to delete it upon JVM exit if you forgot / can't delete due exception
        tempFile.deleteOnExit();
        // transfer MultipartFile to File
        fichier.transferTo(tempFile);
        // do business logic here
        String resultatValidation = generateurService.validerFichierXML(tempFile);

        if (resultatValidation.equals("true")) {
            String url = generateurService.genererApplicationFromXml(tempFile);

            if (url != null) {

                Path path = Paths.get(url);
                Resource resource = new UrlResource(path.toUri());

                return ResponseEntity.ok()
                            .contentType(MediaType.parseMediaType("application/zip"))
                            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                            .body(resource);
            }
        }

        return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(resultatValidation);
    }

    @PostMapping(value = "application")
    @ApiOperation(value = "Permet de générer une API REST Spring Boot à partir d'un objet application",
            notes = "Retourne un fichier ZIP de l'application générée"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class)})
    public ResponseEntity generateApplication(@RequestBody @Valid Application application, HttpServletRequest httpServletRequest) throws IOException, ParserConfigurationException, TemplateException {

        System.out.println(application.getArtifactApplication());

        String url = generateurService.genererApplication(application);
        if (url != null) {
            Path path = Paths.get(url);
            Resource resource = new UrlResource(path.toUri());

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/zip"))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        }
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(false);
    }

    @PostMapping(value = "enregistrer")
    @ApiOperation(value = "Permet d'enregistrer la configuration d'une API à partir d'un objet application'",
            notes = "Retourne un fichier XML"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class)})
    public ResponseEntity enregister(@RequestBody @Valid Application application, HttpServletRequest httpServletRequest) {

        GeneratedProject generatedProject = null;

        try {
            generatedProject = generateurService.enregistrerFichierXML(application, httpServletRequest);
        } catch (GetException | CreateException  e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(false);
        }

        if (generatedProject != null) {
            return ResponseEntity.ok().body(generatedProject);
        }
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(false);
    }

    @PostMapping(value = "modifier_enregistrement")
    @ApiOperation(value = "Permet de modifier l'enregistrement de la configuration d'une API à partir d'un objet application'",
            notes = "Retourne un fichier XML"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class)})
    public ResponseEntity updateEnregisterement(@RequestBody @Valid UpdateProjectContext updateProjectContext, HttpServletRequest httpServletRequest) {

        GeneratedProject updateGeneratedProject = null;

        try {
            updateGeneratedProject = generateurService.modifierEnregistrementFichierXML(updateProjectContext, httpServletRequest);
        } catch (GetException | UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(false);
        }

        if (updateGeneratedProject != null) {
            return ResponseEntity.ok().body(updateGeneratedProject);
        }
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(false);
    }

    @PostMapping("/database_tables")
    @ApiOperation(value = "Permet d'extraire les métadonnées des tables dans une base de données SQL",
            notes = "Retourne la liste des métadonnées des entités construites à partir des métadonnées de votre base de données"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Table.class)})
    public ResponseEntity getDatabaseEntities(@RequestBody Base base, HttpServletRequest httpServletRequest) {

        //Base base1 = new Base(BaseType.valueOf("POSTGRESQL"), "generateur", "localhost", "5432", "postgres", "root");

        try {
            List<Entity> entities = generateurService.extraireDatabaseEntities(base);
            traceDonneesService.add("Extraction des entités à partir d'une base de données", httpServletRequest);
            return  ResponseEntity.ok().body(entities);
        } catch (SQLException | CreateException | GetException e) {
            return ResponseEntity.ok().body(e.getMessage());
        }
    }
    @PostMapping("/database_connection")
    @ApiOperation(value = "Permet de vérifier la connectivité d'une base de données",
            notes = "Retourne l'état de la connection à la base de données"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Collections.class)})
    public ResponseEntity getDatabaseconnectivity(@RequestBody Base base) {
            Boolean result = generateurService.getConnection(base);
            return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @PostMapping(value = "saved_application")
    @ApiOperation(value = "Permet de récupérer la configuration une API REST Spring Boot du serveur",
            notes = "Retourne un objet de type application"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class)})
    public ResponseEntity getSavedApplication(@RequestBody GeneratedProject generatedProject) {

        try {
            return ResponseEntity.ok().body(generateurService.getSavedApplication(generatedProject));
        } catch (ParserConfigurationException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(false);
        }
    }

}
