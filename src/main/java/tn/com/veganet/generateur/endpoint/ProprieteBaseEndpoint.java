package tn.com.veganet.generateur.endpoint;

import tn.com.veganet.generateur.service.IProprieteBaseService;
import tn.com.veganet.generateur.domain.ProprieteBase;
import tn.com.veganet.generateur.dto.ProprieteBaseDto;
import tn.com.veganet.generateur.exception.*;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class ProprieteBaseEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IProprieteBaseService proprieteBaseService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/proprieteBases")
    @ApiOperation(
        value = "Get all proprieteBases",
        notes = "Returns first N proprieteBases specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<ProprieteBaseDto> proprieteBasesDto =
                proprieteBaseService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(proprieteBasesDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/proprieteBase/{id}")
    @ApiOperation(
        value = "Get proprieteBase by id",
        notes = "Returns proprieteBase for id specified.",
        response = ProprieteBaseDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "ProprieteBase not found")})
    public ResponseEntity get(@ApiParam("ProprieteBase id") @PathVariable("id") UUID id) {

        ProprieteBaseDto proprieteBaseDto = null;
        try {
            proprieteBaseDto = convertToDto(proprieteBaseService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteBaseDto);
    }

    @PostMapping(path = "/v1/proprieteBase",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new proprieteBase",
        notes = "Creates new proprieteBase. Returns created proprieteBase with id.",
        response = ProprieteBaseDto.class)
    public ResponseEntity add(
        @ApiParam("ProprieteBase") @Valid @RequestBody ProprieteBaseDto proprieteBaseDto){

        try {
            proprieteBaseDto = convertToDto(proprieteBaseService.add(convertToEntity(proprieteBaseDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteBaseDto);
    }


    @PutMapping(path = "/v1/proprieteBase",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing proprieteBase",
        notes = "Updates exisitng proprieteBase. Returns updated proprieteBase.",
        response = ProprieteBaseDto.class)
    public ResponseEntity update(
        @ApiParam("ProprieteBase") @Valid @RequestBody ProprieteBaseDto proprieteBaseDto){

        try {
            proprieteBaseDto = convertToDto(proprieteBaseService.update(convertToEntity(proprieteBaseDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(proprieteBaseDto);
    }

    @DeleteMapping(path = "/v1/proprieteBase/{id}")
    @ApiOperation(
        value = "Delete proprieteBase",
        notes = "Delete proprieteBase. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("proprieteBase id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = proprieteBaseService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/proprieteBase/count")
    @ApiOperation(
            value = "Count the number of proprieteBase",
            notes = "Count the number of proprieteBase. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = proprieteBaseService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private ProprieteBaseDto convertToDto(ProprieteBase proprieteBase) {
        return modelMapper.map(proprieteBase, ProprieteBaseDto.class);
    }

    private ProprieteBase convertToEntity(ProprieteBaseDto proprieteBaseDto) {
        return modelMapper.map(proprieteBaseDto, ProprieteBase.class);
    }
}
