package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.dto.UtilisateurDto;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.model.MailModel;
import tn.com.veganet.generateur.model.PasswordModel;
import tn.com.veganet.generateur.model.ResetPasswordRequest;
import tn.com.veganet.generateur.service.IMailService;
import tn.com.veganet.generateur.service.IUtilisateurService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class UtilisateurEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IUtilisateurService utilisateurService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IMailService mailService;

    @GetMapping(path = "/v1/utilisateurs")
    @ApiOperation(
        value = "Get all utilisateurs",
        notes = "Returns first N utilisateurs specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<UtilisateurDto> utilisateursDto =
                utilisateurService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(utilisateursDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/utilisateur/{id}")
    @ApiOperation(
        value = "Get utilisateur by id",
        notes = "Returns utilisateur for id specified.",
        response = UtilisateurDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Utilisateur not found")})
    public ResponseEntity get(@ApiParam("Utilisateur id") @PathVariable("id") UUID id) {

        UtilisateurDto utilisateurDto = null;
        try {
            utilisateurDto = convertToDto(utilisateurService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(utilisateurDto);
    }

    @PostMapping(path = "/v1/utilisateur",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new gestionnaire",
        notes = "Creates new gestionnaire. Returns created gestionnaire with id.",
        response = UtilisateurDto.class)
    public ResponseEntity addGestionnaire(
        @ApiParam("Utilisateur") @Valid @RequestBody UtilisateurDto utilisateurDto){

        try {
            utilisateurDto = convertToDto(utilisateurService.addGestionnaire(convertToEntity(utilisateurDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(utilisateurDto);
    }

    @PostMapping(path = "/v1/client",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
            value = "Create new client",
            notes = "Creates new client. Returns created client with id.",
            response = UtilisateurDto.class)
    public ResponseEntity inscriptionClient(
            @ApiParam("Utilisateur") @Valid @RequestBody UtilisateurDto utilisateurDto){

        try {
            utilisateurDto = convertToDto(utilisateurService.inscriptionClient(convertToEntity(utilisateurDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(utilisateurDto);
    }

    @PutMapping(path = "/v1/utilisateur",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing utilisateur",
        notes = "Updates exisitng utilisateur. Returns updated utilisateur.",
        response = UtilisateurDto.class)
    public ResponseEntity update(
        @ApiParam("Utilisateur") @Valid @RequestBody UtilisateurDto utilisateurDto){

        try {
            utilisateurDto = convertToDto(utilisateurService.update(convertToEntity(utilisateurDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(utilisateurDto);
    }

    @DeleteMapping(path = "/v1/utilisateur/{id}")
    @ApiOperation(
        value = "Delete utilisateur",
        notes = "Delete utilisateur. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("utilisateur id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = utilisateurService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @GetMapping(path = "/v1/utilisateurCount")
    @ApiOperation(
            value = "Count the number of utilisateurs",
            notes = "Count the number of utilisateurs. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = utilisateurService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    /*

    @GetMapping(path = "/v1/utilisateur/{id}")
    @ApiOperation(
            value = "Get utilisateur by id",
            notes = "Returns utilisateur for id specified.",
            response = UtilisateurDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Utilisateur not found")})
    public ResponseEntity getBasic(@ApiParam("Utilisateur id") @PathVariable("id") UUID id) {

        Utilisateur saveUtilisateur = new Utilisateur();
        saveUtilisateur.setIdUtilisateur(UUID.fromString("ccc70f38-1eb8-4a59-a7f3-ac94c4d6df7c"));
        saveUtilisateur.setIdentifiantUtilisateur("WaelHermi");
        saveUtilisateur.setPasswordUtilisateur("12345");
        saveUtilisateur.setNomUtilisateur("HERMI");
        saveUtilisateur.setPrenomUtilisateur("Wael");
        saveUtilisateur.setAdresseMailUtilisateur("HermiWael25@gmail.com");
        saveUtilisateur.setTelephoneUtilisateur("93558585");

        try {
            utilisateurService.update(saveUtilisateur);
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }
    }

    */

    @PostMapping("/v1/SendMail")
    @ApiOperation(value = "Send e-mail to user", notes = "Send e-mail to user", response = Object.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Object.class),
            @ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "not found") })
    public ResponseEntity SendMail(HttpServletRequest request, @RequestBody @Valid MailModel mailModel) {
        Boolean result = false;
        try {
            result = mailService.sendSimpleMessage(mailModel);
        } catch (Exception e) {
            ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @PostMapping("/v1/SendEmailToResetPassword/{email}")
    @ApiOperation(value = "Send e-mail to reset password", notes = "Send e-mail to reset password", response = Object.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Object.class),
            @ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "not found") })
    public Object SendEmailToResetPassword(HttpServletRequest request, @PathVariable("email") @Valid String email ) {
        Boolean result = false;
        try {
            result = utilisateurService.SendEmailToResetPassword(email);
        } catch (Exception e) {
            ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private UtilisateurDto convertToDto(Utilisateur utilisateur) {
        return modelMapper.map(utilisateur, UtilisateurDto.class);
    }

    private Utilisateur convertToEntity(UtilisateurDto utilisateurDto) {
        return modelMapper.map(utilisateurDto, Utilisateur.class);
    }

    @PutMapping(path = "/v1/utilisateur/updatePassword",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
            value = "Update password utilisateur",
            notes = "Update password utilisateur. Returns reset result.",
            response = UtilisateurDto.class)
    public ResponseEntity updatePassword(
            HttpServletRequest httpServletRequest, @ApiParam("PasswordModel") @Valid @RequestBody PasswordModel passwordModel) throws Exception {

        Boolean result= false;
        try {
            result = utilisateurService.updatePassword(httpServletRequest, passwordModel);
        } catch (GetException | UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(Collections.singletonMap("result", result));
        } catch (BadCredentialsException e){
            throw new Exception("Nom d'utilisateur ou mot de passe incorrecte", e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    @PutMapping(path = "/v1/utilisateur/resetPassword",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
            value = "Reset password utilisateur",
            notes = "Reset password utilisateur. Returns reset result.",
            response = UtilisateurDto.class)
    public ResponseEntity resetPassword(
            @ApiParam("ResetPasswordRequest") @Valid @RequestBody ResetPasswordRequest resetPasswordRequest){

        Boolean result= false;
        try {
            utilisateurService.resetPassword(resetPasswordRequest);
            result = true;
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(Collections.singletonMap("result", result));
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }
}
