package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.com.veganet.generateur.domain.TraceDonnees;
import tn.com.veganet.generateur.dto.TraceDonneesDto;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.service.ITraceDonneesService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class TraceDonneesEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private ITraceDonneesService traceDonneesService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/traceDonneess")
    @ApiOperation(
        value = "Get all traceDonneess",
        notes = "Returns first N traceDonneess specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<TraceDonneesDto> traceDonneessDto =
                traceDonneesService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(traceDonneessDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/traceDonnees/{id}")
    @ApiOperation(
        value = "Get traceDonnees by id",
        notes = "Returns traceDonnees for id specified.",
        response = TraceDonneesDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "TraceDonnees not found")})
    public ResponseEntity get(@ApiParam("TraceDonnees id") @PathVariable("id") UUID id) {

        TraceDonneesDto traceDonneesDto = null;
        try {
            traceDonneesDto = convertToDto(traceDonneesService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(traceDonneesDto);
    }

    @GetMapping(path = "/v1/traceDonneesCount")
    @ApiOperation(
            value = "Count the number of TraceDonnees",
            notes = "Count the number of TraceDonnees. Returns Long.",
            response = Long.class)
    public ResponseEntity count(){
        Long result = traceDonneesService.count();;
        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private TraceDonneesDto convertToDto(TraceDonnees traceDonnees) {
        return modelMapper.map(traceDonnees, TraceDonneesDto.class);
    }

}
