package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.dto.GeneratedProjectDto;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.service.IGeneratedProjectService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class GeneratedProjectEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IGeneratedProjectService generatedProjectService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/generatedProjects")
    @ApiOperation(
        value = "Get all generatedProjects",
        notes = "Returns first N generatedProjects specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<GeneratedProjectDto> generatedProjectsDto =
                generatedProjectService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(generatedProjectsDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/generatedProject/{id}")
    @ApiOperation(
        value = "Get generatedProject by id",
        notes = "Returns generatedProject for id specified.",
        response = GeneratedProjectDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "GeneratedProject not found")})
    public ResponseEntity get(@ApiParam("GeneratedProject id") @PathVariable("id") UUID id) {

        GeneratedProjectDto generatedProjectDto = null;
        try {
            generatedProjectDto = convertToDto(generatedProjectService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(generatedProjectDto);
    }

    @GetMapping(path = "/v1/generatedProjects/getByUtilisateur")
    @ApiOperation(
            value = "Get all generatedProjects by Utilisateur",
            notes = "Returns generatedProjects by Utilisateur.",
            response = Collection.class)
    public ResponseEntity getByUtilisateur(HttpServletRequest httpServletRequest) {
        try {
            List<GeneratedProjectDto> generatedProjectsDto =
                    generatedProjectService.findByUtilisateur(httpServletRequest).stream().map(this::convertToDto).collect(Collectors.toList());
            return ResponseEntity.ok().body(generatedProjectsDto);
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }
    }

    @PostMapping(path = "/v1/generatedProject",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new generatedProject",
        notes = "Creates new generatedProject. Returns created generatedProject with id.",
        response = GeneratedProjectDto.class)
    public ResponseEntity add(
        @ApiParam("GeneratedProject") @Valid @RequestBody GeneratedProjectDto generatedProjectDto){

        try {
            generatedProjectDto = convertToDto(generatedProjectService.add(convertToEntity(generatedProjectDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(generatedProjectDto);
    }


    @PutMapping(path = "/v1/generatedProject",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing generatedProject",
        notes = "Updates exisitng generatedProject. Returns updated generatedProject.",
        response = GeneratedProjectDto.class)
    public ResponseEntity update(
        @ApiParam("GeneratedProject") @Valid @RequestBody GeneratedProjectDto generatedProjectDto){

        try {
            generatedProjectDto = convertToDto(generatedProjectService.update(convertToEntity(generatedProjectDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(generatedProjectDto);
    }

    @DeleteMapping(path = "/v1/generatedProject/{id}")
    @ApiOperation(
        value = "Delete generatedProject",
        notes = "Delete generatedProject. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("generatedProject id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = generatedProjectService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private GeneratedProjectDto convertToDto(GeneratedProject generatedProject) {
        return modelMapper.map(generatedProject, GeneratedProjectDto.class);
    }

    private GeneratedProject convertToEntity(GeneratedProjectDto generatedProjectDto) {
        return modelMapper.map(generatedProjectDto, GeneratedProject.class);
    }
}
