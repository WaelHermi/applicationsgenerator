package tn.com.veganet.generateur.endpoint;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.com.veganet.generateur.domain.Fonctionnalite;
import tn.com.veganet.generateur.dto.FonctionnaliteDto;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.service.IFonctionnaliteService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated //required for @Valid on method parameters such as @RequesParam, @PathVariable, @RequestHeader
public class FonctionnaliteEndpoint extends BaseEndpoint {

    static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private IFonctionnaliteService fonctionnaliteService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/v1/fonctionnalites")
    @ApiOperation(
        value = "Get all fonctionnalites",
        notes = "Returns first N fonctionnalites specified by the size parameter with page offset specified by page parameter.",
        response = Collection.class)
    public ResponseEntity getAll(
        @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
        @ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
                List<FonctionnaliteDto> fonctionnalitesDto =
                fonctionnaliteService.findAll(pageable).stream().map(this::convertToDto).collect(Collectors.toList());
                    return ResponseEntity.ok().body(fonctionnalitesDto);
            } catch (GetException e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
            }
    }

    @GetMapping(path = "/v1/fonctionnalite/{id}")
    @ApiOperation(
        value = "Get fonctionnalite by id",
        notes = "Returns fonctionnalite for id specified.",
        response = FonctionnaliteDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Fonctionnalite not found")})
    public ResponseEntity get(@ApiParam("Fonctionnalite id") @PathVariable("id") UUID id) {

        FonctionnaliteDto fonctionnaliteDto = null;
        try {
            fonctionnaliteDto = convertToDto(fonctionnaliteService.findOne(id));
        } catch (GetException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(fonctionnaliteDto);
    }

    @PostMapping(path = "/v1/fonctionnalite",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Create new fonctionnalite",
        notes = "Creates new fonctionnalite. Returns created fonctionnalite with id.",
        response = FonctionnaliteDto.class)
    public ResponseEntity add(
        @ApiParam("Fonctionnalite") @Valid @RequestBody FonctionnaliteDto fonctionnaliteDto){

        try {
            fonctionnaliteDto = convertToDto(fonctionnaliteService.add(convertToEntity(fonctionnaliteDto)));
        } catch (CreateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(fonctionnaliteDto);
    }


    @PutMapping(path = "/v1/fonctionnalite",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiOperation(
        value = "Update existing fonctionnalite",
        notes = "Updates exisitng fonctionnalite. Returns updated fonctionnalite.",
        response = FonctionnaliteDto.class)
    public ResponseEntity update(
        @ApiParam("Fonctionnalite") @Valid @RequestBody FonctionnaliteDto fonctionnaliteDto){

        try {
            fonctionnaliteDto = convertToDto(fonctionnaliteService.update(convertToEntity(fonctionnaliteDto)));
        } catch (UpdateException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(fonctionnaliteDto);
    }

    @DeleteMapping(path = "/v1/fonctionnalite/{id}")
    @ApiOperation(
        value = "Delete fonctionnalite",
        notes = "Delete fonctionnalite. Returns Boolean.",
        response = Boolean.class)
    public ResponseEntity delete(@ApiParam("fonctionnalite id") @PathVariable("id") UUID id){

        boolean result;

        try {
            result = fonctionnaliteService.delete(id);
        } catch (DeleteException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e);
        }

        return ResponseEntity.ok().body(Collections.singletonMap("result", result));
    }

    private FonctionnaliteDto convertToDto(Fonctionnalite fonctionnalite) {
        return modelMapper.map(fonctionnalite, FonctionnaliteDto.class);
    }

    private Fonctionnalite convertToEntity(FonctionnaliteDto fonctionnaliteDto) {
        return modelMapper.map(fonctionnaliteDto, Fonctionnalite.class);
    }
}
