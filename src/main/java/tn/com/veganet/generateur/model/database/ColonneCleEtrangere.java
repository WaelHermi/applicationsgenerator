package tn.com.veganet.generateur.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.database.metadata.ColonneCleEtrangereMetaData;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ColonneCleEtrangere {

    ColonneCleEtrangereMetaData colonneCleEtrangereMetaData = new ColonneCleEtrangereMetaData();

    @Override
    public String toString() {
        return "ColonneCleEtrangere{" +
                "colonneCleEtrangereMetaData=" + colonneCleEtrangereMetaData +
                "}\n";
    }
}
