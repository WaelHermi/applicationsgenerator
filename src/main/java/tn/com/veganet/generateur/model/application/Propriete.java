package tn.com.veganet.generateur.model.application;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class Propriete implements Serializable {

    private String nomPropriete;
    private String valeurPropriete;

    public Propriete() {
    }

}

