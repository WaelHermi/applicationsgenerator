package tn.com.veganet.generateur.model.database.metadata;

import java.sql.ResultSet;
import java.sql.SQLException;

import static tn.com.veganet.generateur.util.GlobaleConstantes.*;

public class ColonneCleEtrangereMetaData implements Comparable<ColonneCleEtrangereMetaData> {

    //--- 1..4 : Primary Key Column
    private String nomCatalogueClePrimaire ; // (may be null)
    private String nomShemaClePrimaire ; // (may be null)
    private String nomTableClePrimaire ;
    private String nomColonneClePrimaire ;

    //--- 5..8 : Foreign Key Column
    private String nomCatalogueCleEtrangere ; // (may be null)
    private String nomShemaCleEtrangere ; // (may be null)
    private String nomTableCleEtrangere ;
    private String nomColonneCleEtrangere ;

    //--- 9 : KEY_SEQ : short : sequence number within a foreign key
    private short sequenceCleEtrangere ;

    //--- 10 : UPDATE_RULE : short => What happens to a foreign key when the primary key is updated:
    private short regleModification ;

    //--- 11 : DELETE_RULE : short => What happens to the foreign key when primary is deleted.
    private short regleSuppression ;

    //--- 12 : FK_NAME : String => foreign key name (may be null)
    private String nomCleEtrangere ; // (may be null)

    //--- 13 : PK_NAME : String => primary key name (may be null)
    private String nomClePrimaire ; // (may be null)

    //--- 14 : DEFERRABILITY : short => can the evaluation of foreign key constraints be deferred until commit
    private short deferrabilite ;


    public ColonneCleEtrangereMetaData() {
    }

    public ColonneCleEtrangereMetaData(ResultSet rsColonnesCleEtrangereMetaData) throws SQLException {

        this.nomCatalogueClePrimaire = rsColonnesCleEtrangereMetaData.getString(PKTABLE_CAT);
        this.nomShemaClePrimaire = rsColonnesCleEtrangereMetaData.getString(PKTABLE_SCHEM);
        this.nomTableClePrimaire = rsColonnesCleEtrangereMetaData.getString(PKTABLE_NAME);
        this.nomColonneClePrimaire = rsColonnesCleEtrangereMetaData.getString(PKCOLUMN_NAME);
        this.nomCatalogueCleEtrangere = rsColonnesCleEtrangereMetaData.getString(FKTABLE_CAT);
        this.nomShemaCleEtrangere = rsColonnesCleEtrangereMetaData.getString(FKTABLE_SCHEM);
        this.nomTableCleEtrangere = rsColonnesCleEtrangereMetaData.getString(FKTABLE_NAME);
        this.nomColonneCleEtrangere = rsColonnesCleEtrangereMetaData.getString(FKCOLUMN_NAME);
        this.sequenceCleEtrangere = rsColonnesCleEtrangereMetaData.getShort(KEY_SEQ);
        this.regleModification = rsColonnesCleEtrangereMetaData.getShort(UPDATE_RULE);
        this.regleSuppression = rsColonnesCleEtrangereMetaData.getShort(DELETE_RULE);
        this.nomCleEtrangere = rsColonnesCleEtrangereMetaData.getString(FK_NAME);
        this.nomClePrimaire = rsColonnesCleEtrangereMetaData.getString(PK_NAME);
        this.deferrabilite = rsColonnesCleEtrangereMetaData.getShort(DEFERRABILITY);
    }

    public String getNomCatalogueClePrimaire() {
        return nomCatalogueClePrimaire;
    }

    public void setNomCatalogueClePrimaire(String nomCatalogueClePrimaire) {
        this.nomCatalogueClePrimaire = nomCatalogueClePrimaire;
    }

    public String getNomShemaClePrimaire() {
        return nomShemaClePrimaire;
    }

    public void setNomShemaClePrimaire(String nomShemaClePrimaire) {
        this.nomShemaClePrimaire = nomShemaClePrimaire;
    }

    public String getNomTableClePrimaire() {
        return nomTableClePrimaire;
    }

    public void setNomTableClePrimaire(String nomTableClePrimaire) {
        this.nomTableClePrimaire = nomTableClePrimaire;
    }

    public String getNomColonneClePrimaire() {
        return nomColonneClePrimaire;
    }

    public void setNomColonneClePrimaire(String nomColonneClePrimaire) {
        this.nomColonneClePrimaire = nomColonneClePrimaire;
    }

    public String getNomCatalogueCleEtrangere() {
        return nomCatalogueCleEtrangere;
    }

    public void setNomCatalogueCleEtrangere(String nomCatalogueCleEtrangere) {
        this.nomCatalogueCleEtrangere = nomCatalogueCleEtrangere;
    }

    public String getNomShemaCleEtrangere() {
        return nomShemaCleEtrangere;
    }

    public void setNomShemaCleEtrangere(String nomShemaCleEtrangere) {
        this.nomShemaCleEtrangere = nomShemaCleEtrangere;
    }

    public String getNomTableCleEtrangere() {
        return nomTableCleEtrangere;
    }

    public void setNomTableCleEtrangere(String nomTableCleEtrangere) {
        this.nomTableCleEtrangere = nomTableCleEtrangere;
    }

    public String getNomColonneCleEtrangere() {
        return nomColonneCleEtrangere;
    }

    public void setNomColonneCleEtrangere(String nomColonneCleEtrangere) {
        this.nomColonneCleEtrangere = nomColonneCleEtrangere;
    }

    public short getSequenceCleEtrangere() {
        return sequenceCleEtrangere;
    }

    public void setSequenceCleEtrangere(short sequenceCleEtrangere) {
        this.sequenceCleEtrangere = sequenceCleEtrangere;
    }

    public short getRegleModification() {
        return regleModification;
    }

    public void setRegleModification(short regleModification) {
        this.regleModification = regleModification;
    }

    public short getRegleSuppression() {
        return regleSuppression;
    }

    public void setRegleSuppression(short regleSuppression) {
        this.regleSuppression = regleSuppression;
    }

    public String getNomCleEtrangere() {
        return nomCleEtrangere;
    }

    public void setNomCleEtrangere(String nomCleEtrangere) {
        this.nomCleEtrangere = nomCleEtrangere;
    }

    public String getNomClePrimaire() {
        return nomClePrimaire;
    }

    public void setNomClePrimaire(String nomClePrimaire) {
        this.nomClePrimaire = nomClePrimaire;
    }

    public short getDeferrabilite() {
        return deferrabilite;
    }

    public void setDeferrabilite(short deferrabilite) {
        this.deferrabilite = deferrabilite;
    }

    @Override
    public String toString() {
        return "ColonneCleEtrangereMetaData{" +
                "nomCatalogueClePrimaire='" + nomCatalogueClePrimaire + '\'' +
                ", nomShemaClePrimaire='" + nomShemaClePrimaire + '\'' +
                ", nomTableClePrimaire='" + nomTableClePrimaire + '\'' +
                ", nomColonneClePrimaire='" + nomColonneClePrimaire + '\'' +
                ", nomCatalogueCleEtrangere='" + nomCatalogueCleEtrangere + '\'' +
                ", nomShemaCleEtrangere='" + nomShemaCleEtrangere + '\'' +
                ", nomTableCleEtrangere='" + nomTableCleEtrangere + '\'' +
                ", nomColonneCleEtrangere='" + nomColonneCleEtrangere + '\'' +
                ", sequenceCleEtrangere=" + sequenceCleEtrangere +
                ", regleModification=" + regleModification +
                ", regleSuppression=" + regleSuppression +
                ", nomCleEtrangere='" + nomCleEtrangere + '\'' +
                ", nomClePrimaire='" + nomClePrimaire + '\'' +
                ", deferrabilite=" + deferrabilite +
                '}';
    }

    @Override
    public int compareTo(ColonneCleEtrangereMetaData colonneCleEtrangereMetaData) {
        return 0;
    }
}
