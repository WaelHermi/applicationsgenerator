package tn.com.veganet.generateur.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import tn.com.veganet.generateur.model.application.Entity;
import tn.com.veganet.generateur.model.database.metadata.TableMetaData;
import tn.com.veganet.generateur.model.type.InheritanceType;
import tn.com.veganet.generateur.util.EntityNamingFunctions;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class Table {

    private TableMetaData tableMetaData;

    private boolean aClePrimaire   = false ;

    private String nomCLePrimaire  = null ;

    private ClePrimaire clePrimaire = new ClePrimaire();

    List<Colonne> colonnes = new ArrayList<>();

    List<CleEtrangere> cleEtrangeres = new ArrayList<>();

    public Table() {
    }

    public Table(TableMetaData tableMetaData) {
        this.tableMetaData = tableMetaData;
    }


    public Entity getEntity() {
        Entity entity = new Entity();
        entity.setNomEntity(EntityNamingFunctions.FromTableToEntityName(this.getTableMetaData().getNomTable()));
        entity.setClePrimaire(this.getClePrimaire().getClePrimaire(this.colonnes));
        entity.setAttributs(new ArrayList<>());
        entity.setNomMere("Object");
        entity.setGenerateControleur(true);
        entity.setInheritanceType(InheritanceType.NON);
        for (Colonne colonne: this.colonnes
             ) {
            entity.getAttributs().add(colonne.getAttribut());
        }

        return entity;
    }

    @Override
    public String toString() {
        return "Table{" +
                "tableMetaData=" + tableMetaData +
                ", aClePrimaire=" + aClePrimaire +
                ", nomCLePrimaire='" + nomCLePrimaire + '\'' +
                ", clePrimaire=" + clePrimaire +
                ", colonnes=" + colonnes +
                ", cleEtrangeres=" + cleEtrangeres +
                "}\n";
    }
}
