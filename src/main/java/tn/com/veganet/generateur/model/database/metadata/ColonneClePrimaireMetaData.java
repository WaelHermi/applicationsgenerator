package tn.com.veganet.generateur.model.database.metadata;

import java.sql.ResultSet;
import java.sql.SQLException;

import static tn.com.veganet.generateur.util.GlobaleConstantes.*;

public class ColonneClePrimaireMetaData {

    private String  NomCatalogue ; // table catalog (may be null)

    private String  NomSchema ; // table schema (may be null)

    private String  NomTable ; // table name

    private String  Nomcolumn ; // column name

    private short   sequenceClePrimaire = 0 ; // sequence number within primary key

    private String  NomClePrimaire ; // primary key name (may be null)

    public ColonneClePrimaireMetaData() {
    }

    public ColonneClePrimaireMetaData(ResultSet rsColonneClePrimaireMetaData) throws SQLException {

        NomCatalogue =  rsColonneClePrimaireMetaData.getString(TABLE_CAT);
        NomSchema = rsColonneClePrimaireMetaData.getString(TABLE_SCHEM);
        NomTable = rsColonneClePrimaireMetaData.getString(TABLE_NAME);
        Nomcolumn = rsColonneClePrimaireMetaData.getString(COLUMN_NAME);
        sequenceClePrimaire = rsColonneClePrimaireMetaData.getShort(KEY_SEQ);
        NomClePrimaire = rsColonneClePrimaireMetaData.getString(PK_NAME);
    }



    public String getNomCatalogue() {
        return NomCatalogue;
    }

    public void setNomCatalogue(String nomCatalogue) {
        NomCatalogue = nomCatalogue;
    }

    public String getNomSchema() {
        return NomSchema;
    }

    public void setNomSchema(String nomSchema) {
        NomSchema = nomSchema;
    }

    public String getNomTable() {
        return NomTable;
    }

    public void setNomTable(String nomTable) {
        NomTable = nomTable;
    }

    public String getNomcolumn() {
        return Nomcolumn;
    }

    public void setNomcolumn(String nomcolumn) {
        Nomcolumn = nomcolumn;
    }

    public short getSequenceClePrimaire() {
        return sequenceClePrimaire;
    }

    public void setSequenceClePrimaire(short sequenceClePrimaire) {
        this.sequenceClePrimaire = sequenceClePrimaire;
    }

    public String getNomClePrimaire() {
        return NomClePrimaire;
    }

    public void setNomClePrimaire(String nomClePrimaire) {
        NomClePrimaire = nomClePrimaire;
    }

    @Override
    public String toString() {
        return "ColonneClePrimaireMetaData{" +
                "NomCatalogue='" + NomCatalogue + '\'' +
                ", NomSchema='" + NomSchema + '\'' +
                ", NomTable='" + NomTable + '\'' +
                ", Nomcolumn='" + Nomcolumn + '\'' +
                ", sequenceClePrimaire=" + sequenceClePrimaire +
                ", NomClePrimaire='" + NomClePrimaire + '\'' +
                '}';
    }
}
