package tn.com.veganet.generateur.model.database.metadata;

import tn.com.veganet.generateur.util.GlobaleConstantes;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableMetaData {

    private String nomTable ;

    // Typical types are :
    // "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM".
    private String typeTable ;

    private String nomCatalogue; // (may be null)

    private String nomSchema ; // (may be null)

    private String commentaire ; // explanatory comment on the table

    public TableMetaData() {}

    public TableMetaData(ResultSet rsTableMetaData) throws SQLException {

        this.nomTable = rsTableMetaData.getString(GlobaleConstantes.TABLE_NAME);
        this.typeTable = rsTableMetaData.getString(GlobaleConstantes.TABLE_TYPE);
        this.nomCatalogue = rsTableMetaData.getString(GlobaleConstantes.TABLE_CAT);
        this.nomSchema = rsTableMetaData.getString(GlobaleConstantes.TYPE_NAME);
        this.commentaire = rsTableMetaData.getString(GlobaleConstantes.REMARKS);
    }

    public String getNomTable() {
        return nomTable;
    }

    public void setNomTable(String nomTable) {
        this.nomTable = nomTable;
    }

    public String getTypeTable() {
        return typeTable;
    }

    public void setTypeTable(String typeTable) {
        this.typeTable = typeTable;
    }

    public String getNomCatalogue() {
        return nomCatalogue;
    }

    public void setNomCatalogue(String nomCatalogue) {
        this.nomCatalogue = nomCatalogue;
    }

    public String getNomSchema() {
        return nomSchema;
    }

    public void setNomSchema(String nomSchema) {
        this.nomSchema = nomSchema;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public String toString() {
        return "Table{" +
                "nomTable='" + nomTable + '\'' +
                ", typeTable='" + typeTable + '\'' +
                ", nomCatalogue='" + nomCatalogue + '\'' +
                ", nomSchema='" + nomSchema + '\'' +
                ", commentaire='" + commentaire + '\'' +
                '}';
    }
}
