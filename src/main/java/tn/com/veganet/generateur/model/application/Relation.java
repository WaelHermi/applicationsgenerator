package tn.com.veganet.generateur.model.application;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.type.CascadeType;
import tn.com.veganet.generateur.model.type.FetchType;
import tn.com.veganet.generateur.model.type.RelationJPAType;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Getter @Setter
public class Relation implements Serializable {

    // Nom de l'instance de la relation dans l'entité principale
    private String nomRelation;
    // Nom de l'instance de la relation dans l'entité cible
    private String biNomRelation;
    private RelationJPAType relationJPAType;
    // Nom de l'éntité cible
    private String nomEntity;
    // L'entité principale
    @JsonIgnore
    private Entity source;
    // L'entité cible
    @JsonIgnore
    private Entity destination;
    private Boolean bidirectionnelle;
    private List<CascadeType> cascadeTypes;
    private FetchType fetchType;
    private Boolean findBy;
    private Boolean jsonIgnore;
    private Boolean dtoRelation;

    public static Relation getRelationForDatabase(RelationJPAType relationJPAType){

        Relation relation = new Relation();
        relation.setRelationJPAType(relationJPAType);
        relation.setBidirectionnelle(false);
        relation.setCascadeTypes(null);
        relation.setFetchType(FetchType.DEFAULT);
        relation.setFindBy(false);
        relation.setJsonIgnore(false);
        relation.setDtoRelation(true);

        return relation;
    }

    @Override
    public String toString() {

        String sourceName = "";

        if(source != null){

            sourceName = source.getNomEntity();
        }

        return "Relation{" +
                "nomRelation='" + nomRelation + '\'' +
                ", relationJPAType=" + relationJPAType +
                ", nomEntity='" + nomEntity + '\'' +
                ", source=" + sourceName +
                ", destination=" + destination.getNomEntity() +
                ", bidirectionnelle=" + bidirectionnelle +
                ", cascadeTypes=" + cascadeTypes +
                ", fetchType=" + fetchType +
                ", findBy=" + findBy +
                ", jsonIgnore=" + jsonIgnore +
                ", dtoRelation=" + dtoRelation +
                '}';
    }
}
