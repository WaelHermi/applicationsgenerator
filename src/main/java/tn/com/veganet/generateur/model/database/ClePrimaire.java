package tn.com.veganet.generateur.model.database;

import tn.com.veganet.generateur.model.application.Attribut;
import tn.com.veganet.generateur.util.EntityNamingFunctions;

import java.util.ArrayList;
import java.util.List;

public class ClePrimaire {

    List<ColonneClePrimaire> colonneClePrimaires = new ArrayList<>();

    public ClePrimaire() {
    }

    public ClePrimaire(List<ColonneClePrimaire> colonneClePrimaires) {
        this.colonneClePrimaires = colonneClePrimaires;
    }

    public List<ColonneClePrimaire> getColonneClePrimaires() {
        return colonneClePrimaires;
    }

    public void setColonneClePrimaires(List<ColonneClePrimaire> colonneClePrimaires) {
        this.colonneClePrimaires = colonneClePrimaires;
    }

    public tn.com.veganet.generateur.model.application.ClePrimaire getClePrimaire(List<Colonne> colonnes) {
        tn.com.veganet.generateur.model.application.ClePrimaire clePrimaire = new tn.com.veganet.generateur.model.application.ClePrimaire();
        if(this.getColonneClePrimaires().size() > 0) {

            clePrimaire.setNomClePrimaire(EntityNamingFunctions.FromTableColonneToEntityAttributName(this.getColonneClePrimaires().get(0).getColonneClePrimaireMetaData().getNomClePrimaire()));
            clePrimaire.setAttributs(new ArrayList<>());
            for (ColonneClePrimaire colonne : this.getColonneClePrimaires()
            ) {
                clePrimaire.getAttributs().add(colonne.getAttributClePrimaire(colonnes));
            }
        }

        return clePrimaire;
    }

    @Override
    public String toString() {
        return "ClePrimaire{" +
                "colonneClePrimaires=" + colonneClePrimaires +
                '}';
    }
}
