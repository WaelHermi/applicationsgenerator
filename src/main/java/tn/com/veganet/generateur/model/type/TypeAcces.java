package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TypeAcces {

    AJOUT(0001),
    CONSULTATION(0010),
    MODIFICATION(0100),
    SUPRESSION(1000);

    private Integer codeAcces;

}
