package tn.com.veganet.generateur.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.application.Attribut;
import tn.com.veganet.generateur.model.database.metadata.ColonneMetaData;
import tn.com.veganet.generateur.model.type.BaseColonneType;
import tn.com.veganet.generateur.model.type.Visibilite;
import tn.com.veganet.generateur.util.EntityNamingFunctions;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Colonne {


    private ColonneMetaData colonneMetaData ;

    private boolean dansClePrimaire = false ;

    private int     sequenceClePrimaire = 0 ;

    private int     utiliseDansCleEtrangere = 0 ; // 1 : in one FK, 2 : in two FK, etc...

    private boolean incrementeAutomatiquement = false ;

    private Boolean unique = false;


    public Attribut getAttribut() {
        Attribut attribut = new Attribut();
        attribut.setNomAttribut(EntityNamingFunctions.FromTableColonneToEntityAttributName(this.colonneMetaData.getNomColonne()));
        attribut.setVisibiliteAttribut(Visibilite.PRIVATE);
        attribut.setAttributType(BaseColonneType.valueOf(this.colonneMetaData.getDbNomType().toUpperCase()).getAttributType());
        // this.unique = false;
        attribut.setNullable(!this.colonneMetaData.isNotNull());
        attribut.setGenerationType(BaseColonneType.valueOf(this.colonneMetaData.getDbNomType().toUpperCase()).getGenerationType());
        attribut.setFindBy(false);
        attribut.setDtoAttribut(true);
        attribut.setUnique(this.unique);
        return attribut;
    }

    @Override
    public String toString() {
        return "Colonne{" +
                "colonneMetaData=" + colonneMetaData +
                ", dansClePrimaire=" + dansClePrimaire +
                ", sequenceClePrimaire=" + sequenceClePrimaire +
                ", utiliseDansCleEtrangere=" + utiliseDansCleEtrangere +
                ", incrementeAutomatiquement=" + incrementeAutomatiquement +
                "}\n";
    }
}
