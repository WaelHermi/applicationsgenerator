package tn.com.veganet.generateur.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class AuthenticationRequest implements Serializable {

    private String usernameUtilisateur;
    private String passwordUtilisateur;
    private Boolean status = false;

}
