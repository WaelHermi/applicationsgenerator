package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InheritanceType {

    NON("Pas d'héritage",null, (short)0),
    SINGLE_TABLE("Une seule table", "InheritanceType.SINGLE_TABLE", (short)1),
    TABLE_PER_CLASS("Table par classe", "InheritanceType.SINGLE_TABLE", (short)2),
    JOINED("Rejointe", "InheritanceType.JOINED", (short)3);

    private String nomType;
    private String valeur;
    private short order;

}
