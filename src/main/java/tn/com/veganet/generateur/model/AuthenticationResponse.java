package tn.com.veganet.generateur.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import tn.com.veganet.generateur.dto.UtilisateurDto;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class AuthenticationResponse implements Serializable {

    private UtilisateurDto utilisateur;
    private final String jwt;

}
