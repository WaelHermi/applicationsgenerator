package tn.com.veganet.generateur.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EnumValeursWrapper implements Serializable {

    private String nomEnum;
    private String valeurEnum;

}
