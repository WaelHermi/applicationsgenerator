package tn.com.veganet.generateur.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class PasswordModel {
    private String ancienMotDePasse;
    private String nouveauMotDePasse;
}
