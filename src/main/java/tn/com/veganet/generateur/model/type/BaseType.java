package tn.com.veganet.generateur.model.type;

import lombok.Getter;
import lombok.Setter;
import tn.com.veganet.generateur.domain.DependanceBase;
import tn.com.veganet.generateur.domain.ProprieteBase;

import java.util.Collection;

import static tn.com.veganet.generateur.util.BaseConstantes.ENTITY_ANNOTATION;
import static tn.com.veganet.generateur.util.BaseConstantes.ID_ANNOTATION;

@Getter
public enum BaseType {

    POSTGRESQL("PostgreSQL", ID_ANNOTATION(), ENTITY_ANNOTATION(), "postgresql"),
    MYSQL("MySQL", ID_ANNOTATION(), ENTITY_ANNOTATION(), "mysql");


    BaseType(String nomType, String idAnnotation, String entityAnnotation, String connector) {
        this.nomType = nomType;
        this.idAnnotation = idAnnotation;
        this.entityAnnotation = entityAnnotation;
        this.connector = connector;
    }

    private String nomType;
    private String idAnnotation;
    private String entityAnnotation;
    private String connector;
    @Setter
    private Collection<ProprieteBase> proprietes;
    @Setter
    private Collection<DependanceBase> dependances;
}
