package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public enum GenerationType {

    NON(null, "Non"),
    TABLE ("GenerationType.TABLE", "Table"),
    SEQUENCE ("GenerationType.SEQUENCE", "Sequence"),
    IDENTITY ("GenerationType.IDENTITY", "Identity"),
    AUTO ("GenerationType.AUTO", "Auto"),
    AUTRE("", "Autre");

    private String valeur;
    private String nomGenerationType;

    private final String importation ="javax.persistence.GenerationType";

    GenerationType(String valeur, String nomGenerationType) {
        this.valeur = valeur;
        this.nomGenerationType = nomGenerationType;
    }

}
