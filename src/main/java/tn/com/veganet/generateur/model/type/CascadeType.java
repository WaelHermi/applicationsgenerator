package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CascadeType {

    ALL ("ALL","CascadeType.ALL"),
    PERSIST ("PERSIST","CascadeType.PERSIST"),
    MERGE ("MERGE","CascadeType.MERGE"),
    REMOVE ("REMOVE","CascadeType.REMOVE"),
    REFRESH ("REFRESH","CascadeType.REFRESH"),
    DETACH ("DETACH","CascadeType.DETACH");

    private String nomCascadeType;
    private String valeur;
}
