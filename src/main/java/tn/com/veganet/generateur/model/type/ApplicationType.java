package tn.com.veganet.generateur.model.type;

import lombok.Getter;
import lombok.Setter;
import tn.com.veganet.generateur.domain.DependanceApplication;
import tn.com.veganet.generateur.domain.ProprieteApplication;

import java.util.Collection;

@Getter
public enum ApplicationType {

    MONOLITHIQUE("Monolithique"),
    MICROSERVICE("Microservice");


    ApplicationType(String nomType) {
        this.nomType = nomType;
    }

    private String nomType;
    private String modeleChemin;
    @Setter
    private Collection<ProprieteApplication> proprietes;
    @Setter
    private Collection<DependanceApplication> dependances;


}
