package tn.com.veganet.generateur.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@Getter @Setter
public class ResetPasswordRequest implements Serializable {
    private String token;
    private String password;
}
