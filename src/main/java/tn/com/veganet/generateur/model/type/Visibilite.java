package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Visibilite {

    PRIVATE("private", "Private"),
    PROTECTED("protected", "Protected"),
    PUBLIC("public", "Public");

    private String valeur;
    private String nomVisibilite;

}
