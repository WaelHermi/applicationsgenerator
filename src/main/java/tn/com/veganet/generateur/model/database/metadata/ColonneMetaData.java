package tn.com.veganet.generateur.model.database.metadata;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import static tn.com.veganet.generateur.util.GlobaleConstantes.*;

public class ColonneMetaData {

    private String  nomcatalogue ; // table catalog (may be null)

    private String  nomSchema ; // table schema (may be null)

    private String  nomTable ; // table name

    private String  nomColonne = "" ;

    private int     jdbcCodeType = 0 ;

    private String  dbNomType = "" ;

    private boolean notNull = false ;

    private int     size = 0 ;

    private int     decimalDigits = 0 ; // the number of fractional digits

    private int     numPrecRadix = 0 ; // base ( 2 or 10 )

    private String  defaultValue ;

    private int     charOctetLength = 0 ;

    private int     ordinalPosition = 0 ;

    private String  comment ; // explanatory comment on the table

    public ColonneMetaData() {
    }

    public ColonneMetaData(ResultSet colonneMetaData) throws SQLException {

        this.nomcatalogue = colonneMetaData.getString(TABLE_CAT);
        this.nomSchema = colonneMetaData.getString(TABLE_SCHEM);
        this.nomTable = colonneMetaData.getString(TABLE_NAME);
        this.nomColonne = colonneMetaData.getString(COLUMN_NAME);
        this.jdbcCodeType = colonneMetaData.getInt(DATA_TYPE);
        this.dbNomType = colonneMetaData.getString(TYPE_NAME);
        this.notNull = false;

        int nullable = colonneMetaData.getInt(NULLABLE);

        // 3 values :
        // * columnNoNulls - might not allow NULL values
        // * columnNullable - definitely allows NULL values
        // * columnNullableUnknown - nullability unknown
        if ( nullable == DatabaseMetaData.columnNoNulls )
        {
            notNull = true ;
        }

        this.size = colonneMetaData.getInt(COLUMN_SIZE);
        this.decimalDigits = colonneMetaData.getInt(DECIMAL_DIGITS);
        this.numPrecRadix = colonneMetaData.getInt(NUM_PREC_RADIX);
        this.defaultValue = colonneMetaData.getString(COLUMN_DEF);
        this.charOctetLength = colonneMetaData.getInt(CHAR_OCTET_LENGTH);
        this.ordinalPosition = colonneMetaData.getInt(ORDINAL_POSITION);
        this.comment = colonneMetaData.getString(REMARKS);
    }

    public String getNomcatalogue() {
        return nomcatalogue;
    }

    public void setNomcatalogue(String nomcatalogue) {
        this.nomcatalogue = nomcatalogue;
    }

    public String getNomSchema() {
        return nomSchema;
    }

    public void setNomSchema(String nomSchema) {
        this.nomSchema = nomSchema;
    }

    public String getNomTable() {
        return nomTable;
    }

    public void setNomTable(String nomTable) {
        this.nomTable = nomTable;
    }

    public String getNomColonne() {
        return nomColonne;
    }

    public void setNomColonne(String nomColonne) {
        this.nomColonne = nomColonne;
    }

    public int getJdbcCodeType() {
        return jdbcCodeType;
    }

    public void setJdbcCodeType(int jdbcCodeType) {
        this.jdbcCodeType = jdbcCodeType;
    }

    public String getDbNomType() {
        return dbNomType;
    }

    public void setDbNomType(String dbNomType) {
        this.dbNomType = dbNomType;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getDecimalDigits() {
        return decimalDigits;
    }

    public void setDecimalDigits(int decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

    public int getNumPrecRadix() {
        return numPrecRadix;
    }

    public void setNumPrecRadix(int numPrecRadix) {
        this.numPrecRadix = numPrecRadix;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getCharOctetLength() {
        return charOctetLength;
    }

    public void setCharOctetLength(int charOctetLength) {
        this.charOctetLength = charOctetLength;
    }

    public int getOrdinalPosition() {
        return ordinalPosition;
    }

    public void setOrdinalPosition(int ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ColonneMetaData{" +
                "nomcatalogue='" + nomcatalogue + '\'' +
                ", nomSchema='" + nomSchema + '\'' +
                ", nomTable='" + nomTable + '\'' +
                ", nomColonne='" + nomColonne + '\'' +
                ", jdbcCodeType=" + jdbcCodeType +
                ", dbNomType='" + dbNomType + '\'' +
                ", notNull=" + notNull +
                ", size=" + size +
                ", decimalDigits=" + decimalDigits +
                ", numPrecRadix=" + numPrecRadix +
                ", defaultValue='" + defaultValue + '\'' +
                ", charOctetLength=" + charOctetLength +
                ", ordinalPosition=" + ordinalPosition +
                ", comment='" + comment + '\'' +
                '}';
    }
}
