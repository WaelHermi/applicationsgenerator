package tn.com.veganet.generateur.model.application;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.type.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Getter @Setter
public class Application implements Serializable {

    @NotNull(message = "Le groupe de l'application ne peut pas être nul")
    private String groupApplication;
    @NotNull(message = "L'artifact de l'application ne peut pas être nul")
    private String artifactApplication;
    @NotNull(message = "La version de l'application ne peut pas être nulle")
    private String versionApplication;
    private String descriptionApplication;
    @NotNull(message = "Le port de l'application ne peut pas être nul")
    private String portApplication;
    @NotNull(message = "Le type de packaging de l'application ne peut pas être nul")
    private Packaging packagingApplication;
    @NotNull(message = "Le type de l'application ne peut pas être nul")
    private ApplicationType applicationType;
    private AuthentificationType authentificationType;
    @NotNull(message = "L'utilisation ou pas de lombok dans l'application doit être spécifier")
    private Boolean lombok;
    //private SpringVersion springVersion;
    //private JavaVersion javaVersion;
    @Valid
    private List<Entity> entities;
    @Valid
    private Base base;

    public String getPackageApplication() {
        String packageApplication = this.groupApplication + "/" + this.artifactApplication;
        return packageApplication.replace(".", "/");
    }

    @Override
    public String toString() {
        return "Application{" +
                "groupApplication='" + groupApplication + '\'' +
                ", artifactApplication='" + artifactApplication + '\'' +
                ", versionApplication='" + versionApplication + '\'' +
                ", descriptionApplication='" + descriptionApplication + '\'' +
                ", portApplication='" + portApplication + '\'' +
                ", packagingApplication=" + packagingApplication +
                ", applicationType=" + applicationType +
                ", authentificationType=" + authentificationType +
                ", lombok=" + lombok +
                ", entities=" + entities +
                ", base=" + base +
                '}';
    }
}
