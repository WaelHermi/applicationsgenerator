package tn.com.veganet.generateur.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CleEtrangere {

    private String nomCleEtrangere;

    List<ColonneCleEtrangere> colonneCleEtrangereList = new ArrayList<>();

    @Override
    public String toString() {
        return "CleEtrangere{" +
                "nomCleEtrangere='" + nomCleEtrangere + '\'' +
                ", colonneCleEtrangereList=" + colonneCleEtrangereList +
                "}\n";
    }
}
