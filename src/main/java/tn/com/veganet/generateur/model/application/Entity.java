package tn.com.veganet.generateur.model.application;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.database.Table;
import tn.com.veganet.generateur.model.type.InheritanceType;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Getter @Setter
public class Entity implements Serializable {

    @NotNull(message = "Le nom de l'entité ne peut pas être nul")
    private String nomEntity;
    @NotNull(message = "La clé primaire l'entité ne peut pas être nulle")
    @Valid
    private ClePrimaire clePrimaire;
    @Valid
    private List<Attribut> attributs;
    @Valid
    private List<Relation> relations;
    @NotNull(message = "Le choix de génération d'un contrôleur pour l'entité ne peut pas être nul")
    private Boolean generateControleur;

    private String nomMere;
    private Entity mere;

    private InheritanceType inheritanceType;
    private String discriminatorColumn;
    private String discriminatorValue;

    private String primaryKeyJoinColumn;

    @Override
    public String toString() {
        return "Entity{" +
                "nomEntity='" + nomEntity + '\'' +
                ", clePrimaire=" + clePrimaire +
                ", attributs=" + attributs +
                ", relations=" + relations +
                ", generateControleur=" + generateControleur +
                ", nomMere='" + nomMere + '\'' +
                ", mere=" + mere +
                ", inheritanceType=" + inheritanceType +
                ", discriminatorColumn='" + discriminatorColumn + '\'' +
                ", discriminatorValue='" + discriminatorValue + '\'' +
                ", primaryKeyJoinColumn='" + primaryKeyJoinColumn + '\'' +
                '}';
    }

}
