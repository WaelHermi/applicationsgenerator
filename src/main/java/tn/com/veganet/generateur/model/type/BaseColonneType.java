package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BaseColonneType {

    CHARACTER(AttributType.STRING, GenerationType.NON),
    VARCHAR(AttributType.STRING, GenerationType.NON),
    LONGVARCHAR(AttributType.STRING, GenerationType.NON),
    NUMERIC(AttributType.BIGDECIMAL, GenerationType.NON),
    DECIMAL(AttributType.BIGDECIMAL, GenerationType.NON),
    BIT(AttributType.BOOLEAN, GenerationType.NON),
    BOOL(AttributType.BOOLEAN, GenerationType.NON),
    TINYINT(AttributType.BYTE, GenerationType.NON),
    SMALLINT(AttributType.SHORT, GenerationType.NON),
    INTEGER(AttributType.INTEGER, GenerationType.NON),
    INT4(AttributType.INTEGER, GenerationType.NON),
    INT8(AttributType.LONG, GenerationType.NON),
    SERIAL(AttributType.INTEGER, GenerationType.IDENTITY),
    BIGINT(AttributType.LONG, GenerationType.NON),
    REAL(AttributType.FLOAT, GenerationType.NON),
    FLOAT(AttributType.DOUBLE, GenerationType.NON),
    DOUBLE(AttributType.DOUBLE, GenerationType.NON),
    //BINARY("byte[]"),
    //VARBINARY("byte[]"),
    //LONGVARBINARY("byte[]"),
    DATE(AttributType.DATE, GenerationType.NON),
    TIME(AttributType.TIME, GenerationType.NON),
    TIMESTAMP(AttributType.TIMESTAMP, GenerationType.NON),
    BLOB(AttributType.BLOB, GenerationType.NON),
    UUID(AttributType.UUID, GenerationType.AUTRE),
    FLOAT4(AttributType.FLOAT, GenerationType.NON);

    private AttributType attributType;
    private GenerationType generationType;
}
