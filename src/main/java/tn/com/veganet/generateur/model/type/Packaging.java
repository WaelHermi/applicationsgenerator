package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Packaging {

    JAR("Jar", "jar"),
    WAR("War", "war");

    private String nomPackaging;
    private String valeurPackaging;

}
