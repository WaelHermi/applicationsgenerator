package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RelationJPAType {

    ONETOONE("OneToOne","@OneToOne", "@OneToOne"),
    ONETOMANY( "OneToMany","@OneToMany", "@ManyToOne"),
    MANYTOONE( "ManyToOne", "@ManyToOne", "@OneToMany"),
    MANYTOMANY( "ManyToMany", "@ManyToMany", "@ManyToMany");

    private String nomRelation;
    private String annotation;
    private String inverseAnnotation;

}
