package tn.com.veganet.generateur.model.type;

import lombok.Getter;
import lombok.Setter;
import tn.com.veganet.generateur.domain.DependanceAuthentification;
import tn.com.veganet.generateur.domain.ProprieteAuthentification;

import java.util.Collection;

@Getter
public enum AuthentificationType {

    NON("Non"),
    JWT("JWT"),
    BASIC("Basic"),
    ;

    AuthentificationType(String nomType) {
        this.nomType = nomType;
    }

    private String nomType;
    @Setter
    private Collection<ProprieteAuthentification> proprietes;
    @Setter
    private Collection<DependanceAuthentification> dependances;
}
