package tn.com.veganet.generateur.model.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FetchType {

    DEFAULT("Default", null),
    EAGER("Eager","FetchType.EAGER"),
    LAZY("Lazy","FetchType.LAZY");

    private String nomFetch;
    private String valeur;

}
