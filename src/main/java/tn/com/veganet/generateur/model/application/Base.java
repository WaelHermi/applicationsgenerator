package tn.com.veganet.generateur.model.application;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.type.BaseType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@NoArgsConstructor
@Getter @Setter
public class Base implements Serializable {

    @NotNull(message = "Le type de la base de données ne peut pas être nul")
    private BaseType baseType;
    @NotNull(message = "Le nom de la base de données ne peut pas être nul")
    private String nomBase;
    @NotNull(message = "L'adresse ip de la base de données ne peut pas être nul")
    private String ipBase;
    @NotNull(message = "Le port de la base de données ne peut pas être nul")
    private String portBase;
    @NotNull(message = "Le nom d'utilisateur de la base de données ne peut pas être nul")
    private String usernameBase;
    @NotNull(message = "Le mot de passe de l'utilisateur de la base de données ne peut pas être nul")
    private String passwordBase;

    public Base(BaseType baseType, String nomBase, String ipBase, String portBase, String usernameBase, String passwordBase) {
        this.baseType = baseType;
        this.nomBase = nomBase;
        this.ipBase = ipBase;
        this.portBase = portBase;
        this.usernameBase = usernameBase;
        this.passwordBase = passwordBase;
    }

}
