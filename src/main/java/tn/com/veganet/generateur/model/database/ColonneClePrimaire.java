package tn.com.veganet.generateur.model.database;

import tn.com.veganet.generateur.model.application.Attribut;
import tn.com.veganet.generateur.model.database.metadata.ColonneClePrimaireMetaData;
import tn.com.veganet.generateur.model.type.BaseColonneType;
import tn.com.veganet.generateur.model.type.GenerationType;
import tn.com.veganet.generateur.model.type.Visibilite;
import tn.com.veganet.generateur.util.EntityNamingFunctions;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public class ColonneClePrimaire {

    private ColonneClePrimaireMetaData colonneClePrimaireMetaData;

    public ColonneClePrimaire() {
    }

    public ColonneClePrimaire(ColonneClePrimaireMetaData colonneClePrimaireMetaData) {
        this.colonneClePrimaireMetaData = colonneClePrimaireMetaData;
    }

    public ColonneClePrimaireMetaData getColonneClePrimaireMetaData() {
        return colonneClePrimaireMetaData;
    }

    public void setColonneClePrimaireMetaData(ColonneClePrimaireMetaData colonneClePrimaireMetaData) {
        this.colonneClePrimaireMetaData = colonneClePrimaireMetaData;
    }

    public Attribut getAttributClePrimaire(List<Colonne> colonnes) {
        Attribut attribut = new Attribut();
        attribut.setNomAttribut(EntityNamingFunctions.FromTableColonneToEntityAttributName(this.colonneClePrimaireMetaData.getNomcolumn()));
        attribut.setVisibiliteAttribut(Visibilite.PRIVATE);

        Optional<Colonne> colonneOptional = colonnes.stream().filter(element -> element.getColonneMetaData().getNomColonne().equals(colonneClePrimaireMetaData.getNomcolumn()) ).findFirst();
        if(colonneOptional.isPresent()){
            attribut.setAttributType(BaseColonneType.valueOf(colonneOptional.get().getColonneMetaData().getDbNomType().toUpperCase()).getAttributType());
            attribut.setGenerationType(BaseColonneType.valueOf(colonneOptional.get().getColonneMetaData().getDbNomType().toUpperCase()).getGenerationType());
        }

        return attribut;
    }

    @Override
    public String toString() {
        return "ColonneClePrimaire{" +
                "colonneClePrimaireMetaData=" + colonneClePrimaireMetaData +
                "}\n";
    }
}
