package tn.com.veganet.generateur.model.application;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.database.Colonne;
import tn.com.veganet.generateur.model.database.ColonneClePrimaire;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class ClePrimaire implements Serializable {

    private String nomClePrimaire;
    @NotNull(message = "Une clé primaire doit avoir au moins un attribut")
    @Valid
    private List<Attribut> attributs;

    @Override
    public String toString() {
        return "ClePrimaire{" +
                "nomClePrimaire='" + nomClePrimaire + '\'' +
                ", attributs=" + attributs +
                "}\n";
    }
}
