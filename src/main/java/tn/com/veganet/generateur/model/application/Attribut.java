package tn.com.veganet.generateur.model.application;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.model.database.Colonne;
import tn.com.veganet.generateur.model.database.ColonneClePrimaire;
import tn.com.veganet.generateur.model.type.AttributType;
import tn.com.veganet.generateur.model.type.BaseColonneType;
import tn.com.veganet.generateur.model.type.GenerationType;
import tn.com.veganet.generateur.model.type.Visibilite;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@NoArgsConstructor
@Getter @Setter
public class Attribut implements Serializable {

    @NotNull(message = "Le nom de l'attribut ne peut pas être nul")
    @NotEmpty(message = "Le nom de l'attribut ne peut pas être vide")
    private String nomAttribut;
    private Visibilite visibiliteAttribut;
    private AttributType attributType;
    private Boolean unique;
    private Boolean nullable;
    private GenerationType generationType;
    private Boolean findBy;
    private Boolean dtoAttribut;

    @Override
    public String toString() {
        return "Attribut{" +
                "nomAttribut='" + nomAttribut + '\'' +
                ", visibiliteAttribut=" + visibiliteAttribut +
                ", attributType=" + attributType +
                ", unique=" + unique +
                ", nullable=" + nullable +
                ", generationType=" + generationType +
                ", findBy=" + findBy +
                ", dtoAttribut=" + dtoAttribut +
                '}';
    }
}


