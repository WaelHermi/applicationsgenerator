package tn.com.veganet.generateur.model.type;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DependanceScope {

    COMPILE("Compile", "compile"),
    PROVIDED("Provided", "provided"),
    RUNTIME("Runtime", "runtime"),
    TEST("Test", "test"),
    SYSTEM("System", "system"),
    IMPORT("Import", "import");

    String nomType;
    String valeurType;
}
