package tn.com.veganet.generateur.model.type;

import lombok.Getter;

@Getter
public enum AttributType {

    UUID("UUID"),
    BYTE("Byte"),
    SHORT("Short"),
    INTEGER("Integer"),
    LONG("Long"),
    FLOAT("Float"),
    DOUBLE("Double"),
    BIGDECIMAL("BigDecimal"),
    CHARACTER("Character" ),
    STRING("String"),
    BOOLEAN("Boolean"),
    DATE("Date"),
    TIME("Time"),
    TIMESTAMP("Timestamp"),
    BLOB("Blob");

    private String nomType;

    AttributType(String nomType) {
        this.nomType = nomType;
    }
    
}
