package tn.com.veganet.generateur.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.model.application.Application;

import java.io.Serializable;

@NoArgsConstructor
@Getter @Setter
public class UpdateProjectContext implements Serializable {

    private GeneratedProject generatedProject;
    private Application application;

}
