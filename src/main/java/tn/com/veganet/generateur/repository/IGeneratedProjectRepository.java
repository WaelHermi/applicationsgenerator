package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.domain.Utilisateur;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Collection;
import java.util.UUID;

public interface IGeneratedProjectRepository extends JpaRepository<GeneratedProject, UUID>{

    Collection<GeneratedProject> getByUtilisateur(Utilisateur utilisateur);

    boolean existsByUrlGeneratedProject(String urlGeneratedProject);

    boolean existsByUrlGeneratedProjectAndIdGeneratedProjectNot(String urlGeneratedProject, UUID idGeneratedProject);

}

