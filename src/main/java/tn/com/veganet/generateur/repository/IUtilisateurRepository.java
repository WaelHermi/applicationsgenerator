package tn.com.veganet.generateur.repository;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import tn.com.veganet.generateur.domain.Utilisateur;

import java.util.Optional;
import java.util.UUID;

public interface IUtilisateurRepository extends JpaRepository<Utilisateur, UUID>{

    Optional<Utilisateur> findByUsernameUtilisateur(String identifiantUtilisateur);

    boolean existsByUsernameUtilisateur(String identifiantUtilisateur);

    boolean existsByUsernameUtilisateurAndIdUtilisateurNot(String identifiantUtilisateur, UUID idUtilisateur);

    Utilisateur findByAdresseMailUtilisateur(String email);

}

