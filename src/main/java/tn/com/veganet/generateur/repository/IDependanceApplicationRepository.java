package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.DependanceApplication;
import tn.com.veganet.generateur.model.type.ApplicationType;

public interface IDependanceApplicationRepository extends JpaRepository<DependanceApplication, UUID>{

    Collection<DependanceApplication> findByApplicationType(ApplicationType applicationType);

    Optional<DependanceApplication> findByApplicationTypeAndGroupDependanceAndArtifactDependance(ApplicationType applicationType, String group, String artifact);

}

