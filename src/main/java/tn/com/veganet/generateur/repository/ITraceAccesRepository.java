package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.com.veganet.generateur.domain.TraceAcces;

import java.util.UUID;

public interface ITraceAccesRepository extends JpaRepository<TraceAcces, UUID>{

}

