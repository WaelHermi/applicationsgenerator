package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.Dependance;

public interface IDependanceRepository extends JpaRepository<Dependance, UUID>{

}

