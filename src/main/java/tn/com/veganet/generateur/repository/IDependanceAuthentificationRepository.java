package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.DependanceAuthentification;
import tn.com.veganet.generateur.model.type.AuthentificationType;

public interface IDependanceAuthentificationRepository extends JpaRepository<DependanceAuthentification, UUID>{

    Collection<DependanceAuthentification> findByAuthentificationType(AuthentificationType authentificationType);

    Optional<DependanceAuthentification> findByAuthentificationTypeAndGroupDependanceAndArtifactDependance(AuthentificationType authentificationType, String group, String artifact);

}

