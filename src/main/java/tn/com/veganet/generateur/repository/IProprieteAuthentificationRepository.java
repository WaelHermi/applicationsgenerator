package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.ProprieteAuthentification;
import tn.com.veganet.generateur.model.type.AuthentificationType;

public interface IProprieteAuthentificationRepository extends JpaRepository<ProprieteAuthentification, UUID>{

    Collection<ProprieteAuthentification> findByAuthentificationType(AuthentificationType authentificationType);

    Optional<ProprieteAuthentification> findByAuthentificationTypeAndNomPropriete(AuthentificationType authentificationType, String nom);
}

