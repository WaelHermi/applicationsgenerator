package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.ProprieteApplication;
import tn.com.veganet.generateur.model.type.ApplicationType;

public interface IProprieteApplicationRepository extends JpaRepository<ProprieteApplication, UUID>{

    Collection<ProprieteApplication> findByApplicationType(ApplicationType applicationType);

    Optional<ProprieteApplication> findByApplicationTypeAndNomPropriete(ApplicationType applicationType, String nom);
}

