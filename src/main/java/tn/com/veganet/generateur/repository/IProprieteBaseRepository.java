package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.ProprieteBase;
import tn.com.veganet.generateur.model.type.BaseType;

public interface IProprieteBaseRepository extends JpaRepository<ProprieteBase, UUID>{

    Collection<ProprieteBase> findByBaseType(BaseType baseType);

    Optional<ProprieteBase> findByBaseTypeAndNomPropriete(BaseType baseType, String nom);
}

