package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.com.veganet.generateur.domain.TraceDonnees;

import java.util.UUID;

public interface ITraceDonneesRepository extends JpaRepository<TraceDonnees, UUID>{

}

