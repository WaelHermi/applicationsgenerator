package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.Propriete;

public interface IProprieteRepository extends JpaRepository<Propriete, UUID>{

}

