package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.com.veganet.generateur.domain.Fonctionnalite;

import java.util.Optional;
import java.util.UUID;

public interface IFonctionnaliteRepository extends JpaRepository<Fonctionnalite, UUID>{

    boolean existsByUrlFonctionnalite(String urlFonctionnalite);

    boolean existsByUrlFonctionnaliteAndIdFonctionnaliteNot(String urlFonctionnalite, UUID idFonctionnalite);

    boolean existsByDesignationFonctionnalite(String designationFonctionnalite);

    Optional<Fonctionnalite> findByDesignationFonctionnalite(String designationFonctionnalite);

    boolean existsByDesignationFonctionnaliteAndIdFonctionnaliteNot(String designationFonctionnalite, UUID idFonctionnalite);

    boolean existsByCodeFonctionnalite(String codeFonctionnalite);

    boolean existsByCodeFonctionnaliteAndIdFonctionnaliteNot(String codeFonctionnalite, UUID idFonctionnalite);

}

