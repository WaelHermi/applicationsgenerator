package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.com.veganet.generateur.domain.Role;

import java.util.Optional;
import java.util.UUID;

public interface IRoleRepository extends JpaRepository<Role, UUID>{

    boolean existsByNomRole(String nomRole);

    boolean existsByNomRoleAndIdRoleNot(String nomRole, UUID idRole);

    Optional<Role> findByNomRole(String nomRole);
}

