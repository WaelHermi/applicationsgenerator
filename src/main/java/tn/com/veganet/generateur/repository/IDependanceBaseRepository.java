package tn.com.veganet.generateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import tn.com.veganet.generateur.domain.DependanceBase;
import tn.com.veganet.generateur.model.type.BaseType;

public interface IDependanceBaseRepository extends JpaRepository<DependanceBase, UUID>{

    Collection<DependanceBase> findByBaseType(BaseType baseType);

    Optional<DependanceBase> findByBaseTypeAndGroupDependanceAndArtifactDependance(BaseType baseType, String group, String Artifact);

}

