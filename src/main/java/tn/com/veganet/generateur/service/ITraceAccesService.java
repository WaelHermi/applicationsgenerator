package tn.com.veganet.generateur.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.domain.TraceAcces;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.model.AuthenticationRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public interface ITraceAccesService {

    Page<TraceAcces> findAll(Pageable pageable) throws GetException;

    TraceAcces findOne(UUID id) throws GetException;

    TraceAcces add(AuthenticationRequest authenticationRequest, HttpServletRequest httpServletRequest) throws CreateException;

    Long count();
}