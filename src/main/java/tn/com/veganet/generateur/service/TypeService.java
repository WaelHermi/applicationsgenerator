package tn.com.veganet.generateur.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import tn.com.veganet.generateur.domain.*;
import tn.com.veganet.generateur.model.EnumValeursWrapper;
import tn.com.veganet.generateur.model.type.*;
import tn.com.veganet.generateur.service.ITypeService;

import java.util.ArrayList;
import java.util.List;

@Service
@Primary
public class TypeService implements ITypeService {

    @Override
    public List<EnumValeursWrapper> getApplicationTypeEnumValues() {

        List<EnumValeursWrapper> applicationTypeList = new ArrayList<>();

        for (ApplicationType applicationType: ApplicationType.values()
             ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(applicationType.name(), applicationType.getNomType());
            applicationTypeList.add(enumValeursWrapper);
        }

        return applicationTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getPackagingEnumValues() {

        List<EnumValeursWrapper> packagingTypeList = new ArrayList<>();

        for (Packaging packaging: Packaging.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(packaging.name(), packaging.getNomPackaging());
            packagingTypeList.add(enumValeursWrapper);
        }

        return packagingTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getBaseTypeEnumValues() {
        List<EnumValeursWrapper> baseTypeList = new ArrayList<>();

        for (BaseType baseType: BaseType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(baseType.name(), baseType.getNomType());
            baseTypeList.add(enumValeursWrapper);
        }

        return baseTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getAttributTypeEnumValues() {
        List<EnumValeursWrapper> AttributTypeList = new ArrayList<>();

        for (AttributType attributType: AttributType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(attributType.name(), attributType.getNomType());
            AttributTypeList.add(enumValeursWrapper);
        }

        return AttributTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getVisibiliteEnumValues() {
        List<EnumValeursWrapper> visibiliteList = new ArrayList<>();

        for (Visibilite visibilite: Visibilite.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(visibilite.name(), visibilite.getNomVisibilite());
            visibiliteList.add(enumValeursWrapper);
        }

        return visibiliteList;
    }

    @Override
    public List<EnumValeursWrapper> getGenerationTypeEnumValues() {
        List<EnumValeursWrapper> generationTypeList = new ArrayList<>();

        for (GenerationType generationType: GenerationType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(generationType.name(), generationType.getNomGenerationType());
            generationTypeList.add(enumValeursWrapper);
        }

        return generationTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getRelationJPATypeEnumValues() {
        List<EnumValeursWrapper> relationTypeList = new ArrayList<>();

        for (RelationJPAType relationJPAType: RelationJPAType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(relationJPAType.name(), relationJPAType.getNomRelation());
            relationTypeList.add(enumValeursWrapper);
        }

        return relationTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getFetchTypeEnumValues() {
        List<EnumValeursWrapper> fetchTypeList = new ArrayList<>();

        for (FetchType fetchType: FetchType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(fetchType.name(), fetchType.getNomFetch());
            fetchTypeList.add(enumValeursWrapper);
        }

        return fetchTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getCascadeTypeEnumValues() {
        List<EnumValeursWrapper> cascadeTypeList = new ArrayList<>();

        for (CascadeType cascadeType: CascadeType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(cascadeType.name(), cascadeType.getNomCascadeType());
            cascadeTypeList.add(enumValeursWrapper);
        }

        return cascadeTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getAuthentificationTypeEnumValues() {
        List<EnumValeursWrapper> authentificationTypeList = new ArrayList<>();

        for (AuthentificationType authentificationType: AuthentificationType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(authentificationType.name(), authentificationType.getNomType());
            authentificationTypeList.add(enumValeursWrapper);
        }

        return authentificationTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getInheritanceTypeEnumValues() {
        List<EnumValeursWrapper> inheritanceTypeList = new ArrayList<>();

        for (InheritanceType inheritanceType: InheritanceType.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(inheritanceType.name(), inheritanceType.getNomType());
            inheritanceTypeList.add(enumValeursWrapper);
        }

        return inheritanceTypeList;
    }

    @Override
    public List<EnumValeursWrapper> getDependanceScopeEnumValues() {

        List<EnumValeursWrapper> dependanceScopeList = new ArrayList<>();

        for (DependanceScope dependanceScope: DependanceScope.values()
        ) {

            EnumValeursWrapper enumValeursWrapper = new EnumValeursWrapper(dependanceScope.name(), dependanceScope.getNomType());
            dependanceScopeList.add(enumValeursWrapper);
        }

        return dependanceScopeList;
    }

}
