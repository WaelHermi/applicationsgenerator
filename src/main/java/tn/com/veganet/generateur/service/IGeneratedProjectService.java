package tn.com.veganet.generateur.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.UUID;

public interface IGeneratedProjectService {

    Page<GeneratedProject> findAll(Pageable pageable) throws GetException;

    GeneratedProject findOne(UUID id) throws GetException;

    GeneratedProject add(GeneratedProject generatedProject) throws CreateException;

    GeneratedProject update(GeneratedProject generatedProject) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

    Collection<GeneratedProject> findByUtilisateur(HttpServletRequest httpServletRequest) throws GetException;

}