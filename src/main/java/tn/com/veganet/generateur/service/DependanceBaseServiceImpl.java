package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.DependanceBase;
import tn.com.veganet.generateur.model.type.BaseType;
import tn.com.veganet.generateur.repository.IDependanceBaseRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class DependanceBaseServiceImpl implements IDependanceBaseService {

    @Autowired
    private IDependanceBaseRepository dependanceBaseRepository;
    private static final String ENTITY_NAME = "DependanceBase";

    @Transactional(readOnly = true)
    public Page<DependanceBase> findAll(Pageable pageable) {

        return dependanceBaseRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public DependanceBase findOne(UUID id) throws GetException {

        Optional<DependanceBase> dependanceBase = dependanceBaseRepository.findById(id);

        if(!dependanceBase.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return dependanceBase.get();
    }

    @Override
    public DependanceBase add(DependanceBase dependanceBase) throws CreateException {


        return dependanceBaseRepository.saveAndFlush(dependanceBase);
    }

    @Override
    public DependanceBase update(DependanceBase dependanceBase) throws UpdateException {

        if( dependanceBase.getIdDependance() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return dependanceBaseRepository.saveAndFlush(dependanceBase);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!dependanceBaseRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        dependanceBaseRepository.deleteById(id);
        return !dependanceBaseRepository.existsById(id);
    }

    @Override
    public Long count() {
        return dependanceBaseRepository.count();
    }

    @Override
    public Collection<DependanceBase> findByBaseType(BaseType baseType) throws GetException {

    Collection<DependanceBase> dependancesBase = dependanceBaseRepository.findByBaseType(baseType);

    if(dependancesBase == null){
        GetException getException = new GetException();
        getException.setNomEntitie(ENTITY_NAME);
        getException.setRaisonExceptionNotFound();
        throw  getException;
    }

        return dependancesBase;
    }
}