package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.DependanceApplication;
import tn.com.veganet.generateur.model.type.ApplicationType;
import tn.com.veganet.generateur.repository.IDependanceApplicationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IDependanceApplicationService {

    Page<DependanceApplication> findAll(Pageable pageable) throws GetException;

    DependanceApplication findOne(UUID id) throws GetException;

    DependanceApplication add(DependanceApplication dependanceApplication) throws CreateException;

    DependanceApplication update(DependanceApplication dependanceApplication) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;


    Long count();

    Collection<DependanceApplication> findByApplicationType(ApplicationType ApplicationType) throws GetException;

}