package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.ProprieteBase;
import tn.com.veganet.generateur.model.type.BaseType;
import tn.com.veganet.generateur.repository.IProprieteBaseRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class ProprieteBaseServiceImpl implements IProprieteBaseService {

    @Autowired
    private IProprieteBaseRepository proprieteBaseRepository;
    private static final String ENTITY_NAME = "ProprieteBase";

    @Transactional(readOnly = true)
    public Page<ProprieteBase> findAll(Pageable pageable) {

        return proprieteBaseRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public ProprieteBase findOne(UUID id) throws GetException {

        Optional<ProprieteBase> proprieteBase = proprieteBaseRepository.findById(id);

        if(!proprieteBase.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return proprieteBase.get();
    }

    @Override
    public ProprieteBase add(ProprieteBase proprieteBase) throws CreateException {


        return proprieteBaseRepository.saveAndFlush(proprieteBase);
    }

    @Override
    public ProprieteBase update(ProprieteBase proprieteBase) throws UpdateException {

        if( proprieteBase.getIdPropriete() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return proprieteBaseRepository.saveAndFlush(proprieteBase);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!proprieteBaseRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        proprieteBaseRepository.deleteById(id);
        return !proprieteBaseRepository.existsById(id);
    }

    @Override
    public Long count() {
        return proprieteBaseRepository.count();
    }

    @Override
    public Collection<ProprieteBase> findByBaseType(BaseType baseType) throws GetException {

        Collection<ProprieteBase> proprieteBase = proprieteBaseRepository.findByBaseType(baseType);

        if(proprieteBase == null){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
        return proprieteBase;
    }

}