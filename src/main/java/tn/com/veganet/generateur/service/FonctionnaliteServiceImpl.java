package tn.com.veganet.generateur.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.Fonctionnalite;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.repository.IFonctionnaliteRepository;

import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class FonctionnaliteServiceImpl implements IFonctionnaliteService {

    @Autowired
    private IFonctionnaliteRepository fonctionnaliteRepository;
    private static final String ENTITY_NAME = "Fonctionnalite";

    @Transactional(readOnly = true)
    public Page<Fonctionnalite> findAll(Pageable pageable) {

        return fonctionnaliteRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Fonctionnalite findOne(UUID id) throws GetException {

        Optional<Fonctionnalite> fonctionnalite = fonctionnaliteRepository.findById(id);

        if(!fonctionnalite.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return fonctionnalite.get();
    }

    @Override
    public Fonctionnalite add(Fonctionnalite fonctionnalite) throws CreateException {

        if(fonctionnaliteRepository.existsByUrlFonctionnalite(fonctionnalite.getUrlFonctionnalite())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("urlFonctionnalite");
            throw createException;
        }
        if(fonctionnaliteRepository.existsByDesignationFonctionnalite(fonctionnalite.getDesignationFonctionnalite())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("designationFonctionnalite");
            throw createException;
        }
        if(fonctionnaliteRepository.existsByCodeFonctionnalite(fonctionnalite.getCodeFonctionnalite())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("codeFonctionnalite");
            throw createException;
        }

        return fonctionnaliteRepository.saveAndFlush(fonctionnalite);
    }

    @Override
    public Fonctionnalite update(Fonctionnalite fonctionnalite) throws UpdateException {

        if( fonctionnalite.getIdFonctionnalite() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        if(fonctionnaliteRepository.existsByUrlFonctionnaliteAndIdFonctionnaliteNot(fonctionnalite.getUrlFonctionnalite(), fonctionnalite.getIdFonctionnalite())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique("urlFonctionnalite");

            throw updateException;

        }

        if(fonctionnaliteRepository.existsByDesignationFonctionnaliteAndIdFonctionnaliteNot(fonctionnalite.getDesignationFonctionnalite(), fonctionnalite.getIdFonctionnalite())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique("designationFonctionnalite");

            throw updateException;

        }

        if(fonctionnaliteRepository.existsByCodeFonctionnaliteAndIdFonctionnaliteNot(fonctionnalite.getCodeFonctionnalite(), fonctionnalite.getIdFonctionnalite())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique("codeFonctionnalite");

            throw updateException;

        }

        return fonctionnaliteRepository.saveAndFlush(fonctionnalite);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!fonctionnaliteRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        fonctionnaliteRepository.deleteById(id);
        return !fonctionnaliteRepository.existsById(id);
    }
}