package tn.com.veganet.generateur.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.domain.TraceDonnees;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public interface ITraceDonneesService {

    Page<TraceDonnees> findAll(Pageable pageable) throws GetException;

    TraceDonnees findOne(UUID id) throws GetException;

    TraceDonnees add(String operation, HttpServletRequest httpServletRequest) throws CreateException, GetException;

    Long count();
}