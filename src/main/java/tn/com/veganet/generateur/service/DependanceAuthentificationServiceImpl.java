package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.DependanceAuthentification;
import tn.com.veganet.generateur.model.type.AuthentificationType;
import tn.com.veganet.generateur.repository.IDependanceAuthentificationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class DependanceAuthentificationServiceImpl implements IDependanceAuthentificationService {

    @Autowired
    private IDependanceAuthentificationRepository dependanceAuthentificationRepository;
    private static final String ENTITY_NAME = "DependanceAuthentification";

    @Transactional(readOnly = true)
    public Page<DependanceAuthentification> findAll(Pageable pageable) {

        return dependanceAuthentificationRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public DependanceAuthentification findOne(UUID id) throws GetException {

        Optional<DependanceAuthentification> dependanceAuthentification = dependanceAuthentificationRepository.findById(id);

        if(!dependanceAuthentification.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return dependanceAuthentification.get();
    }

    @Override
    public DependanceAuthentification add(DependanceAuthentification dependanceAuthentification) throws CreateException {


        return dependanceAuthentificationRepository.saveAndFlush(dependanceAuthentification);
    }

    @Override
    public DependanceAuthentification update(DependanceAuthentification dependanceAuthentification) throws UpdateException {

        if( dependanceAuthentification.getIdDependance() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return dependanceAuthentificationRepository.saveAndFlush(dependanceAuthentification);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!dependanceAuthentificationRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        dependanceAuthentificationRepository.deleteById(id);
        return !dependanceAuthentificationRepository.existsById(id);
    }

    @Override
    public Long count() {
        return dependanceAuthentificationRepository.count();
    }

    @Override
    public Collection<DependanceAuthentification> findByAuthentificationType(AuthentificationType authentificationType) throws GetException {

        Collection<DependanceAuthentification> dependanceAuthentification = dependanceAuthentificationRepository.findByAuthentificationType(authentificationType);

        if(dependanceAuthentification == null){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
        return dependanceAuthentification;
    }

}