package tn.com.veganet.generateur.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.domain.Fonctionnalite;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

import java.util.UUID;

public interface IFonctionnaliteService {

    Page<Fonctionnalite> findAll(Pageable pageable) throws GetException;

    Fonctionnalite findOne(UUID id) throws GetException;

    Fonctionnalite add(Fonctionnalite fonctionnalite) throws CreateException;

    Fonctionnalite update(Fonctionnalite fonctionnalite) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

}