package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.*;
import tn.com.veganet.generateur.model.EnumValeursWrapper;
import tn.com.veganet.generateur.model.type.DependanceScope;

import java.util.List;

public interface ITypeService {

    List<EnumValeursWrapper> getApplicationTypeEnumValues();

    List<EnumValeursWrapper> getPackagingEnumValues();

    List<EnumValeursWrapper> getBaseTypeEnumValues();

    List<EnumValeursWrapper> getAttributTypeEnumValues();

    List<EnumValeursWrapper> getVisibiliteEnumValues();

    List<EnumValeursWrapper> getGenerationTypeEnumValues();

    List<EnumValeursWrapper> getRelationJPATypeEnumValues();

    List<EnumValeursWrapper> getFetchTypeEnumValues();

    List<EnumValeursWrapper> getCascadeTypeEnumValues();

    List<EnumValeursWrapper> getAuthentificationTypeEnumValues();

    List<EnumValeursWrapper> getInheritanceTypeEnumValues();

    List<EnumValeursWrapper> getDependanceScopeEnumValues();
}
