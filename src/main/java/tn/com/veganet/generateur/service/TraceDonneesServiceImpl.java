package tn.com.veganet.generateur.service;

import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.TraceDonnees;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.repository.ITraceDonneesRepository;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class TraceDonneesServiceImpl implements ITraceDonneesService {

    @Autowired
    private ITraceDonneesRepository traceDonneesRepository;

    @Autowired
    private JwtUtil jwtUtil;

    private static final String ENTITY_NAME = "TraceDonnees";

    @Transactional(readOnly = true)
    public Page<TraceDonnees> findAll(Pageable pageable) {

        return traceDonneesRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public TraceDonnees findOne(UUID id) throws GetException {

        Optional<TraceDonnees> traceDonnees = traceDonneesRepository.findById(id);

        if(!traceDonnees.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return traceDonnees.get();
    }

    @Override
    public TraceDonnees add(String operation, HttpServletRequest httpServletRequest) throws CreateException, GetException {

        UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));
        TraceDonnees traceDonnees = new TraceDonnees();
        traceDonnees.setOperation(operation);
        traceDonnees.setUtilisateur(jwtUtil.getUtilisateurFromToken(httpServletRequest));
        traceDonnees.setDate(new Timestamp(System.currentTimeMillis()));
        traceDonnees.setNavigateur(userAgent.getBrowser().getName());
        traceDonnees.setVersionNavigateur(userAgent.getBrowserVersion().getVersion());
        traceDonnees.setIp(httpServletRequest.getRemoteAddr());
        return traceDonneesRepository.saveAndFlush(traceDonnees);
    }

    @Override
    public Long count() {
        return traceDonneesRepository.count();
    }

}