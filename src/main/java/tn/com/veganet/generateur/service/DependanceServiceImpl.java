package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.Dependance;
import tn.com.veganet.generateur.repository.IDependanceRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class DependanceServiceImpl implements IDependanceService {

    @Autowired
    private IDependanceRepository dependanceRepository;
    private static final String ENTITY_NAME = "Dependance";

    @Transactional(readOnly = true)
    public Page<Dependance> findAll(Pageable pageable) {

        return dependanceRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Dependance findOne(UUID id) throws GetException {

        Optional<Dependance> dependance = dependanceRepository.findById(id);

        if(!dependance.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return dependance.get();
    }

    @Override
    public Dependance add(Dependance dependance) throws CreateException {


        return dependanceRepository.saveAndFlush(dependance);
    }

    @Override
    public Dependance update(Dependance dependance) throws UpdateException {

        if( dependance.getIdDependance() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return dependanceRepository.saveAndFlush(dependance);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!dependanceRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        dependanceRepository.deleteById(id);
        return !dependanceRepository.existsById(id);
    }
}