package tn.com.veganet.generateur.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.domain.Role;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

import java.util.UUID;

public interface IRoleService {

    Page<Role> findAll(Pageable pageable) throws GetException;

    Role findOne(UUID id) throws GetException;

    Role add(Role role) throws CreateException;

    Role update(Role role) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

}