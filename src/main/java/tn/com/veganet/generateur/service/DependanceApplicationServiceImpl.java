package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.DependanceApplication;
import tn.com.veganet.generateur.model.type.ApplicationType;
import tn.com.veganet.generateur.repository.IDependanceApplicationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class DependanceApplicationServiceImpl implements IDependanceApplicationService {

    @Autowired
    private IDependanceApplicationRepository dependanceApplicationRepository;
    private static final String ENTITY_NAME = "DependanceApplication";

    @Transactional(readOnly = true)
    public Page<DependanceApplication> findAll(Pageable pageable) {

        return dependanceApplicationRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public DependanceApplication findOne(UUID id) throws GetException {

        Optional<DependanceApplication> dependanceApplication = dependanceApplicationRepository.findById(id);

        if(!dependanceApplication.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return dependanceApplication.get();
    }

    @Override
    public DependanceApplication add(DependanceApplication dependanceApplication) throws CreateException {


        return dependanceApplicationRepository.saveAndFlush(dependanceApplication);
    }

    @Override
    public DependanceApplication update(DependanceApplication dependanceApplication) throws UpdateException {

        if( dependanceApplication.getIdDependance() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return dependanceApplicationRepository.saveAndFlush(dependanceApplication);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!dependanceApplicationRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        dependanceApplicationRepository.deleteById(id);
        return !dependanceApplicationRepository.existsById(id);
    }

    @Override
    public Long count() {
        return dependanceApplicationRepository.count();
    }

    @Override
    public Collection<DependanceApplication> findByApplicationType(ApplicationType applicationType) throws GetException {

        Collection<DependanceApplication> dependanceApplication = dependanceApplicationRepository.findByApplicationType(applicationType);

        if(dependanceApplication == null){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
        return dependanceApplication;
    }

}