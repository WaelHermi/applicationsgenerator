package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.ProprieteApplication;
import tn.com.veganet.generateur.model.type.ApplicationType;
import tn.com.veganet.generateur.repository.IProprieteApplicationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class ProprieteApplicationServiceImpl implements IProprieteApplicationService {

    @Autowired
    private IProprieteApplicationRepository proprieteApplicationRepository;
    private static final String ENTITY_NAME = "ProprieteApplication";

    @Transactional(readOnly = true)
    public Page<ProprieteApplication> findAll(Pageable pageable) {

        return proprieteApplicationRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public ProprieteApplication findOne(UUID id) throws GetException {

        Optional<ProprieteApplication> proprieteApplication = proprieteApplicationRepository.findById(id);

        if(!proprieteApplication.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return proprieteApplication.get();
    }

    @Override
    public ProprieteApplication add(ProprieteApplication proprieteApplication) throws CreateException {


        return proprieteApplicationRepository.saveAndFlush(proprieteApplication);
    }

    @Override
    public ProprieteApplication update(ProprieteApplication proprieteApplication) throws UpdateException {

        if( proprieteApplication.getIdPropriete() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return proprieteApplicationRepository.saveAndFlush(proprieteApplication);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!proprieteApplicationRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        proprieteApplicationRepository.deleteById(id);
        return !proprieteApplicationRepository.existsById(id);
    }

    @Override
    public Long count() {
        return proprieteApplicationRepository.count();
    }

    @Override
    public Collection<ProprieteApplication> findByApplicationType(ApplicationType applicationType) throws GetException {

        Collection<ProprieteApplication> proprieteApplication = proprieteApplicationRepository.findByApplicationType(applicationType);

        if(proprieteApplication == null){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
        return proprieteApplication;
    }

}