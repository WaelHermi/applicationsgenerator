package tn.com.veganet.generateur.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.model.PasswordModel;
import tn.com.veganet.generateur.model.ResetPasswordRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public interface IUtilisateurService {

    Page<Utilisateur> findAll(Pageable pageable) throws GetException;

    Utilisateur findOne(UUID id) throws GetException;

    Utilisateur findByUsernameUtilisateur(String username) throws GetException;

    Utilisateur addGestionnaire(Utilisateur utilisateur) throws CreateException;

    Utilisateur inscriptionClient(Utilisateur utilisateur) throws CreateException;

    Utilisateur update(Utilisateur utilisateur) throws UpdateException;

    Boolean delete(UUID id) throws DeleteException;

    Boolean SendEmailToResetPassword(String email);

    Boolean resetPassword(ResetPasswordRequest resetPasswordRequest) throws GetException;

    Boolean updatePassword(HttpServletRequest httpServletRequest, PasswordModel passwordModel) throws GetException, UpdateException, BadCredentialsException;

    Long count();
}