package tn.com.veganet.generateur.service;

import freemarker.template.TemplateException;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.model.UpdateProjectContext;
import tn.com.veganet.generateur.model.application.Application;
import tn.com.veganet.generateur.model.application.Base;
import tn.com.veganet.generateur.model.application.Entity;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface IGenerateurService {

    String genererApplicationFromXml(File file) throws ParserConfigurationException, IOException;

    String genererApplication(Application application) throws ParserConfigurationException, IOException, TemplateException;

    List<Entity> extraireDatabaseEntities(Base base) throws SQLException;

    String validerFichierXML(File file);

    GeneratedProject enregistrerFichierXML(Application application, HttpServletRequest httpServletRequest) throws GetException, CreateException;

    GeneratedProject modifierEnregistrementFichierXML(UpdateProjectContext updateProjectContext, HttpServletRequest httpServletRequest) throws GetException, UpdateException;

    Boolean getConnection(Base base);

    Application getSavedApplication(GeneratedProject generatedProject) throws ParserConfigurationException;
}
