package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.DependanceBase;
import tn.com.veganet.generateur.model.type.BaseType;
import tn.com.veganet.generateur.repository.IDependanceBaseRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IDependanceBaseService {

    Page<DependanceBase> findAll(Pageable pageable) throws GetException;

    DependanceBase findOne(UUID id) throws GetException;

    DependanceBase add(DependanceBase dependanceBase) throws CreateException;

    DependanceBase update(DependanceBase dependanceBase) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

    Long count();

    Collection<DependanceBase> findByBaseType(BaseType baseType) throws GetException;

}