package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.DependanceAuthentification;
import tn.com.veganet.generateur.model.type.AuthentificationType;
import tn.com.veganet.generateur.repository.IDependanceAuthentificationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IDependanceAuthentificationService {

    Page<DependanceAuthentification> findAll(Pageable pageable) throws GetException;

    DependanceAuthentification findOne(UUID id) throws GetException;

    DependanceAuthentification add(DependanceAuthentification dependanceAuthentification) throws CreateException;

    DependanceAuthentification update(DependanceAuthentification dependanceAuthentification) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;


    Long count();

    Collection<DependanceAuthentification> findByAuthentificationType(AuthentificationType AuthentificationType) throws GetException;

}