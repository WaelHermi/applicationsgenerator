package tn.com.veganet.generateur.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.model.MailModel;

import javax.mail.internet.MimeMessage;

@Service
@Primary
public class MailServiceImpl implements IMailService {


    @Autowired
    public JavaMailSender emailSender;

   @Value("${front.resetpassword.url}")
    private String url_frontend;

    @Override
    public Boolean sendSimpleMessage(MailModel mailModel) {
        try {

            MimeMessage message = emailSender.createMimeMessage();

            boolean multipart = true;

            MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");


            message.setContent(mailModel.getText(), "text/html");

            helper.setTo(mailModel.getSendto());

            helper.setSubject(mailModel.getSubject());


            this.emailSender.send(message);
            return true;

        }catch (Exception e){
            return false;
        }

    }

    @Override
    public Boolean sendSimpleMessageForResetPassword(Utilisateur utilisateur, String token) {
        try{
            String htmltext= "<html>"+
                    "<head>"+
                    "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n" +
                    "\n" +
                    "<!-- Optional theme -->\n" +
                    "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">\n" +
                    "\n" +
                    "<!-- Latest compiled and minified JavaScript -->\n" +
                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>"+
                    "</head>"+
                    "<body>"+
                    "<div class='row'> <h1>Cher " + utilisateur.getPrenomUtilisateur() + " " + utilisateur.getNomUtilisateur().toUpperCase()+"</h1></div>"+
                    "<div class='row'>  </div>"+
                    "<div class='row'> Vous avez récemment demandé à réinitialiser votre mot de passe pour <br> votre compte.  Utilisez le bouton ci-dessous pour le réinitialiser.<strong> Cette <br> réinitialisation du mot de passe n'est valable que pour les <br> prochaines 24 heures. </strong>"+
                    "<div class='row'> <a href='"+url_frontend+"password/"+token+"' class='btn btn-success'>Réinitialisez votre mot de passe</a>\n </div>"+
                    "<div class='row'> Si vous n'avez pas demandé de réinitialisation de mot de passe, <br> veuillez ignorer cet email ou contacter le support technique <br> sur support@vegagroup.com.tn si vous avez des questions. </div>"+
                    "<br>"+
                    "<div class='row'></div>"+
                    "<hr>"+
                    "<br>"+
                    "<div class='row'> Si vous rencontrez des  problèmes avec le bouton ci-dessus, copiez et collez lâ€™URL ci-dessous dans votre navigateur Web. </div>"+
                    "<br>"+
                    "<div class='row'> <a href='"+url_frontend+""+token+"' class='btn btn-success'>"+url_frontend+""+token+"</a> </div>"+
                    " </body>"+
                    "</html>";
            MailModel mailModel=new MailModel();
            mailModel.setText(htmltext);
            mailModel.setSubject("réinitialisation du mot de passe");
            mailModel.setSendto(utilisateur.getAdresseMailUtilisateur());
            return sendSimpleMessage(mailModel);
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public Boolean sendSimpleMessageAfterCreatingGestionnaireAccount(Utilisateur utilisateur) {
        try{
            String htmltext= "<html><p><img src='https://le-rim.org/wp-content/uploads/2018/06/bienvenue-rim-730x482.png' alt='' width='633' height='281' /></p><h2 style='text-align: center;'>On vous souhaite la bienvenue au sein de notre plateforme <span style='color: #0000ff;'>API-Creator</span>!</h2><p>&nbsp;</p><h2 style='text-align: center;'><span style='text-align: center; color: #0000ff;'>&nbsp; &nbsp; &nbsp; <span style='color: #999999;'>votre compte est maintenant Actif.</span></span></h2><p>&nbsp;</p><h3>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style='text-decoration: underline;'>Votre identifiant</span>: "
                    + utilisateur.getUsernameUtilisateur()
                    + "</h3><h3>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style='text-decoration: underline;'>Votre Mot de passe</span>:"
                    + utilisateur.getPasswordUtilisateur()
                    + "</h3><p>&nbsp;</p><h2 style='text-align: center;'><span style='background-color: #3366ff;'><a style='background-color: #3366ff;' href='localhost:4200/auth/login'>&nbsp; <span style='color: #ffffff;'>Cliquer ici&nbsp;&nbsp;</span>&nbsp;</a></span></h2><p>&nbsp;</p><p><span style='color: #999999;'>&nbsp; &nbsp;Merci pour votre confiance!</span></p></html>";
            MailModel mailModel=new MailModel();
            mailModel.setText(htmltext);
            mailModel.setSubject("Création d'un compte sur API-Creator");

            mailModel.setSendto(utilisateur.getAdresseMailUtilisateur());
            return sendSimpleMessage(mailModel);
        }catch (Exception e){
            return false;
        }

    }

}

