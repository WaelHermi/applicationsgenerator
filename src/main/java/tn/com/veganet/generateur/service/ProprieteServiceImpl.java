package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.Propriete;
import tn.com.veganet.generateur.repository.IProprieteRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class ProprieteServiceImpl implements IProprieteService {

    @Autowired
    private IProprieteRepository proprieteRepository;
    private static final String ENTITY_NAME = "Propriete";

    @Transactional(readOnly = true)
    public Page<Propriete> findAll(Pageable pageable) {

        return proprieteRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Propriete findOne(UUID id) throws GetException {

        Optional<Propriete> propriete = proprieteRepository.findById(id);

        if(!propriete.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return propriete.get();
    }

    @Override
    public Propriete add(Propriete propriete) throws CreateException {


        return proprieteRepository.saveAndFlush(propriete);
    }

    @Override
    public Propriete update(Propriete propriete) throws UpdateException {

        if( propriete.getIdPropriete() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return proprieteRepository.saveAndFlush(propriete);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!proprieteRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        proprieteRepository.deleteById(id);
        return !proprieteRepository.existsById(id);
    }
}