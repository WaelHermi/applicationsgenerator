package tn.com.veganet.generateur.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.repository.IGeneratedProjectRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class GeneratedProjectServiceImpl implements IGeneratedProjectService {

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    private IGeneratedProjectRepository generatedProjectRepository;
    private static final String ENTITY_NAME = "GeneratedProject";

    @Transactional(readOnly = true)
    public Page<GeneratedProject> findAll(Pageable pageable) {

        return generatedProjectRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public GeneratedProject findOne(UUID id) throws GetException {

        Optional<GeneratedProject> generatedProject = generatedProjectRepository.findById(id);

        if(!generatedProject.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return generatedProject.get();
    }

    @Override
    public GeneratedProject add(GeneratedProject generatedProject) throws CreateException {

        if(generatedProjectRepository.existsByUrlGeneratedProject(generatedProject.getUrlGeneratedProject())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("urlGeneratedProject");
            throw createException;
        }

        return generatedProjectRepository.saveAndFlush(generatedProject);
    }

    @Override
    public GeneratedProject update(GeneratedProject generatedProject) throws UpdateException {

        if( generatedProject.getIdGeneratedProject() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        if(generatedProjectRepository.existsByUrlGeneratedProjectAndIdGeneratedProjectNot(generatedProject.getUrlGeneratedProject(), generatedProject.getIdGeneratedProject())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique("urlGeneratedProject");

            throw updateException;

        }

        return generatedProjectRepository.saveAndFlush(generatedProject);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!generatedProjectRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        generatedProjectRepository.deleteById(id);
        return !generatedProjectRepository.existsById(id);
    }

    @Override
    public Collection<GeneratedProject> findByUtilisateur(HttpServletRequest httpServletRequest) throws GetException {
        return generatedProjectRepository.getByUtilisateur(jwtUtil.getUtilisateurFromToken(httpServletRequest));
    }
}