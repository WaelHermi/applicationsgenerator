package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.ProprieteAuthentification;
import tn.com.veganet.generateur.model.type.AuthentificationType;
import tn.com.veganet.generateur.repository.IProprieteAuthentificationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IProprieteAuthentificationService {

    Page<ProprieteAuthentification> findAll(Pageable pageable) throws GetException;

    ProprieteAuthentification findOne(UUID id) throws GetException;

    ProprieteAuthentification add(ProprieteAuthentification proprieteAuthentification) throws CreateException;

    ProprieteAuthentification update(ProprieteAuthentification proprieteAuthentification) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

    Long count();

    Collection<ProprieteAuthentification> findByAuthentificationType(AuthentificationType AuthentificationType) throws GetException;

}