package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.ProprieteAuthentification;
import tn.com.veganet.generateur.model.type.AuthentificationType;
import tn.com.veganet.generateur.repository.IProprieteAuthentificationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

@Service
@Transactional
public class ProprieteAuthentificationServiceImpl implements IProprieteAuthentificationService {

    @Autowired
    private IProprieteAuthentificationRepository proprieteAuthentificationRepository;
    private static final String ENTITY_NAME = "ProprieteAuthentification";

    @Transactional(readOnly = true)
    public Page<ProprieteAuthentification> findAll(Pageable pageable) {

        return proprieteAuthentificationRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public ProprieteAuthentification findOne(UUID id) throws GetException {

        Optional<ProprieteAuthentification> proprieteAuthentification = proprieteAuthentificationRepository.findById(id);

        if(!proprieteAuthentification.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return proprieteAuthentification.get();
    }

    @Override
    public ProprieteAuthentification add(ProprieteAuthentification proprieteAuthentification) throws CreateException {


        return proprieteAuthentificationRepository.saveAndFlush(proprieteAuthentification);
    }

    @Override
    public ProprieteAuthentification update(ProprieteAuthentification proprieteAuthentification) throws UpdateException {

        if( proprieteAuthentification.getIdPropriete() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        return proprieteAuthentificationRepository.saveAndFlush(proprieteAuthentification);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!proprieteAuthentificationRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        proprieteAuthentificationRepository.deleteById(id);
        return !proprieteAuthentificationRepository.existsById(id);
    }

    @Override
    public Long count() {
        return proprieteAuthentificationRepository.count();
    }

    @Override
    public Collection<ProprieteAuthentification> findByAuthentificationType(AuthentificationType authentificationType) throws GetException {

        Collection<ProprieteAuthentification> proprieteAuthentification = proprieteAuthentificationRepository.findByAuthentificationType(authentificationType);

        if(proprieteAuthentification == null){
            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;
        }
        return proprieteAuthentification;
    }

}