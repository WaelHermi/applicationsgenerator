package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.ProprieteBase;
import tn.com.veganet.generateur.model.type.BaseType;
import tn.com.veganet.generateur.repository.IProprieteBaseRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IProprieteBaseService {

    Page<ProprieteBase> findAll(Pageable pageable) throws GetException;

    ProprieteBase findOne(UUID id) throws GetException;

    ProprieteBase add(ProprieteBase proprieteBase) throws CreateException;

    ProprieteBase update(ProprieteBase proprieteBase) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

    Long count();

    Collection<ProprieteBase> findByBaseType(BaseType baseType) throws GetException;

}