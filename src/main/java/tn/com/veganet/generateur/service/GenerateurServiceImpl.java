package tn.com.veganet.generateur.service;


import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import tn.com.veganet.generateur.compresseur.Compresseur;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.extracteur.xml.ExtracteurUtil;
import tn.com.veganet.generateur.extracteur.xml.IExtracteurFichier;
import tn.com.veganet.generateur.extracteur.database.IExtracteurModeleDatabase;
import tn.com.veganet.generateur.extracteur.xml.ValidateurFichierXMLImpl;
import tn.com.veganet.generateur.generateur.GenerateurFichierXML;
import tn.com.veganet.generateur.generateur.IGenerateurAPI;
import tn.com.veganet.generateur.model.UpdateProjectContext;
import tn.com.veganet.generateur.model.application.Application;
import tn.com.veganet.generateur.model.application.Base;
import tn.com.veganet.generateur.model.application.Entity;
import tn.com.veganet.generateur.util.GlobaleConstantes;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Service
@Primary
public class GenerateurServiceImpl implements IGenerateurService {

    @Autowired
    private IExtracteurFichier fileExtracteur;

    @Autowired
    private ValidateurFichierXMLImpl validateurFichierXMLImpl;

    @Autowired
    private IGenerateurAPI generateurAPI;

    @Autowired
    private GenerateurFichierXML generateurFichierXML;

    @Autowired
    private IExtracteurModeleDatabase extracteurModeleDatabase;

    @Autowired
    private ExtracteurUtil extracteurUtil;

    @Autowired
    private IGeneratedProjectService generatedProjectService;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public String genererApplicationFromXml(File file) throws ParserConfigurationException, IOException{

            String parentDirectoryName = UUID.randomUUID().toString();
            Application application = fileExtracteur.extraireApplicationsFromFile(file);
            generateurAPI.genererFromArchetype(application, GlobaleConstantes.SOURCE_APPLICATION_MONOLITHIQUE_CHEMIN, parentDirectoryName);
            return Compresseur.zipDirectory(parentDirectoryName + "/" + application.getArtifactApplication());
    }

    @Override
    public String genererApplication(Application application) throws ParserConfigurationException, IOException, TemplateException {

        String parentDirectoryName = UUID.randomUUID().toString();
        if(application.getEntities() != null){
            application.setEntities(extracteurUtil.affecterExtendedEntities(application.getEntities()));
            application.setEntities(extracteurUtil.affecterEntitiesToRelations(application.getEntities()));
        }
        generateurAPI.genererFromArchetype(application, GlobaleConstantes.SOURCE_APPLICATION_MONOLITHIQUE_CHEMIN, parentDirectoryName);
        return Compresseur.zipDirectory(parentDirectoryName + "/" + application.getArtifactApplication());
    }

    @Override
    public List<Entity> extraireDatabaseEntities(Base base) throws SQLException {
        List<Entity> entities  = extracteurModeleDatabase.getEntitiesFromDatabaseTables(base);
        return entities;

    }

    @Override
    public String validerFichierXML(File file){

        validateurFichierXMLImpl = ValidateurFichierXMLImpl.getInstance();
        return validateurFichierXMLImpl.validerFichier(file);
    }

    @Override
    public GeneratedProject enregistrerFichierXML(Application application, HttpServletRequest httpServletRequest) throws GetException, CreateException {
        jwtUtil.getUtilisateurFromToken(httpServletRequest);
        GeneratedProject generatedProject = generateurFichierXML.genererFichierXML(application, httpServletRequest);
        generatedProject = generatedProjectService.add(generatedProject);
        return generatedProject;
    }

    @Override
    public GeneratedProject modifierEnregistrementFichierXML(UpdateProjectContext updateProjectContext, HttpServletRequest httpServletRequest) throws GetException, UpdateException {
        jwtUtil.getUtilisateurFromToken(httpServletRequest);
        GeneratedProject updateGeneratedProject = generateurFichierXML.regenererFichierXML(updateProjectContext.getGeneratedProject(), updateProjectContext.getApplication(), httpServletRequest);
        updateGeneratedProject.setIdGeneratedProject(updateProjectContext.getGeneratedProject().getIdGeneratedProject());
        return generatedProjectService.update(updateGeneratedProject);
    }

    @Override
    public Boolean getConnection(Base base) {
        return extracteurModeleDatabase.getConnection(base);
    }

    @Override
    public Application getSavedApplication(GeneratedProject generatedProject) throws ParserConfigurationException {
        return fileExtracteur.extraireApplicationsFromFile(new File(generatedProject.getUrlGeneratedProject()));
    }
}
