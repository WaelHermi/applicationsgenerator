package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.model.MailModel;

public interface IMailService {
    Boolean sendSimpleMessage(MailModel mailModel);
    Boolean sendSimpleMessageForResetPassword(Utilisateur utilisateur, String token);
    Boolean sendSimpleMessageAfterCreatingGestionnaireAccount(Utilisateur utilisateur);
}
