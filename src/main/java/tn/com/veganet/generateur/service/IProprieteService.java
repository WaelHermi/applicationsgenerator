package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.Propriete;
import tn.com.veganet.generateur.repository.IProprieteRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IProprieteService {

    Page<Propriete> findAll(Pageable pageable) throws GetException;

    Propriete findOne(UUID id) throws GetException;

    Propriete add(Propriete propriete) throws CreateException;

    Propriete update(Propriete propriete) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

}