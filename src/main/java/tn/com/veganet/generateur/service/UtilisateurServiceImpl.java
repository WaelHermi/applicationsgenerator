package tn.com.veganet.generateur.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.Role;
import tn.com.veganet.generateur.domain.Utilisateur;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.model.PasswordModel;
import tn.com.veganet.generateur.model.ResetPasswordRequest;
import tn.com.veganet.generateur.repository.IRoleRepository;
import tn.com.veganet.generateur.repository.IUtilisateurRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Primary
@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService, UserDetailsService {

    @Autowired
    private IUtilisateurRepository utilisateurRepository;

    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private IMailService mailService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private static final String ENTITY_NAME = "Utilisateur";

    @Transactional(readOnly = true)
    public Page<Utilisateur> findAll(Pageable pageable) {

        return utilisateurRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Utilisateur findOne(UUID id) throws GetException {

        Optional<Utilisateur> utilisateur = utilisateurRepository.findById(id);

        if(!utilisateur.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return utilisateur.get();
    }

    @Override
    public Utilisateur findByUsernameUtilisateur(String username) throws GetException {

        Optional<Utilisateur> utilisateur = utilisateurRepository.findByUsernameUtilisateur(username);

        if(!utilisateur.isPresent()){

            GetException getException = new GetException();
            getException.setNomEntitie(ENTITY_NAME);
            getException.setRaisonExceptionNotFound();
            throw  getException;

        }

        return utilisateur.get();
    }

    @Override
    public Utilisateur addGestionnaire(Utilisateur utilisateur) throws CreateException {

        if(utilisateurRepository.existsByUsernameUtilisateur(utilisateur.getUsernameUtilisateur())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("usernameUtilisateur");
            throw createException;
        }

        Optional<Role> role = roleRepository.findByNomRole("ROLE_GESTIONNAIRE");
        utilisateur.setActive(true);
        utilisateur.setRoles(Arrays.asList(role.isPresent() ? role.get() : null));
        mailService.sendSimpleMessageAfterCreatingGestionnaireAccount(utilisateur);
        utilisateur.setPasswordUtilisateur(passwordEncoder.encode(utilisateur.getPasswordUtilisateur()));
        return utilisateurRepository.saveAndFlush(utilisateur);
    }

    @Override
    public Utilisateur inscriptionClient(Utilisateur utilisateur) throws CreateException {

        if(utilisateurRepository.existsByUsernameUtilisateur(utilisateur.getUsernameUtilisateur())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("usernameUtilisateur");
            throw createException;
        }

        Optional<Role> role = roleRepository.findByNomRole("ROLE_CLIENT");

        utilisateur.setRoles(Arrays.asList(role.isPresent() ? role.get() : null));
        utilisateur.setActive(true);
        utilisateur.setPasswordUtilisateur(passwordEncoder.encode(utilisateur.getPasswordUtilisateur()));
        return utilisateurRepository.saveAndFlush(utilisateur);
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur) throws UpdateException {

        if( utilisateur.getIdUtilisateur() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        Optional<Utilisateur> utilisateurOptionalForRoles = this.utilisateurRepository.findByUsernameUtilisateur(utilisateur.getUsernameUtilisateur());

        if(utilisateurOptionalForRoles.isPresent()){
         utilisateur.setRoles(utilisateurOptionalForRoles.get().getRoles());
        }

        if(utilisateurRepository.existsByUsernameUtilisateurAndIdUtilisateurNot(utilisateur.getUsernameUtilisateur(), utilisateur.getIdUtilisateur())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique("usernameUtilisateur");

            throw updateException;

        }
        return utilisateurRepository.saveAndFlush(utilisateur);
    }


    @Override
    public Boolean delete(UUID id) throws DeleteException {

        if(!utilisateurRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        utilisateurRepository.deleteById(id);
        return !utilisateurRepository.existsById(id);
    }

    @Override
    public Boolean SendEmailToResetPassword(String email) {
        try {
            if (email != null) {
                String regex = "^(.+)@(.+)$";

                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(email);
                if (matcher.matches()) {
                    Utilisateur utilisateur = utilisateurRepository.findByAdresseMailUtilisateur(email);
                    if (utilisateur != null) {
                        UserDetails userDetails=new User(utilisateur.getUsernameUtilisateur(),utilisateur.getPasswordUtilisateur(),getAuthorities(utilisateur.getRoles().stream().collect(Collectors.toList())));
                        String tokenresponse = jwtUtil.generateToken(userDetails);
                        mailService.sendSimpleMessageForResetPassword(utilisateur, tokenresponse);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;

                }

            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean resetPassword(ResetPasswordRequest resetPasswordRequest) throws GetException {
        Utilisateur utilisateur = jwtUtil.getUtilisateurFromToken(resetPasswordRequest.getToken());
        if(utilisateur != null){
            utilisateur.setPasswordUtilisateur(passwordEncoder.encode(resetPasswordRequest.getPassword()));
            utilisateurRepository.save(utilisateur);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public Boolean updatePassword(HttpServletRequest httpServletRequest, PasswordModel passwordModel) throws GetException, UpdateException, BadCredentialsException {

        Utilisateur utilisateur = jwtUtil.getUtilisateurFromToken(httpServletRequest);
        if(utilisateur != null){
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(utilisateur.getUsernameUtilisateur(), passwordModel.getAncienMotDePasse()));
            utilisateur.setPasswordUtilisateur(passwordEncoder.encode(passwordModel.getNouveauMotDePasse()));
            update(utilisateur);
            return true;
        }
        return false;
    }

    @Override
    public Long count() {
        return utilisateurRepository.count();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Utilisateur> utilisateur = utilisateurRepository.findByUsernameUtilisateur(username);

        if(utilisateur.isPresent()){
            return new User(utilisateur.get().getUsernameUtilisateur(), utilisateur.get().getPasswordUtilisateur(), true, true ,true, utilisateur.get().getActive(), getAuthorities(new ArrayList<>(utilisateur.get().getRoles()))) ;
        }else{
            throw new UsernameNotFoundException("Nom d'utilisateur non trouvé");
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles) {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        for (Role role: roles) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getNomRole());
            grantedAuthorities.add(grantedAuthority);
        }
        return grantedAuthorities;
    }

    /*
    @Override
    public ModuleResponse SendEmailToResetPassword(String email) {
        try {
            if (email != null) {
                String regex = "^(.+)@(.+)$";

                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(email);
                if (matcher.matches()) {
                    UserEl utilisateur = userElrepository.findByEmail(email);
                    if (utilisateur != null) {
                        ModuleResponse tokenresponse = jwtTokenUtil.generate(utilisateur);
                        String token = (String) tokenresponse.getpartnerResponse();
                        iMailService.sendSimpleMessageForResetPassword(utilisateur, token);
                        return new ModuleResponse(EnumMessage.EMAIL_SENDED.code, EnumMessage.EMAIL_SENDED.label, true);

                    } else {
                        return new ModuleResponse(EnumMessage.USER_NOT_EXIST.code, EnumMessage.USER_NOT_EXIST.label,
                                null);
                    }
                } else {
                    return new ModuleResponse(EnumMessage.INVALID_EMAIL_FORMAT.code,
                            EnumMessage.INVALID_EMAIL_FORMAT.label, null);

                }

            } else {
                return new ModuleResponse(EnumMessage.Email_EMPTY.code, EnumMessage.Email_EMPTY.label, null);
            }

        } catch (Exception e) {
            return new ModuleResponse(EnumMessage.ERREUR_QUERY.code, e.getMessage(), null);

        }
    }

     */
}