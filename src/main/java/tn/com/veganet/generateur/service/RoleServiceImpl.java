package tn.com.veganet.generateur.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.Role;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;
import tn.com.veganet.generateur.repository.IRoleRepository;

import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleRepository roleRepository;
    private static final String ENTITY_NAME = "Role";

    @Transactional(readOnly = true)
    public Page<Role> findAll(Pageable pageable) {

        return roleRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Role findOne(UUID id) throws GetException {

        Optional<Role> role = roleRepository.findById(id);

        if(!role.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return role.get();
    }

    @Override
    public Role add(Role role) throws CreateException {

        if(roleRepository.existsByNomRole(role.getNomRole())){

            CreateException createException = new CreateException();
            createException.setNomEntitie(ENTITY_NAME);
            createException.setRaisonExceptionChampUnique("nomRole");
            throw createException;
        }

        return roleRepository.saveAndFlush(role);
    }

    @Override
    public Role update(Role role) throws UpdateException {

        if( role.getIdRole() == null){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionIdNull();

            throw updateException;

        }

        if(roleRepository.existsByNomRoleAndIdRoleNot(role.getNomRole(), role.getIdRole())){

            UpdateException updateException = new UpdateException();
            updateException.setNomEntitie(ENTITY_NAME);
            updateException.setRaisonExceptionChampUnique("nomRole");

            throw updateException;

        }

        return roleRepository.saveAndFlush(role);
    }

    @Override
    public boolean delete(UUID id) throws DeleteException {

        if(!roleRepository.existsById(id)){
            DeleteException deleteException = new DeleteException();
            deleteException.setNomEntitie(ENTITY_NAME);
            deleteException.setRaisonDosentExist();

            throw deleteException;
        }

        roleRepository.deleteById(id);
        return !roleRepository.existsById(id);
    }
}