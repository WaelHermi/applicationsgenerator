package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.ProprieteApplication;
import tn.com.veganet.generateur.model.type.ApplicationType;
import tn.com.veganet.generateur.repository.IProprieteApplicationRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IProprieteApplicationService {

    Page<ProprieteApplication> findAll(Pageable pageable) throws GetException;

    ProprieteApplication findOne(UUID id) throws GetException;

    ProprieteApplication add(ProprieteApplication proprieteApplication) throws CreateException;

    ProprieteApplication update(ProprieteApplication proprieteApplication) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

    Long count();

    Collection<ProprieteApplication> findByApplicationType(ApplicationType ApplicationType) throws GetException;

}