package tn.com.veganet.generateur.service;

import tn.com.veganet.generateur.domain.Dependance;
import tn.com.veganet.generateur.repository.IDependanceRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.DeleteException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.exception.UpdateException;

public interface IDependanceService {

    Page<Dependance> findAll(Pageable pageable) throws GetException;

    Dependance findOne(UUID id) throws GetException;

    Dependance add(Dependance dependance) throws CreateException;

    Dependance update(Dependance dependance) throws UpdateException;

    boolean delete(UUID id) throws DeleteException;

}