package tn.com.veganet.generateur.service;

import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.com.veganet.generateur.domain.TraceAcces;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.model.AuthenticationRequest;
import tn.com.veganet.generateur.repository.ITraceAccesRepository;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class TraceAccesServiceImpl implements ITraceAccesService {

    @Autowired
    private ITraceAccesRepository traceAccesRepository;
    private static final String ENTITY_NAME = "TraceAcces";

    @Transactional(readOnly = true)
    public Page<TraceAcces> findAll(Pageable pageable) {

        return traceAccesRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public TraceAcces findOne(UUID id) throws GetException {

        Optional<TraceAcces> traceAcces = traceAccesRepository.findById(id);

        if(!traceAcces.isPresent()){

                GetException getException = new GetException();
                getException.setNomEntitie(ENTITY_NAME);
                getException.setRaisonExceptionNotFound();
                throw  getException;

        }

        return traceAcces.get();
    }

    @Override
    public TraceAcces add(AuthenticationRequest authenticationRequest, HttpServletRequest httpServletRequest) throws CreateException {

        UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));
        TraceAcces traceAcces = new TraceAcces();
        traceAcces.setIdentifiant(authenticationRequest.getUsernameUtilisateur());
        traceAcces.setDate(new Timestamp(System.currentTimeMillis()));
        traceAcces.setIp(httpServletRequest.getRemoteAddr());
        traceAcces.setNavigateur(userAgent.getBrowser().getName());
        traceAcces.setVersionNavigateur(userAgent.getBrowserVersion().getVersion());
        traceAcces.setStatus(authenticationRequest.getStatus());
        return traceAccesRepository.saveAndFlush(traceAcces);
    }

    @Override
    public Long count() {
        return traceAccesRepository.count();
    }

}