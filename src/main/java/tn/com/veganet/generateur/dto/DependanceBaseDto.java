package tn.com.veganet.generateur.dto;

import java.io.Serializable;
import java.util.*;
import java.sql.*;
import lombok.*;
import tn.com.veganet.generateur.model.type.BaseType;

import javax.validation.constraints.*;


@NoArgsConstructor
@Getter @Setter
public class DependanceBaseDto extends DependanceDto implements Serializable {
    private BaseType baseType;
}

