package tn.com.veganet.generateur.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;


@NoArgsConstructor
@Getter @Setter
public class RoleDto implements Serializable {

    private UUID idRole;

    @NotNull(message = "nomRole ne peut pas être null")
    private String nomRole;

    private Collection<FonctionnaliteDto> fonctionnalites;
}

