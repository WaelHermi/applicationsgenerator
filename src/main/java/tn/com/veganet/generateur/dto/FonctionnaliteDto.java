package tn.com.veganet.generateur.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;


@NoArgsConstructor
@Getter @Setter
public class FonctionnaliteDto implements Serializable {

    private UUID idFonctionnalite;

    private String iconFonctionnalite;

    private String urlFonctionnalite;

    @NotNull(message = "designationFonctionnalite ne peut pas être null")
    private String designationFonctionnalite;

    @NotNull(message = "codeFonctionnalite ne peut pas être null")
    private String codeFonctionnalite;

}

