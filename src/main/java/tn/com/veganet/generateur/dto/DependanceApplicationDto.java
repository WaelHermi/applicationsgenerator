package tn.com.veganet.generateur.dto;

import java.io.Serializable;
import java.util.*;
import java.sql.*;
import lombok.*;
import tn.com.veganet.generateur.model.type.ApplicationType;

import javax.validation.constraints.*;


@NoArgsConstructor
@Getter @Setter
public class DependanceApplicationDto extends DependanceDto implements Serializable {
    private ApplicationType applicationType;
}

