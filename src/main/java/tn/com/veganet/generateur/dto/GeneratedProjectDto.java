package tn.com.veganet.generateur.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;


@NoArgsConstructor
@Getter @Setter
public class GeneratedProjectDto implements Serializable {

    private UUID idGeneratedProject;

    private String typeGeneratedProject;

    @NotNull(message = "urlGeneratedProject ne peut pas être null")
    private String urlGeneratedProject;

    @NotNull(message = "nomGeneratedProject ne peut pas être null")
    private String nomGeneratedProject;

    private Timestamp date;

    private UtilisateurDto utilisateur;

}

