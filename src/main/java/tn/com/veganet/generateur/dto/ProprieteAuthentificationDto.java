package tn.com.veganet.generateur.dto;

import java.io.Serializable;
import java.util.*;
import java.sql.*;
import lombok.*;
import tn.com.veganet.generateur.model.type.AuthentificationType;

import javax.validation.constraints.*;


@NoArgsConstructor
@Getter @Setter
public class ProprieteAuthentificationDto extends ProprieteDto implements Serializable {
    private AuthentificationType authentificationType;
}

