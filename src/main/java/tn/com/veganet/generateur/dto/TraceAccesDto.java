package tn.com.veganet.generateur.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;


@NoArgsConstructor
@Getter @Setter
public class TraceAccesDto implements Serializable {

    private UUID idTraceAcces;

    private Timestamp date;

    private String versionNavigateur;

    private String navigateur;

    private String ip;

    private Boolean status;

    private String identifiant;

}

