package tn.com.veganet.generateur.dto;

import java.io.Serializable;
import java.util.*;
import java.sql.*;
import lombok.*;
import javax.validation.constraints.*;


@NoArgsConstructor
@Getter @Setter
public class ProprieteDto implements Serializable {

    private UUID idPropriete;

    @NotNull(message = "valeurPropriete ne peut pas être null")
    private String valeurPropriete;

    @NotNull(message = "nomPropriete ne peut pas être null")
    private String nomPropriete;

}

