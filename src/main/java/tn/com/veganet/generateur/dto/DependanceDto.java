package tn.com.veganet.generateur.dto;

import java.io.Serializable;
import java.util.*;
import java.sql.*;
import lombok.*;
import tn.com.veganet.generateur.model.type.DependanceScope;

import javax.validation.constraints.*;


@NoArgsConstructor
@Getter @Setter
public class DependanceDto implements Serializable {

    private UUID idDependance;

    private String versionDependance;

    @NotNull(message = "artifactDependance ne peut pas être null")
    private String artifactDependance;

    @NotNull(message = "groupDependance ne peut pas être null")
    private String groupDependance;

    private DependanceScope dependanceScope;

}

