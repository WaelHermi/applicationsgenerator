package tn.com.veganet.generateur.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;


@NoArgsConstructor
@Getter @Setter
public class UtilisateurDto implements Serializable {

    private UUID idUtilisateur;

    @NotNull(message = "adresseMailUtilisateur ne peut pas être null")
    private String adresseMailUtilisateur;

    private String adresseUtilisateur;

    private String telephoneUtilisateur;

    @NotNull(message = "passwordUtilisateur ne peut pas être null")
    private String passwordUtilisateur;

    @NotNull(message = "identifiantUtilisateur ne peut pas être null")
    private String usernameUtilisateur;

    @NotNull(message = "prenomUtilisateur ne peut pas être null")
    private String prenomUtilisateur;

    @NotNull(message = "nomUtilisateur ne peut pas être null")
    private String nomUtilisateur;

    private Date dateDeNaissanceUtilisateur;

    private Boolean active;

    private Collection<RoleDto> roles;

    @Getter(onMethod = @__( @JsonIgnore))
    private Collection<TraceDonneesDto> traceDonneess;

    @Getter(onMethod = @__( @JsonIgnore))
    private Collection<GeneratedProjectDto> generatedProjects;

}

