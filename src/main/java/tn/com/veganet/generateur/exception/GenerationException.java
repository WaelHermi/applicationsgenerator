package tn.com.veganet.generateur.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class GenerationException extends Exception {

    private String element;
    private String raisonException = "Un problème est survenu au cours de la génération";


    @Override
    public String getMessage(){
        return  element + ": " + raisonException;
    }

}
