package tn.com.veganet.generateur.generateur;

import tn.com.veganet.generateur.model.application.Application;

public interface IGenerateurAPI {

    void genererFromArchetype(Application application, String sourceApplicationChemin, String parentDirectory);
}
