package tn.com.veganet.generateur.generateur;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tn.com.veganet.generateur.domain.GeneratedProject;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.model.application.Application;
import tn.com.veganet.generateur.service.JwtUtil;
import tn.com.veganet.generateur.util.GlobaleConstantes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class GenerateurFichierXML {

    @Autowired
    JwtUtil jwtUtil;

    //freemarker configuration class
    private Configuration cfg = new Configuration();

    public GeneratedProject genererFichierXML(Application application, HttpServletRequest httpServletRequest) {

        //directory for our ftl files.
        cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
        cfg.setObjectWrapper(Configuration.getDefaultObjectWrapper(Configuration.getVersion()));
        GeneratedProject generatedProject = new GeneratedProject();

        Map<String, Object> map = new HashMap<>();
        map.put("application", application);
        String fileName = UUID.randomUUID().toString();
        String destinationFile = GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/xmlFiles/" + fileName + ".xml";

        try(BufferedWriter elementOut = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(destinationFile), "UTF-8"))) {
            //here is our example template
            Template elementTemp = cfg.getTemplate("/autre/application.ftl");
            //freemaerker processing our attributes with keys and populates ftl file.
            elementTemp.process(map, elementOut);
            elementOut.flush();

            generatedProject.setNomGeneratedProject(application.getArtifactApplication());
            generatedProject.setTypeGeneratedProject(application.getApplicationType().getNomType());
            generatedProject.setUrlGeneratedProject(destinationFile);
            generatedProject.setUtilisateur(jwtUtil.getUtilisateurFromToken(httpServletRequest));
            generatedProject.setDate(new Timestamp(System.currentTimeMillis()));

        } catch (TemplateException | IOException | GetException e) {
            e.printStackTrace();
        }


        return generatedProject;
    }

    public GeneratedProject regenererFichierXML(GeneratedProject generatedProjectInput, Application application, HttpServletRequest httpServletRequest) {

        //directory for our ftl files.
        cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
        cfg.setObjectWrapper(Configuration.getDefaultObjectWrapper(Configuration.getVersion()));
        GeneratedProject generatedProject = new GeneratedProject();

        Map<String, Object> map = new HashMap<>();
        map.put("application", application);
        String fileName = UUID.randomUUID().toString();
        String destinationFile = generatedProjectInput.getUrlGeneratedProject();

        try(BufferedWriter elementOut = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(destinationFile), "UTF-8"))) {
            //here is our example template
            Template elementTemp = cfg.getTemplate("/autre/application.ftl");
            //freemaerker processing our attributes with keys and populates ftl file.
            elementTemp.process(map, elementOut);
            elementOut.flush();

            generatedProject.setNomGeneratedProject(application.getArtifactApplication());
            generatedProject.setTypeGeneratedProject(application.getApplicationType().getNomType());
            generatedProject.setUrlGeneratedProject(destinationFile);
            generatedProject.setUtilisateur(jwtUtil.getUtilisateurFromToken(httpServletRequest));
            generatedProject.setDate(new Timestamp(System.currentTimeMillis()));

        } catch (TemplateException | IOException | GetException e) {
            e.printStackTrace();
        }


        return generatedProject;
    }
}
