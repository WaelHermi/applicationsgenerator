package tn.com.veganet.generateur.generateur;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tn.com.veganet.generateur.domain.ProprieteBase;
import tn.com.veganet.generateur.model.application.*;
import tn.com.veganet.generateur.model.type.*;
import tn.com.veganet.generateur.repository.*;
import tn.com.veganet.generateur.util.GlobaleConstantes;

import java.io.*;
import java.util.*;

@Component
public class GenerateurAPIImpl implements IGenerateurAPI{

    //freemarker configuration class
    private Configuration cfg = new Configuration();

    @Autowired
    private IProprieteBaseRepository proprieteBaseRepository;

    @Autowired
    private IDependanceBaseRepository dependanceBaseRepository;

    @Autowired
    private IProprieteApplicationRepository proprieteApplicationRepository;

    @Autowired
    private IDependanceApplicationRepository dependanceApplicationRepository;

    @Autowired
    private IProprieteAuthentificationRepository proprieteAuthentificationRepository;

    @Autowired
    private IDependanceAuthentificationRepository dependanceAuthentificationRepository;

    @Override
    public void genererFromArchetype(Application application, String sourceApplicationChemin, String parentDirectory) {

        cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
        cfg.setObjectWrapper(Configuration.getDefaultObjectWrapper(Configuration.getVersion()));

        try {
            FileUtils.copyDirectory(new File(sourceApplicationChemin), new File(GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory + "/" + application.getArtifactApplication()));
            genererApplication(application, parentDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void genererApplication(Application application, String parentDirectory) throws IOException {
        Map<String, Object> map = new HashMap<>();

        map.put("application", application);
        map.put("package", application.getPackageApplication().replace("/", "."));

        String destination = GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory + "/" + application.getArtifactApplication() + GlobaleConstantes.JAVA_EMPLACEMENT + application.getPackageApplication();
        new File( destination).mkdirs();

        if(application.getApplicationType() != null){
            application.getApplicationType().setProprietes(proprieteApplicationRepository.findByApplicationType(application.getApplicationType()));
            application.getApplicationType().setDependances(dependanceApplicationRepository.findByApplicationType(application.getApplicationType()));
        }

        if(application.getAuthentificationType() != null && application.getAuthentificationType() != AuthentificationType.NON){
            application.getAuthentificationType().setProprietes(proprieteAuthentificationRepository.findByAuthentificationType(application.getAuthentificationType()));
            application.getAuthentificationType().setDependances(dependanceAuthentificationRepository.findByAuthentificationType(application.getAuthentificationType()));
        }

        if(application.getBase() != null && application.getBase().getBaseType() != null){

            application.getBase().getBaseType().setProprietes(proprieteBaseRepository.findByBaseType(application.getBase().getBaseType()));
            application.getBase().getBaseType().setDependances(dependanceBaseRepository.findByBaseType(application.getBase().getBaseType()));

            List<ProprieteBase> proprietes = new ArrayList<>();

            for (ProprieteBase propriete: application.getBase().getBaseType().getProprietes()
            ) {

                if(propriete.getValeurPropriete().contains("://")){
                    propriete.setValeurPropriete(propriete.getValeurPropriete()+application.getBase().getIpBase() + ":" + application.getBase().getPortBase() + "/"+application.getBase().getNomBase());
                }
                else if(propriete.getNomPropriete().contains("username"))
                {
                    propriete.setValeurPropriete(application.getBase().getUsernameBase());
                }
                else if(propriete.getNomPropriete().contains("password"))
                {
                    propriete.setValeurPropriete(application.getBase().getPasswordBase());
                }

                proprietes.add(propriete);
            }
            application.getBase().getBaseType().setProprietes(proprietes);
        }

        String applicationName = application.getArtifactApplication().substring(0, 1).toUpperCase() + application.getArtifactApplication().substring(1);
        genererBaseElement(map, parentDirectory, application.getArtifactApplication(), "/server/src/pom/pom.ftl", "/pom.xml");
        genererBaseElement(map, parentDirectory, application.getArtifactApplication(), "/server/src/properties/application.ftl", "/src/main/resources/application.properties");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/application/application.ftl", "/" + applicationName + "Application");

        if(application.getEntities() != null && !application.getEntities().isEmpty() || (application.getAuthentificationType() != null && !application.getAuthentificationType().name().equals("NON"))) {

            new File( destination + "/domain/").mkdirs();
            new File( destination + "/dto/").mkdirs();
            new File( destination + "/exception/").mkdirs();
            new File( destination + "/repository/").mkdirs();
            new File( destination + "/service/").mkdirs();
            new File( destination + "/endpoint/error").mkdirs();
            new File( destination + "/config/").mkdirs();

            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/endpoint/baseendpoint.ftl", "/endpoint/BaseEndpoint");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/exception/CreateException.ftl", "/exception/CreateException");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/exception/UpdateException.ftl", "/exception/UpdateException");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/exception/GetException.ftl", "/exception/GetException");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/exception/DeleteException.ftl", "/exception/DeleteException");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/endpoint/error/error.ftl", "/endpoint/error/Error");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/config/SwaggerConfig.ftl", "/config/" + "SwaggerConfig");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/config/WebConfiguration.ftl", "/config/" + "WebConfiguration");
            genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/config/SpringConfig.ftl", "/config/" + "SpringConfig");

            if(application.getAuthentificationType() != null && !application.getAuthentificationType().name().equals("NON")) {
                genererAuthentification(application, parentDirectory, map, destination);
            }
                genererEntities(application, parentDirectory, map);
        }

    }

    public void genererAuthentification(Application application, String parentDirectory, Map<String, Object> map, String destination) {

        new File( destination + "/filter/").mkdirs();
        new File( destination + "/util/").mkdirs();
        new File( destination + "/model/").mkdirs();

        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/config/SecurityConfig.ftl", "/config/SecurityConfig");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/filter/CORSFilter.ftl", "/filter/CORSFilter");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/filter/JwtRequestFilter.ftl", "/filter/JwtRequestFilter");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/service/JwtUtil.ftl", "/service/JwtUtil");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/endpoint/AuthenticationEndpoint.ftl", "/endpoint/AuthenticationEndpoint");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/util/SetupDataLoader.ftl", "/util/SetupDataLoader");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/util/SetupDataLoader.ftl", "/util/SetupDataLoader");
        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/model/AuthenticationRequest.ftl", "/model/AuthenticationRequest");        genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/authentification/model/AuthenticationResponse.ftl", "/model/AuthenticationResponse");

        Optional<Entity> entityRoleOptional = application.getEntities().stream().filter( element -> element.getNomEntity().toUpperCase().equals("ROLE")).findFirst();
        Entity roleEntity;
        if(!entityRoleOptional.isPresent()){
            roleEntity = new Entity();
            roleEntity.setNomEntity("Role");
            ClePrimaire roleClePrimaire = new ClePrimaire();
            Attribut idRoleattribut = new Attribut();
            idRoleattribut.setNomAttribut("idRole");
            idRoleattribut.setAttributType(AttributType.UUID);
            idRoleattribut.setVisibiliteAttribut(Visibilite.PRIVATE);
            idRoleattribut.setGenerationType(GenerationType.AUTRE);
            roleClePrimaire.setAttributs(Arrays.asList(idRoleattribut));
            roleEntity.setClePrimaire(roleClePrimaire);

        } else {
            roleEntity = entityRoleOptional.get();
            roleEntity.getAttributs().removeIf( element -> element.getNomAttribut().toUpperCase().equals("nomRole".toUpperCase()));
            roleEntity.getRelations().removeIf( element -> element.getNomEntity().equals("utilisateur"));
        }

        Attribut nomRoleAttribut = new Attribut();
        nomRoleAttribut.setNomAttribut("nomRole");
        nomRoleAttribut.setAttributType(AttributType.STRING);
        nomRoleAttribut.setVisibiliteAttribut(Visibilite.PRIVATE);
        nomRoleAttribut.setUnique(true);
        nomRoleAttribut.setDtoAttribut(true);
        nomRoleAttribut.setFindBy(true);
        nomRoleAttribut.setNullable(false);
        roleEntity.setAttributs(new ArrayList<>());
        roleEntity.getAttributs().add(nomRoleAttribut);
        application.getEntities().add(roleEntity);

        Relation relation = new Relation();
        relation.setNomEntity(roleEntity.getNomEntity());
        relation.setFindBy(false);
        relation.setFetchType(FetchType.DEFAULT);
        relation.setBidirectionnelle(false);
        relation.setNomRelation("roles");
        relation.setBiNomRelation("utilisateurs");
        relation.setRelationJPAType(RelationJPAType.MANYTOMANY);
        relation.setJsonIgnore(false);
        relation.setDtoRelation(true);
        relation.setDestination(roleEntity);

        Entity entity = new Entity();

        if (application.getAuthentificationType().name().equals("JWT")) {

            Optional<Entity> entityOptional = application.getEntities().stream().filter(element -> element.getNomEntity().toUpperCase().equals("Utilisateur".toUpperCase())).findFirst();

            if (entityOptional.isPresent()) {

                entity = entityOptional.get();
                Optional<Attribut> username = entityOptional.get().getAttributs().stream().filter(element -> element.getNomAttribut().toUpperCase().equals("usernameUtilisateur".toUpperCase())).findFirst();

                entity.getAttributs().stream().forEach( element -> {
                    element.setNullable(true);
                } );

                if(entity.getAttributs() == null){
                    entity.setAttributs(new ArrayList<>());
                }

                if(entity.getRelations() == null){
                    entity.setRelations(new ArrayList<>());
                }

                entity.getRelations().add(relation);

                if (!username.isPresent()) {

                    Attribut attribut = new Attribut();
                    attribut.setNomAttribut("usernameUtilisateur");
                    attribut.setAttributType(AttributType.STRING);
                    attribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                    attribut.setUnique(true);
                    attribut.setDtoAttribut(true);
                    attribut.setFindBy(true);
                    attribut.setNullable(false);
                    entity.getAttributs().add(attribut);

                }else{
                    username.get().setFindBy(true);
                    username.get().setUnique(true);
                }

                Optional<Attribut> password = entityOptional.get().getAttributs().stream().filter(element -> element.getNomAttribut().toUpperCase().equals("passwordUtilisateur".toUpperCase())).findFirst();

                if (!password.isPresent()) {

                    Attribut attribut = new Attribut();
                    attribut.setNomAttribut("passwordUtilisateur");
                    attribut.setAttributType(AttributType.UUID);
                    attribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                    attribut.setUnique(false);
                    attribut.setDtoAttribut(true);
                    attribut.setFindBy(false);
                    attribut.setNullable(false);
                    entity.getAttributs().add(attribut);

                }

                Optional<Attribut> active = entityOptional.get().getAttributs().stream().filter(element -> element.getNomAttribut().toUpperCase().equals("active".toUpperCase())).findFirst();

                if(!active.isPresent()){

                    Attribut activeAttribut = new Attribut();
                    activeAttribut.setNomAttribut("active");
                    activeAttribut.setAttributType(AttributType.BOOLEAN);
                    activeAttribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                    activeAttribut.setUnique(false);
                    activeAttribut.setDtoAttribut(true);
                    activeAttribut.setFindBy(false);
                    activeAttribut.setNullable(false);
                    entity.getAttributs().add(activeAttribut);
                }

            } else {

                entity.setNomEntity("Utilisateur");
                ClePrimaire utilisateurClePrimaire = new ClePrimaire();
                Attribut attribut = new Attribut();
                attribut.setNomAttribut("idUtilisateur");
                attribut.setAttributType(AttributType.UUID);
                attribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                attribut.setGenerationType(GenerationType.AUTRE);
                utilisateurClePrimaire.setAttributs(Arrays.asList(attribut));
                entity.setClePrimaire(utilisateurClePrimaire);

                Attribut usernameAttribut = new Attribut();
                usernameAttribut.setNomAttribut("usernameUtilisateur");
                usernameAttribut.setAttributType(AttributType.STRING);
                usernameAttribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                usernameAttribut.setUnique(true);
                usernameAttribut.setDtoAttribut(true);
                usernameAttribut.setFindBy(true);
                usernameAttribut.setNullable(false);

                Attribut passwordAttribut = new Attribut();
                passwordAttribut.setNomAttribut("passwordUtilisateur");
                passwordAttribut.setAttributType(AttributType.STRING);
                passwordAttribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                passwordAttribut.setUnique(false);
                passwordAttribut.setDtoAttribut(true);
                passwordAttribut.setFindBy(false);
                passwordAttribut.setNullable(false);

                Attribut activeAttribut = new Attribut();
                activeAttribut.setNomAttribut("active");
                activeAttribut.setAttributType(AttributType.BOOLEAN);
                activeAttribut.setVisibiliteAttribut(Visibilite.PRIVATE);
                activeAttribut.setUnique(false);
                activeAttribut.setDtoAttribut(true);
                activeAttribut.setFindBy(false);
                activeAttribut.setNullable(false);

                entity.setAttributs(Arrays.asList(usernameAttribut, passwordAttribut, activeAttribut));
                entity.setRelations(Arrays.asList(relation));
                application.getEntities().add(entity);

            }
        }
    }

    public void  genererEntities(Application application, String parentDirectory, Map<String, Object> map) throws IOException {

        if(application.getEntities() != null) {

            for (Entity entity : application.getEntities()) {

                String primaryKey = null;
                String primaryKeyNom = null;

                if (entity.getClePrimaire() != null && entity.getClePrimaire().getAttributs() != null && entity.getClePrimaire().getAttributs().size() > 1) {
                    primaryKey = entity.getClePrimaire().getNomClePrimaire();
                    primaryKeyNom = entity.getClePrimaire().getNomClePrimaire();
                } else if(entity.getClePrimaire() != null && entity.getClePrimaire().getAttributs() != null && entity.getClePrimaire().getAttributs().size() == 1) {
                    primaryKey = entity.getClePrimaire().getAttributs().get(0).getAttributType().getNomType();
                    primaryKeyNom = entity.getClePrimaire().getAttributs().get(0).getNomAttribut();
                }

                map.put("entity", entity);

                map.put("primarykey", primaryKey);
                map.put("primaryKeyNom", primaryKeyNom);

                genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/domain/Entity.ftl", "/domain/" + entity.getNomEntity());
                genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/dto/Dto.ftl", "/dto/" + entity.getNomEntity() + "Dto");

                if (entity.getClePrimaire() != null && entity.getClePrimaire().getAttributs() != null && entity.getClePrimaire().getAttributs().size() > 1) {
                    genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/primarykey/primarykey.ftl", "/domain/" + entity.getClePrimaire().getNomClePrimaire());
                }

                genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/repository/Repository.ftl", "/repository/I" + entity.getNomEntity() + "Repository");
                genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/service/service.ftl", "/service/" + entity.getNomEntity() + "ServiceImpl");
                genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/service/iservice.ftl", "/service/I" + entity.getNomEntity() + "Service");
                if(entity.getGenerateControleur()){
                    genererElement(map, parentDirectory, application.getArtifactApplication(), application.getPackageApplication(), "/server/src/endpoint/endpoint.ftl", "/endpoint/" + entity.getNomEntity() + "Endpoint");
                }
            }
        }
    }


    protected void genererElement(Map<String, Object> map, String parentDirectory, String artifactApplication, String packageApplication, String emplacementSourceElement, String emplacementDestinationElement){

        String elementfileName = GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory + "/" + artifactApplication + GlobaleConstantes.JAVA_EMPLACEMENT + packageApplication + emplacementDestinationElement + ".java";
        try(BufferedWriter elementOut = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(elementfileName), "UTF-8"));) {
            //here is our example template
            Template elementTemp = cfg.getTemplate(emplacementSourceElement);

            //freemaerker processing our attributes with keys and populates ftl file.
            elementTemp.process(map, elementOut);
            elementOut.flush();
        } catch ( TemplateException | IOException e) {
            e.printStackTrace();
        }

    }

    protected void genererBaseElement(Map<String, Object> map, String parentDirectory, String artifactApplication, String emplacementSourceElement, String emplacementDestinationElement) throws IOException {

        String elementfileName = GlobaleConstantes.DESTIONATION_APPLICATION_CHEMIN + "/" + parentDirectory  + "/" + artifactApplication + emplacementDestinationElement;

        try(BufferedWriter elementOut = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(elementfileName), "UTF-8"))) {
            //here is our example template
            Template elementTemp = cfg.getTemplate(emplacementSourceElement);

            //freemaerker processing our attributes with keys and populates ftl file.
            elementTemp.process(map, elementOut);
            elementOut.flush();
        } catch (UnsupportedEncodingException | FileNotFoundException | TemplateException e) {
            e.printStackTrace();
        }
    }
}
