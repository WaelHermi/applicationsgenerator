package tn.com.veganet.generateur.filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import tn.com.veganet.generateur.exception.CreateException;
import tn.com.veganet.generateur.exception.GetException;
import tn.com.veganet.generateur.service.ITraceDonneesService;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
@Order(1)
public class TarceFilter implements Filter {

    @Autowired
    private ITraceDonneesService traceDonneesService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        StatusExposingServletResponse response = new StatusExposingServletResponse((HttpServletResponse)servletResponse);

        String message = "";
        String operation = "";
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        String url = req.getRequestURL().toString();

        filterChain.doFilter(servletRequest, servletResponse);

        if(res.getStatus() == HttpServletResponse.SC_OK) {
            if(!url.contains("generateur")) {

                if(req.getMethod().equals("GET")){
                    operation = "Consulter";
                }
                else if (req.getMethod().equals("POST")){
                    operation = "Ajouter";
                }
                else if (req.getMethod().equals("PUT")){
                    operation = "Modifier";
                }
                else if (req.getMethod().equals("DELETE")){
                    operation = "Supprimer";
                }

                if(url.contains("utilisateur")){
                    message = operation + " Utilisateur";
                }
                else if (url.contains("utilisateur")) {
                    message = operation + " Client";
                }
                else if(url.contains("role")) {
                    message = operation + " Role";
                }
                else if (url.contains("type")){
                    message = operation + " Type";
                }
                else if (url.contains("Generated Project")){
                    message = operation + " Generated Project";
                }
                else if (url.contains("dependance")){
                    message = operation + " Dependance";
                }
                else if (url.contains("propriete")){
                    message = operation + " Propriété";
                }
            }
            else if(url.contains("generateur")) {
                if(url.contains("application_from_xml")){
                    message = "Génération d'une API à partir d'un fichier XML";
                }
                else if(url.contains("application")){
                    message = "Génération d'une API à partir d'un objet application";
                }
                else if(url.contains("enregistrer")){
                    message = "Enregistrement de la configuration d'une API";
                }
                else if(url.contains("modifier_enregistrement")){
                    message = "Modification de l'enregistrement de la configuration d'une API";
                }
                else if(url.contains("database_tables")){
                    message = "Extraction des entités à partir d'une base de données";
                }
                else if(url.contains("database_connection")){
                    message = "Connection à une base de données";
                }else if(url.contains("saved_application")){
                    message = "Récupération de la configuration d'une API REST Spring Boot du serveur";
                }
            }
        }
        if(!message.isEmpty()){
            try {
                traceDonneesService.add(message, req);
            } catch (CreateException | GetException e) {

            }
        }
    }
}
